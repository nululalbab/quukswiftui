//
//  ListItem+CoreDataProperties.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 18/11/20.
//
//

import Foundation
import CoreData


extension ListItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ListItem> {
        return NSFetchRequest<ListItem>(entityName: "ListItem")
    }

    @NSManaged public var check: Bool
    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var qty: Float
    @NSManaged public var origin: ListCat?
    
    public var wrappedListName: String{
                        name ?? "Unknown bahan"
                    }
            public var wrappedListQty: Float{
                qty
            }

        //Bahan
        public var wrappedBahanName: String{
                        name ?? "Unknown bahan"
                    }
            public var wrappedBahanQty: Float{
                qty
            }

}

extension ListItem : Identifiable {

}
