//
//  QuukSwiftUIApp.swift
//  QuukSwiftUI
//
//  Created by Najibullah Ulul Albab on 24/09/20.
//

import SwiftUI
import Sentry
import OneSignal

class AppDelegate: NSObject, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    @State var notifBadge =  UserDefaults.standard.integer(forKey: "notification_badge") as? Int ?? 0
    let notificationManager = LocalNotificationManager()
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // Sentry configurations start
        SentrySDK.start { options in
            options.dsn = "https://dee7678e170f4ece97d3605aa8064c98@o458124.ingest.sentry.io/5457751"
            options.debug = true // Enabled debug when first installing is always helpful
        }
        // Sentry Configuration ends
        
        //Remove this method to stop OneSignal Debugging
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

        //START OneSignal initialization code
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false]
      
        // Replace 'YOUR_ONESIGNAL_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
          appId: "b7d18529-7a7f-45e6-9ced-35f6e61f3b5b",
          handleNotificationAction: nil,
          settings: onesignalInitSettings)

        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

        // promptForPushNotifications will show the native iOS notification permission prompt.
        // We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 8)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
          print("User accepted notifications: \(accepted)")
        })
        //END OneSignal initializataion code

        
        // Apple Local Notification Configure Start
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter
            .current()
            .requestAuthorization(options: [.alert, .badge, .alert]) { granted, error in
                if granted == true && error == nil {
                    // We have permission!
                    print("App Delegate : Allow Notif")
                }
        }
        // END Apple Local Notification
        return true
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "MealPlanNotification" {
            print("Handling notifications with the Local Notification Identifier")
            
            notificationManager.resetBadgeValue()
        } else {
            print("Else")
            notificationManager.resetBadgeValue()
        }
        
        completionHandler()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("Did Become Active")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("Enter Foreground")
    }
}

@main
struct QuukSwiftUIApp: App {
    let persistenceContainer = PersistenceController.shared
    @Environment(\.scenePhase) var scenePhase
    @UIApplicationDelegateAdaptor private var appDelegate: AppDelegate

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceContainer.container.viewContext)
        }.onChange(of: scenePhase) { phase in
            switch phase {
            case .active:
                print("Application is Active")
                
                let badgeValue : Int = appDelegate.notificationManager.notifBadge
                
                if badgeValue != 0 {
                    appDelegate.notificationManager.resetBadgeValue()
                }
            case .inactive:
                print("Application is Inactive")
            case .background:
                print("Application enter Background")
            @unknown default:
                print("New App State not yet Introduce")
            }
        }
    }
}

final class ScreenCoordinator: ObservableObject {
    @Published var selectedTabItem: Int = 0
    @Published var selectedPushedItem : PushedItem?
    @Published var isPresented: Bool = false
    
    enum PushedItem: String {
        case firstScreen
        case secondSreen
    }
}
