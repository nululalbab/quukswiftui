//
//  HistoryViewModel.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 17/11/20.
//

import SwiftUI

class HistoryViewModel: ObservableObject {
    
    private var historyService : HistoryService!
    
    @ObservedObject var fetchdetailVM: FetchHistoryViewModel = FetchHistoryViewModel()
    
    @Published var history = historyPost()
    
    @Published var isNeverLogin = false
    @Published var isLoading = false
    
    init() {
        self.historyService = HistoryService()
//        self.history.History?.id = name
    }
    
    func Update() {
        if  DefaultsKeys.defaults.get(.bearer) == nil {
            self.isNeverLogin = true
        }else{
            
            CreateHistory()
        }
        
    }
    
    func CreateHistory() {

        self.historyService.postHistory(vm: self) { (result) in
            switch result {
            case .success(let result):
//                DispatchQueue.main.async {
                    
//                    self.fetchdetailVM = vm
//                    self.fetchdetailVM.history
                    print("post asd : \(result.message)")
                    
//                    self.fetchdetailVM.objectWillChange.send()
//                }
            case .failure(let error):
                print("Post History error")
            }
        }
    }

    func postToAPI(listCat:FetchedResults<ListCat>) {
        
        
        
//        for item in listCat.indices {
//            viewModel.id = listCat[item].wrappedCatTitle
            
//            self.history.id = listCat[item].wrappedCatTitle
//            self.history.userID = 3
//            self.history.createdAt = "\(Date())"
//            self.history.updatedAt = "\(Date())"
//            self.history.isDone = false
//
//            for haha in listCat[item].listCatArray {
//
//                self.history.ingredients = [PostHistoryIngredient(name: haha.wrappedListName, weight: Double(haha.wrappedListQty))]
                
//                viewModel.ingredients = [PostHistoryIngredient(name: haha.wrappedListName, weight: Double(haha.wrappedListQty))]
//            }
//        }
//
//        self.CreateHistory()
//        viewModel.CreateHistory()
        
    }
    
    func encoding(msg:String, alert:Bool){
        guard let encoded = try? JSONEncoder().encode(self.history) else {
            print("failed to encode")
            return
        }
        
        let url = URL(string: "http://inibukan.com/api/shop-list")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = encoded
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print("No data in response \(error?.localizedDescription ?? "Unknown error")")
                return
            }
            
            if let decodedOrder = try? JSONDecoder().decode(CreateHistoryResult.self, from: data) {
                var msg = "Your order for \(decodedOrder.History?.id)x\(decodedOrder.History?.ingredients) cupcakes is on its way!"
                var alert = true
            } else {
                print("Invalid response from server")
            }
            
        }.resume()
        
    }
    
}
