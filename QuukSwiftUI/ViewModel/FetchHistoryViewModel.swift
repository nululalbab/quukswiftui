//
//  FetchHistoryViewModel.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 17/11/20.
//

import Foundation
import SwiftUI
import Combine


class FetchHistoryViewModel: ObservableObject {
    
    private var historyservice : HistoryService!
    
    @Published var history = [History]()
    
    @Published var isLoading = false
    
    @Published var isNeverLogin = false
    
    @Published var isEmpty = false
    
    init() {
        self.historyservice = HistoryService()
    }
    
    func Fetch(){
        self.isEmpty = false
        if  DefaultsKeys.defaults.get(.bearer) == nil {
            self.isNeverLogin = true
        }else{
            self.isNeverLogin = false
            LoadFetch()
        }
    }
    
    func LoadFetch() {
        
        self.isLoading = true
        
        self.historyservice.fetchHistory(){ (results) in
            
            switch results {
            
            case .success(let result):
                
                if result.history?.count == 0 {
                    
                    DispatchQueue.main.async {
                        
                        self.isEmpty = true
                        
                        self.history.removeAll()
                    }
                }else{
                    DispatchQueue.main.async {
                        self.history.removeAll()
                        
                        self.history = result.history!.sorted{$0.createdAt! < $1.createdAt!}
                        
                        self.isEmpty = false
                    }
                }
                
                
            case .failure(let error):
                
                print("errror fetch history  :  \(error)")
            }
            
            DispatchQueue.main.async {
                
                self.isLoading = false
            }
        }
    }
}
