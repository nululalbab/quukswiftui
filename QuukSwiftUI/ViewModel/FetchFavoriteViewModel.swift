//
//  FetchFavoriteViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 21/10/20.
//

import Foundation
import SwiftUI
import Combine

class FetchFavoriteViewModel : ObservableObject {
    
    private var favoriteservice : FavoriteService!
    
    
    @Published var favorite = [Favorite]()
    
    @Published var isLoading = false
    
    @Published var isNeverLogin = false
    
    @Published var isEmpty = false
    
    init() {
        self.favoriteservice = FavoriteService()
    }
    
    
    func Fetch() {
        
        self.isEmpty = false 
        if  DefaultsKeys.defaults.get(.bearer) == nil {
            self.isNeverLogin = true
        }else{
            self.isNeverLogin = false
            LoadFetch()
        }
        
    }
    
    func LoadFetch() {
        
        self.isLoading = true
        
        self.favoriteservice.fetchFavorite(){ (results) in
            
            switch results {
            
            case .success(let result):
                if result.favorite?.count == 0 {
                    
                        self.isEmpty = true
                        
                } else {
                        self.favorite = result.favorite!
                        
                        self.isEmpty = false
                }
                
                
            case .failure(let error):
                
                print("errror fetch favorite  :  \(error)")
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0) {

                self.isLoading = false

            }
        }
    }
    
}
