//
//  ProfilePageViewViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 27/10/20.
//

import Foundation

class  ProfilePageViewViewModel: ObservableObject {
    
//    @Published var isNeverLogin : Bool
    
    @Published var username = ""
    @Published var phone = ""
    
    func Fetch(complition :  @escaping(Bool) ->() ) {
//
        if  DefaultsKeys.defaults.get(.bearer) != nil {
            
            LoadData()
            complition(false)
            
        }else{
            complition(true)
        }
        
    }
    
   private func LoadData() {
        
        self.username = DefaultsKeys.defaults.get(.username) ?? ""
        
        self.phone = DefaultsKeys.defaults.get(.phone) ?? ""
    
    }
    
}
