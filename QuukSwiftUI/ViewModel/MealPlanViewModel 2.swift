//
//  MealPlanViewModel.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 19/10/20.
//

import Foundation

class MealPlanViewModel: ObservableObject {
    
    private var mealPlanService : MealPlanService!
    
    var recipe_id: String = ""
    var recipe_name: String = ""
    var name: String = ""
    var datetime : String = ""
    var datetimeDate : Date = Date()
    
    init() {
        self.mealPlanService = MealPlanService()
    }
    
    func CreateMealPlan() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "y-MM-dd HH:mm"
        datetime = dateFormatter.string(from: datetimeDate)
        
        self.mealPlanService.createPlanService(vm: self) { (result) in
            switch result {

            case .success(let result):
                print("success create plan   : \(result) ")
                self.setNotification()
                
            case .failure(let error):

                print("Create meal plan errror   :  \(error)")
            }
        }
    }
    
    func setNotification() -> Void {
        let manager = LocalNotificationManager()
        manager.addNotification(
            title       : "Jadwal Memasak : \(name)",
            body        : "Saatnya memasak : \(recipe_name). \n \(recipe_id) ",
            scheduleDate: datetimeDate,
            recipeID    : recipe_id)
        
        print("Notif : ", manager)
        manager.schedule()
    }
}
