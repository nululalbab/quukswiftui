//
//  FetchMealPlanVM.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 03/11/20.
//

import Foundation
import SwiftUI
import Combine

class FetchMealPlanVM : ObservableObject {
    
    private var mealPlanService : MealPlanService!
    
    
    @Published var mealPlan = [MealPlan]()
    
    @Published var isLoading = false
    
    @Published var isNeverLogin = false
    
    @Published var isEmpty = false
    
    init() {
        self.mealPlanService = MealPlanService()
    }
    
    
//    func Fetch() {
//        self.isEmpty = false
//        if  DefaultsKeys.defaults.get(.bearer) == nil {
//            self.isNeverLogin = true
//        }else{
//            self.isNeverLogin = false
//            LoadFetch()
//        }
//
//    }
    
//    func LoadFetch() {
//
//        self.isLoading = true
//
//        self.mealPlanService.fetchMealPlan(){ (results) in
//            switch results {
//            case .success(let result):
//
//                if result.mealPlan.count == 0 {
//
//                    DispatchQueue.main.async {
//
//                        self.isEmpty = true
//
//                        self.mealPlan.removeAll()
//                    }
//                }else{
//                    DispatchQueue.main.async {
//                        self.mealPlan.removeAll()
//
//                        self.mealPlan = result.mealPlan
//                        self.isEmpty = false
//                    }
//                }
//
//
//            case .failure(let error):
//
//                print("errror fetch favorite  :  \(error)")
//            }
//
//            DispatchQueue.main.async {
//
//                self.isLoading = false
//            }
//        }
//    }
    
    func fetchDataWithHandler(selectedDate: String, sucess:Bool, completionHandler:@escaping (Bool)->Void) {
        self.isLoading = true
        
        if let date = selectedDate.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            self.mealPlanService.fetchMealPlanByDate(selectedDate: date){ (results) in
                switch results {
                case .success(let result):
                    
                    if result.mealPlan.count == 0 {
                        
                        DispatchQueue.main.async {
                            
                            self.isEmpty = true
                            
                            self.mealPlan.removeAll()
                            completionHandler(false)
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.mealPlan.removeAll()
                            
                            self.mealPlan = result.mealPlan
                            self.isEmpty = false
                            completionHandler(true)
                        }
                    }
                    
                    
                case .failure(let error):
                    
                    print("errror fetch favorite  :  \(error)")
                }
                
                DispatchQueue.main.async {
                    
                    self.isLoading = false
                    completionHandler(false)
                }
            }
            
        }
        
        
       
    }}

