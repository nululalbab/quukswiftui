//
//  FetchUserVIewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 14/10/20.
//

import Foundation

class FetchUserViewModel : ObservableObject {
    
    private var userservice : UserService!
    
    
    init() {
        self.userservice = UserService()
    }
    
    
   
    func fetchUserService(completion : @escaping(Bool) ->()) {
        self.userservice.fetchUser() { result in
            switch result {
            
            case .success(let result):
                DefaultsKeys.defaults.set(.name, to: result.user?.name ?? "")

                DefaultsKeys.defaults.set(.username, to: result.user?.username ?? "")
                
                DefaultsKeys.defaults.set(.email, to: result.user?.email ?? "")
                
                DefaultsKeys.defaults.set(.phone, to: result.user?.phoneNumber ?? "")
                
                DefaultsKeys.defaults.set(.gender, to: result.user?.gender ?? "")
                
                
                DispatchQueue.main.async {
                    
                    completion(true)
                    
                }

            case .failure(let error):
                
                print("errror fetchUser  :  \(error)")
            }
        }
    }
}
