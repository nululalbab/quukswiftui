//
//  LoginViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 07/10/20.
//

import Foundation

class LoginViewModel : ObservableObject {
    
    
    private var autservice : AuthService!
    
    
    private var fetchuserviewmodel  : FetchUserViewModel!
    
    @Published var isSuccess = false
    @Published var isLoading = false
    @Published var isError = false
    @Published var errormassage : String?
    
    init() {
        self.autservice = AuthService()
        self.fetchuserviewmodel = FetchUserViewModel()
    }
    
    var username : String = ""
    
    var password : String = ""
    
    func Login(completion : @escaping(Bool) ->() ) {
        self.isLoading = true
        
        self.autservice.loginService(vm: self) { (result) in
            switch result {
            
            case .success(let result):
                
                DefaultsKeys.defaults.set(.bearer, to: result.accessToken ?? "")
                
                self.fetchuserviewmodel.fetchUserService(completion: { result in
                    
                    if result {
                        DispatchQueue.main.async {
                            
                            completion(true)
                            
                        }
                    }
                })
                
            case .failure(let error):
             
                switch error {
                
                case .already(let already):
                    self.errormassage = already.message
                    
                case .badRequest(let badrequest):
                    self.errormassage = badrequest.message
                    
                case .unauthorized(let unauthorized):
                    self.errormassage = unauthorized.message
                    
                case .forbidden(let forbidden):
                    
                    self.errormassage = forbidden.message
                    
                case .internalServerError:
                    return
                case .requestTimeout:
                    return
                case .noInternetConnection:
                    return
                case .unknown(let unknown):
                    
                    self.errormassage = unknown.message
                }
                DispatchQueue.main.async {
                   
                    self.isError = true
                }

            }
            self.isLoading = false
          
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {

                self.isError = false

            }
        }
    }
    
}
