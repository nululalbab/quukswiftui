//
//  FavoriteViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 08/10/20.
//

import Foundation
import SwiftUI
import Combine

class UpdateFavoriteViewModel : ObservableObject {
    
    private var favoriteservice : FavoriteService!
    
    @ObservedObject var fetchdetailVM: FetchDetailViewModel = FetchDetailViewModel()
    
    @Published var favorite = FavoriteResult()
    
    @Published var isLoading = false
    
    @Published var isNeverLogin = false
    
    init() {
        self.favoriteservice = FavoriteService()
    }
    
    var url : String = ""
    
    func Update(vm : FetchDetailViewModel) {
        if  DefaultsKeys.defaults.get(.bearer) == nil {
            self.isNeverLogin = true
        }else{
            
            LoadUpdate(vm: vm)
        }
        
    }
    
    func LoadUpdate(vm : FetchDetailViewModel) {
        
        self.isNeverLogin = false
        self.isLoading = true
        
        self.favoriteservice.updateFavorite(vm: self) { (result) in
            switch result {
            
            case .success(let result):
                DispatchQueue.main.async {
                    
                    self.fetchdetailVM = vm
                    self.fetchdetailVM.recipe.isFavorite = result.recipe?.isFavorite
                    
                    self.fetchdetailVM.objectWillChange.send()
                }
            case .failure(let error):
                
                print("errror updateFavorite  :  \(error)")
            }
            
            DispatchQueue.main.async {
                
                self.isLoading = false
            }
        }
    }
    
}
