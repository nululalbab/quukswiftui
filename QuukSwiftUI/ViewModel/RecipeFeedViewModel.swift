//
//  RecipeFeedViewModel.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 06/10/20.
//

import Foundation

class RecipeFeedViewModel: ObservableObject {
    
    private var recipeFeedService : RecipeService!
    
    @Published var recipes = [RecipeWajibCoba]()
    
    init() {
        self.recipeFeedService = RecipeService()
    }
    
    var recipeName: String = ""
    
    func search() {
        if let query = self.recipeName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            fetchListBundleRecipe(by: query)
        }
    }
    
    func fetchListBundleRecipe(by query: String) {
        self.recipeFeedService.recipeFeedService(query:query) { result in
            
            switch result {
            case .success(let recipes):
                DispatchQueue.main.async {
                    self.recipes = recipes
                }
                
            case .failure(let error):
                print("errror on fetch  :  "+error.localizedDescription)
                
            }
            
        }
        
    }
    
    func listFeedRecipe() {
        if let query = self.recipeName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            fetchListFeedRecipe(by: query)
        }
    }
    
    func fetchListFeedRecipe(by query: String) {
        self.recipeFeedService.recipeFeedService(query : query) { result in
            
            switch result {
            case .success(let recipes):
                DispatchQueue.main.async {
                    self.recipes = recipes
                }
                
            case .failure(let error):
                print("errror fetchListFeedRecipe  :  "+error.localizedDescription)
                
            }
            
        }
        
    }
    
}
