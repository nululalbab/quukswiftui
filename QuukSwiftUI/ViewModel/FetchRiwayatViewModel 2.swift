//
//  FetchDetailRecipeViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 21/10/20.
//

import Foundation
import Combine

class FetchRiwayatViewModel : ObservableObject {
    
    private var recipeservice : RecipeService!
    
    @Published var recipe = DetailRecipeResult()
    
    @Published var inggridients = [RecipeIngredient]()
    
    init() {
        self.recipeservice = RecipeService()
    }
    
    func Fetch(id : String) {
        let keySplit = id.split(separator: "/")[3]
        fetchDetail(key: String(keySplit))
        
    }
    
    private func fetchDetail(key : String) {
        inggridients.removeAll()
//        self.recipeservice.FetchDetailRecipe(key: key) { (result) in
        self.recipeservice.FetchDetailRecipe(key: key){ (result) in
            switch result {
            case .success(let result):
                DispatchQueue.main.async {
                    self.recipe = result
                    print("succes inggridient detail count \(result.ingredients?.count)")
                    for data in result.ingredients!{
                        self.inggridients.append(data)
                    }
                }
            case .failure(let error):
                
//                switch error {
//                case .already(let already):
//                    already.message
//                default:
//                    <#code#>
//                }
                print("errror fetch detail   :  \(error)")
            }
        }
    }
}
