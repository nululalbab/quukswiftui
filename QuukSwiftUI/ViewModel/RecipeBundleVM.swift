//
//  RecipeFeedViewModel.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 06/10/20.
//

import Foundation

class RecipeBundleVM: ObservableObject {
    
    private var recipeBundleService : BundleService!
    
    @Published var bundles = [BundleData]()
    
    init() {
        self.recipeBundleService = BundleService()
    }
    
    func fetchListBundleRecipe() {
        self.recipeBundleService.recipeBundleService() { result in
            
            switch result {
            case .success(let bundles):
                DispatchQueue.main.async {
                    self.bundles = bundles
                }
            case .failure(let error):
                print("errror on fetch  :  "+error.localizedDescription)
            }
        }
    }
}
