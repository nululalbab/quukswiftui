//
//  historyPostViewModel.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 18/11/20.
//

import Foundation
import SwiftUI
import CoreData

struct HistoryDaftarBelanja: Identifiable {
    var id = UUID()
    var recipeTitle : String
    var recipeID : String
    var recipeDate : Date
    var listIngredients : [listIngredients]
}

struct listIngredients {
    var id: UUID
    var ingredientsName: String
    var ingredientsQty: Float
    var checked: Bool
}

class historyPostViewModel: ObservableObject {
    
    @Published var recipe = [HistoryDaftarBelanja]()
    
}
