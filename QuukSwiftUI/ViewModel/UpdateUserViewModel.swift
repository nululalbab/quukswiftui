//
//  UpdateUserViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 21/10/20.
//

import Foundation
import SwiftUI


class  UpdateuserViewModel : ObservableObject {
    
    
    private var userservice : UserService!
    
    
    init() {
        self.userservice = UserService()
    }
    
    @Published var isLoading = false
    
    @Published var isSuccess = false
    
    @Published var isError : String?
    
    @Published var username : String =  DefaultsKeys.defaults.get(.username) ?? ""
    
    var email : String =  DefaultsKeys.defaults.get(.email) ?? ""
    
    @Published var phone : String = DefaultsKeys.defaults.get(.phone) ?? ""
    
    var gender : String = DefaultsKeys.defaults.get(.gender) ?? ""
    
    
    func update(completion : @escaping (Bool)->()) {
        self.isLoading = true
        
        self.userservice.updateUser(vm: self){ (result) in
            switch result {
            
            case .success(let result):
                
                DefaultsKeys.defaults.set(.name, to: result.user?.name ?? "")
                
                DefaultsKeys.defaults.set(.username, to: result.user?.username ?? "")
                
                DefaultsKeys.defaults.set(.email, to: result.user?.email ?? "")
                
                DefaultsKeys.defaults.set(.phone, to: result.user?.phoneNumber ?? "")
                
                DefaultsKeys.defaults.set(.gender, to: result.user?.gender ?? "")
                
                DispatchQueue.main.async {
                    completion(true)
                }
                
                self.isSuccess = true
                
            case .failure(let error):
                
                print("update profile errror   :  \(error)")
            }
            
            self.isLoading = false
        }
    }
    
}
