//
//  ShoppingViewModel.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 21/10/20.
//

import SwiftUI
import CoreData

class ShoppingViewModel: ObservableObject {
    @FetchRequest(entity: Cart.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Cart.date, ascending: false)])
    var carts: FetchedResults<Cart>
    
    // Navigation
    @Published var tabBarIndex = 0
    
    // For NewData Sheet...
    @Published var isNewData = false
    
    //MARK: sum qty and portion in Cart [CoreData]
    func sumTotal(
        context:NSManagedObjectContext,
        portion:Float,
        qty:Float) -> Float
    {
        let sum = portion * qty
        try! context.save()
        
        return sum
    }
    
    //MARK: move tabBar
    func moveIndex(index:ShoppingViewModel) -> Int {
        
        let move = index.tabBarIndex
        
        return move
    }
    
    //MARK: add Recipe to Cart [CoreData]
    func writeData(context : NSManagedObjectContext, title:String, portion:Int, items: [RecipeIngredient], fetch:FetchedResults<Cart>){
        withAnimation{
            
            var dupTitle = [String]()
            
            for core in fetch.indices{
                dupTitle.append(fetch[core].wrappedTitle)
            }
            
            if dupTitle.contains(title) {
                // skip writedata if title is duplicate
            }
            
            else {
                for item in items.indices {
                    let newTask = Bahan(context: context)
                    //newTask.unit = intConverter(items: items, item: item) / Float(portion)
                    newTask.name = intRemover(items: items, item: item).capitalizingFirstLetter()
                    newTask.qty = (Float(items[item].weight ?? 999999999) / Float(portion))
                    newTask.check = true
                    newTask.id = UUID()
                    

                    newTask.origin = Cart(context: context)
                    newTask.origin?.title = title
                    newTask.origin?.recipeID = items[item].recipeID
                    newTask.origin?.portion = Float(portion)
                    newTask.origin?.date = Date()
                    newTask.origin?.id = UUID()

                    try! context.save()
                    
                    
                }
            }
            
        }
    }
    
    //MARK: ML Data for Classify Bahan
    func classifyBahan(
        bahan: String,
        context : NSManagedObjectContext
    ) -> String{
        
        let MLModel = IngredientsClassifier()
        var result = ""
        
        do{
            let classified = try MLModel.prediction(text: bahan)
            return classified.label
            
        }catch{
            return "ail"
        }
    }
    
    //MARK: regex to remove Digits
    func intRemover(items:[RecipeIngredient], item: Range<Array<RecipeIngredient>.Index>.Element ) -> String{
        
        let dump = items[item].text
        

        let result = dump?.replacingOccurrences(of: "[0-9-/]", with: "", options: .regularExpression)
        
        return result!.capitalizingFirstLetter()
    }
    

    
    func updateCheckbox(_ task:FetchedResults<Bahan>.Element, context : NSManagedObjectContext){
        task.check.toggle()
        try! context.save()
    }
    
    func updateQty(_ task:FetchedResults<Cart>.Element, context : NSManagedObjectContext, portion:Float){
        task.portion = portion
        try! context.save()
    }
    
    func moveToDaftarBelanja(
        context:NSManagedObjectContext,
        carts:FetchedResults<Cart>,
        object:ShoopingPlanList,
        listCat:FetchedResults<ListCat>
    ){
        
        var recipeID = ""
        
        for item in carts.indices {
            for bahan in carts[item].candyArray {
                
                if bahan.check == true {
//                    let cartItems = AddShoopingPlan(
//                        recipeTitle: bahan.origin?.wrappedTitle ?? "title empty",
//                        recipePortion: bahan.origin?.portion ?? 0,
//                        ingredientsName: bahan.wrappedBahanName, recipeID: bahan.origin!.recipeID!,
//                        ingredientsQty: bahan.wrappedBahanQty * bahan.origin!.portion,
//                        checked: bahan.check)
                    let cartItems = AddShoopingPlan(recipeTitle: bahan.origin?.wrappedTitle ?? "title empty", recipePortion: bahan.origin?.portion ?? 0, recipeID: bahan.origin?.recipeID ?? "", ingredientsName: bahan.wrappedBahanName, ingredientsQty: bahan.wrappedBahanQty * bahan.origin!.portion, checked: bahan.check)
                    
                    recipeID = bahan.origin!.recipeID!
                    
                    object.items.append(cartItems)
                }
                else{
                    print("check:\(bahan.check)")
                }
                
            }
        }
        
        let dick = Dictionary(grouping: object.items, by: {$0.ingredientsName}).mapValues{$0.reduce(0, {$0 + $1.ingredientsQty})}
    
        
        if listCat.isEmpty {
            for (key, value) in dick {
                
                let newTask = ListItem(context: context)
                newTask.name = key
                newTask.qty = value
                newTask.check = false
                newTask.id = UUID()
                
                newTask.origin = ListCat(context: context)
                newTask.origin?.title = classifyBahan(bahan: key, context: context)
                newTask.origin?.recipeID = recipeID
                newTask.origin?.date = Date()
                newTask.origin?.id = UUID()
                try! context.save()
            }
        }
        else {
            print("rencana belanja penuh")
        }
        object.items.removeAll()
    }
    
    func deleteCoreData(entity:String,
    context:NSManagedObjectContext){
        let asd : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entity)
        let request = NSBatchDeleteRequest(fetchRequest: asd)
        
        do {
            try context.execute(request)
            try context.save()
        } catch  {
            let error = error as NSError
            fatalError("Unresolved error: \(error)")
        }
        
    }
    
    func addToListCat(context:NSManagedObjectContext, entity:FetchedResults<ListCat>, namaBahan:String, QtyBahan:Float){
        let newTask = ListItem(context: context)
        newTask.name = namaBahan
        newTask.qty = QtyBahan
        newTask.check = false
        newTask.id = UUID()
        
        newTask.origin = ListCat(context: context)
        newTask.origin?.title = classifyBahan(bahan: namaBahan, context: context)
        newTask.origin?.recipeID = ""
        newTask.origin?.date = Date()
        newTask.origin?.id = UUID()
        try! context.save()
    }
    
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
