//
//  MealPlanViewModel.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 19/10/20.
//

import Foundation

class MealPlanViewModel: ObservableObject {
    
    private var mealPlanService : MealPlanService!
    
    var recipe_id: String = ""
    var recipe_name: String = ""
    var name: String = ""
    var datetime : String = ""
    var datetimeDate : Date = Date()
    
    @Published var isSuccess = false
    @Published var isLoading = false
    @Published var isError : String?
    @Published var isNeverLogin = false
    
    init() {
        self.mealPlanService = MealPlanService()
    }
    
    func CreateMealPlan(completion : @escaping(Bool) ->() ) {
        
        if  DefaultsKeys.defaults.get(.bearer) == nil {
            self.isNeverLogin = true
        }else{
            self.isLoading = true
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "y-MM-dd HH:mm"
            datetime = dateFormatter.string(from: datetimeDate)
            
            self.mealPlanService.createPlanService(vm: self) { (result) in
                switch result {
                
                case .success(let result):
                    DispatchQueue.main.async {
                        
                        self.setNotification()
                        completion(true)
                        
                    }
                    
                case .failure(let error):
                    switch error {
                    case .already(let already):
                        self.isError = already.message
                        
                    case .badRequest(let badrequest):
                        self.isError = badrequest.message
                        
                    case .unauthorized(let unauthorized):
                        self.isError = unauthorized.message
                        
                    case .forbidden(let forbidden):
                        
                        self.isError = forbidden.message
                        
                    case .internalServerError:
                        return
                    case .requestTimeout:
                        return
                    case .noInternetConnection:
                        return
                    case .unknown(let unknown):
                        
                        self.isError = unknown.message
                    }
                    
                    print("Create plan errror   :  \(error)")
                    
                    self.isLoading = false
                    completion(false)
                }
                
                DispatchQueue.main.async {
                    
                    self.isLoading = false
                }
            }
        }
    }
    
    func setNotification() -> Void {
        // Change the date format to Time format Only
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let time = dateFormatter.string(from: datetimeDate)
        
        let manager = LocalNotificationManager()
        manager.addNotification(
            title       : name,
            body        : "Jadwal menu kamu hari ini \(recipe_name) buat jam \(time) nih!",
            scheduleDate: datetimeDate,
            recipeID    : recipe_id)
        manager.schedule()
    }
}
