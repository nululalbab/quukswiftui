//
//  FavoriteViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 08/10/20.
//

import Foundation
import SwiftUI
import Combine

class UpdateRiwayatViewModel : ObservableObject {
    
    private var riwayatservice : RiwayatService!
    
    @ObservedObject var fetchdetailVM: FetchRiwayatViewModel = FetchRiwayatViewModel()
    
    @Published var favorite = FavoriteResult()
    
    @Published var isLoading = false
    
    @Published var isNeverLogin = false
    
    init() {
        self.riwayatservice = RiwayatService()
    }
    
    var url : String = ""
    
    func Update(vm : FetchRiwayatViewModel) {
        if  DefaultsKeys.defaults.get(.bearer) == nil {
            self.isNeverLogin = true
        }else{
            
            LoadUpdate(vm: vm)
        }
        
    }
    
    func LoadUpdate(vm : FetchRiwayatViewModel) {
        
        self.isNeverLogin = false
        self.isLoading = true
        
        self.riwayatservice.updateFavorite(vm: self) { (result) in
            switch result {
            
            case .success(let result):
                DispatchQueue.main.async {
                    
                    self.fetchdetailVM = vm
                    self.fetchdetailVM.recipe.isFavorite = result.recipe?.isFavorite
                    
                    self.fetchdetailVM.objectWillChange.send()
                }
            case .failure(let error):
               print("error \(error)")
            }
            
            DispatchQueue.main.async {
                
                self.isLoading = false
            }
        }
    }
    
}
