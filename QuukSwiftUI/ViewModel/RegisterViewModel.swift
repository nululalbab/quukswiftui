//
//  RegisterViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 07/10/20.
//

import Foundation

class  RegisterViewModel : ObservableObject {
    
    
    private var autservice : AuthService!
    @Published var isSuccess = false
    @Published var isLoading = false
    @Published var errormassage : String?
    @Published var isError = false
    
    init() {
        self.autservice = AuthService()
    }
    
    
    var name : String = ""
    
    var username : String = ""
    
    var email : String = ""
    
    @Published var password : String = ""
    
    @Published var password_confirmation : String = ""
    
    
    func Register(completion : @escaping(Bool) ->()) {
        self.isLoading = true
        self.autservice.registerService(vm: self) { (result) in
            switch result {
            
            case .success(let result):
                
                DispatchQueue.main.async {
                    
                    completion(true)
                }
                
            case .failure(let error):
                switch error {
                case .already(let already):
                    self.errormassage = already.message
                    
                case .badRequest(let badrequest):
                    self.errormassage = badrequest.message
                    
                case .unauthorized(let unauthorized):
                    self.errormassage = unauthorized.message
                    
                case .forbidden(let forbidden):
                    
                    self.errormassage = forbidden.message
                    
                case .internalServerError:
                    return
                case .requestTimeout:
                    return
                case .noInternetConnection:
                    return
                case .unknown(let unknown):
                    
                    self.errormassage = unknown.message
                }
                DispatchQueue.main.async {
                   
                    self.isError = true
                }
            }
            DispatchQueue.main.async {
                
                self.isLoading = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {

                self.isError = false

            }
            
        }
    }
    
}
