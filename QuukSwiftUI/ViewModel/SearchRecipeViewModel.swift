//
//  SearchRecipeViewModel.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 25/09/20.
//

import Foundation

class SearchRecipeViewModel: ObservableObject {
    
    private var searchrecipeservice : RecipeService!
    
    @Published var recipes = [Recipe]()
    @Published var calorie : String = ""
    
    @Published var isLoading = false
    
    @Published var isEmpty = false
    
    init() {
        self.searchrecipeservice = RecipeService()
    }
    
    var recipeName: String = ""
    
    
    func search() {
        
        self.recipes.removeAll()
        if let query = self.recipeName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            fetchRecipe(by: query, calorie: self.calorie)
        }
    }
    
    private func fetchRecipe(by query: String, calorie: String) {
        
        self.isEmpty = false
        self.isLoading = true
        
        self.searchrecipeservice.searchRecipeService(query: query, calorie: calorie) { result in
            
            switch result {
            case .success(let result):
                
                if result.count == 0 {
                    
                    self.isEmpty = true
                    
                }else{
                    self.isEmpty = false
                    DispatchQueue.main.async {
                        self.recipes = result
                    }
                }
                
            case .failure(let error):
                
                print("errror on search :  \(error)")
                
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                
                self.isLoading = false
                
            }
            
        }
        
    }
    
}
