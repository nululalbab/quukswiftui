//
//  ContentView.swift
//  QuukSwiftUI
//
//  Created by Najibullah Ulul Albab on 24/09/20.
//

import SwiftUI

struct ContentView: View {
    
    init() {
            UITableView.appearance().showsVerticalScrollIndicator = false
        }
    
    @ObservedObject var fetchuserVM : FetchUserViewModel =  FetchUserViewModel()
    @State var index = 0
    
    @State var selection: Tab = .belanja
    @State var curvePos : CGFloat = 0
    
    var body: some View {
        VStack(spacing: 0){
            Home(tab: $selection, curvePos: $curvePos)
        }
        .edgesIgnoringSafeArea(.top)
        .onAppear{
            self.fetchuserVM.fetchUserService { result in
                
            }
        }
    }
}

struct Home : View {
    @Binding var tab: Tab
    @Binding var curvePos : CGFloat
   
    var body : some View {
        
        ZStack(alignment: Alignment(horizontal: .center, vertical: .bottom), content: {
            
            // Navigation View
            if tab == .belanja{
                NavigationView {
                    daftarBelanjaTabView(tab: $tab, curvePos: $curvePos)
                }
                .tag(Tab.belanja)
            }
            else if tab == .resep{
                NavigationView {
                    resepTabView()
                }
                .tag(Tab.resep)
            }
            else if tab == .jadwalMenu{
                NavigationView {
                    CalendarViewPage(tab: $tab)
                }
                .tag(Tab.jadwalMenu)
            }
            else if tab == .profil{
                NavigationView {
                    punyakuTabView()
                        .padding(.bottom, 20)
                }
                .tag(Tab.profil)
            }
            
            // Tab Bar
            HStack {
                
                //Daftar Belanja
                GeometryReader{ g in
                    
                    VStack{
                        Button(action: {
                            withAnimation(.spring()){
                                self.tab = .belanja
                                self.curvePos = g.frame(in: .global).midX
                            }
                        }, label: {
                            Image("daftar_belanja")
                                .renderingMode(.template)
                                .resizable()
                                .foregroundColor(tab == .belanja ? Color("secondaryColor") : .white)
                                .frame(width: 25, height: 25)
                                .padding(.all, 15)
                        })
                        Text("Daftar Belanja")
                            .foregroundColor(tab == .belanja ? Color("secondaryColor") : .white)
                            .font(.system(size: 11))
                            .bold()
                            .offset(y:-20)
                    }
                    .frame(width: 78, height: 40)
                    .offset(x: -18)
                    .onAppear{
                        
                        DispatchQueue.main.async {
                        self.curvePos = g.frame(in: .global).midX
                        }
                    }
                }
                .frame(width: 40, height: 40)
                
                //Resep
                Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
                GeometryReader{ g in
                    
                    VStack{
                        Button(action: {
                            
                            withAnimation(.spring()){
                                self.tab = .resep
                                self.curvePos = g.frame(in: .global).midX
//                                self.curvePos = 55.0
                            }
                            
                        }, label: {
                            Image("resep")
                                .renderingMode(.template)
                                .resizable()
                                .foregroundColor(tab == .resep ? Color("secondaryColor") : .white)
                                .frame(width: 25, height: 25)
                                .padding(.all, 15)
//                                .background(Color.gray.clipShape(Circle()))
                        })
                        Text("Resep")
                            .foregroundColor(tab == .resep ? Color("secondaryColor") : .white)
                            .font(.system(size: 11))
                            .bold()
                            .offset(y:-20)
                            
                    }
                    .frame(width: 40, height: 40)
                }
                .frame(width: 40, height: 40)
                
                //Meal Plan
                Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
                GeometryReader{ g in
                    
                    VStack{
                        Button(action: {
                            
                            withAnimation(.spring()){
                                self.tab = .jadwalMenu
                                self.curvePos = g.frame(in: .global).midX
                            }
                            
                        }, label: {
                            Image(systemName:"calendar")
                                .renderingMode(.template)
                                .resizable()
                                .foregroundColor(tab == .jadwalMenu ? Color("secondaryColor") : .white)
                                .frame(width: 20, height: 20)
                                .padding(.all, 15)
//                                .background(Color.gray.clipShape(Circle()))
                        })
                        Text("Jadwal Menu")
                            .foregroundColor(tab == .jadwalMenu ? Color("secondaryColor") : .white)
                            .font(.system(size: 11))
                            .bold()
                            .offset(y:-17)
                    }
                    .frame(width: 80, height: 40)
                    .offset(x: -19)
                }
                .frame(width: 40, height: 40)
                
                //Punyaku
                Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
                GeometryReader{ g in
                    
                    VStack{
                        Button(action: {
                            
                            withAnimation(.spring()){
                                self.tab = .profil
                                self.curvePos = g.frame(in: .global).midX
                            }
                            
                        }, label: {
                            Image("punyaku")
                                .renderingMode(.template)
                                .resizable()
                                .foregroundColor(tab == .profil ? Color("secondaryColor") : .white)
                                .frame(width: 25, height: 25)
                                .padding(.all, 15)
//                                .background(Color.gray.clipShape(Circle()))
                        })
                        Text("Profile")
                            .foregroundColor(tab == .profil ? Color("secondaryColor") : .white)
                            .font(.system(size: 11))
                            .bold()
                            .offset(y:-20)
                    }
                    .frame(width: 47, height: 40)
                    .offset(x: -3)
                }
                .frame(width: 40, height: 40)
            }
            .padding(.horizontal, UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0 ? 25 : 35)
            .padding(.bottom, UIApplication.shared.windows.first?.safeAreaInsets.bottom == 0 ? 8 : UIApplication.shared.windows.first?.safeAreaInsets.bottom)
            .padding(.top, 8)
            .background(Color("primaryColor"))
        })
        .edgesIgnoringSafeArea(.all)
    }
}
//
//struct CShape : Shape {
//
//    var curvePos : CGFloat
//
//    //animating path
//    var animatableData: CGFloat{
//        get{return curvePos}
//        set{curvePos = newValue}
//    }
//
//    func path(in rect: CGRect) -> Path {
//
//        return Path { path in
//            path.move(to: CGPoint(x: 0, y: 0))
//            path.addLine(to: CGPoint(x: 0, y: rect.height))
//            path.addLine(to: CGPoint(x: rect.width, y: rect.height))
//            path.addLine(to: CGPoint(x: rect.width, y: 0))
//
//            // adding Curve
//            path.move(to: CGPoint(x: curvePos+35, y: 0))
//            path.addQuadCurve(to: CGPoint(x: curvePos-35, y: 0), control: CGPoint(x: curvePos, y: -30))
//        }
//    }
//}
