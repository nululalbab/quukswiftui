//
//  HandleSlideOverCard.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 29/09/20.
//

import SwiftUI

struct HandleSlideOverCard : View {
    private let handleThickness = CGFloat(5.0)
    
    var body: some View {
        RoundedRectangle(cornerRadius: handleThickness / 2.0)
            .frame(width: 80, height: 5)
            .foregroundColor(Color.white)
            .padding(10)
    }
}
