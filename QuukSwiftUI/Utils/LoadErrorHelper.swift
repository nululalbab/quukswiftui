//
//  LoadErrorHelper.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 07/10/20.
//

import Foundation

enum LoadErrorHelper: Error {
    case badRequest(apiErrors: ApiError)
    case unauthorized(apiError: ApiError)
    case forbidden(apiError: ApiError)
    case already(already : ApiErrorDataAlready)
    case internalServerError
    case requestTimeout
    case noInternetConnection
    case unknown(apiError: ApiError)
}
