//
//  JsonOnboard.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 04/11/20.
//

import Foundation

let beginningOnboardsJson = """
[
    {
        "description": "Klik di sini untuk mencari resep berdasarkan bahan dan kandungan gizi.",
        "previousButtonTitle": "",
        "nextButtonTitle": "Lanjut"
    }

]
"""
//    {
//        "description": "Klik di sini untuk mencari resep berdasarkan kategori yang ada.",
//        "previousButtonTitle": "",
//        "nextButtonTitle": "Lanjut"
//    },
//    {
//        "description": "Klik di sini untuk melihat rencana belanja, membuat daftar belanja, dan melakukan pemesanan online.",
//        "previousButtonTitle": "",
//        "nextButtonTitle": "Lanjut"
//    },
//    {
//        "description": "Klik di sini untuk melihat profil, riwayat belanja, dan pesanan online yang telah kamu lakukan.",
//        "previousButtonTitle": "",
//        "nextButtonTitle": "Selesai"
//    }
