//
//  UserDefault.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 08/10/20.
//

import Foundation


class DefaultsKeys {
    static let defaults = UserDefaults.standard
}
final class DefaultsKey<T>: DefaultsKeys {
    let value: String

    init(_ value: String) {
        self.value = value
    }
}

extension UserDefaults {
    func get<T>(_ key: DefaultsKey<T>) -> T? {
        return object(forKey: key.value) as? T
    }

    func set<T>(_ key: DefaultsKey<T>, to value: T) {
        set(value, forKey: key.value)
    }
    
    
}

//Mark : - Bearer
extension DefaultsKeys {
    static let bearer = DefaultsKey<String>("bearer")
}


//Mark : - Profile
extension DefaultsKeys {
    static let name = DefaultsKey<String>("name")
    static let username = DefaultsKey<String>("username")
    static let email = DefaultsKey<String>("email")
    static let phone = DefaultsKey<String>("phone")
    static let gender = DefaultsKey<String>("gender")
}

//Mark : - OnboadBiginning
extension DefaultsKeys {
    static let beginning = DefaultsKey<Bool>("beginning")
}
