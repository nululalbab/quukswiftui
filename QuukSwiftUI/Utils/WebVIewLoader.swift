//
//  WebVIewLoader.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 02/10/20.
//

import Foundation
import SwiftUI
import WebKit

struct WebViewLoader : UIViewRepresentable  {
   
    var url : String
    
    func makeUIView(context : Context) -> WKWebView {
        guard let url = URL(string : self.url) else {
            return WKWebView()
        }
        
        let request = URLRequest(url: url)
        let wkWebView = WKWebView()
        wkWebView.load(request)
        return wkWebView
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        
    }
    
}
