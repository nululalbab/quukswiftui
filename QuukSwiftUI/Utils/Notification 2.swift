//
//  CreateNotification.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 08/11/20.
//

import UserNotifications
import SwiftUI

struct Notification {
    var id: String
    var title: String
    var body: String
    var scheduleDate: Date
    var recipeID : String
}

class LocalNotificationManager {
    var notifications = [Notification]()
    
    func schedule() -> Void {
          UNUserNotificationCenter.current().getNotificationSettings { settings in
              switch settings.authorizationStatus {
              case .notDetermined:
                  self.requestPermission()
              case .denied:
                  self.directToSettings()
              case .authorized, .provisional:
                  self.scheduleNotifications()
              default:
                  break
              }
          }
      }
    
    func requestPermission() -> Void {
        UNUserNotificationCenter
            .current()
            .requestAuthorization(options: [.alert, .badge, .alert]) { granted, error in
                if granted == true && error == nil {
                    // We have permission!
                    self.scheduleNotifications()
                }
        }
    }
    
    func scheduleNotifications() -> Void {
        for notification in notifications {
            
            let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: notification.scheduleDate)
            _ = dateComponents.year ?? 0
            _ = dateComponents.month ?? 0
            _ = dateComponents.day ?? 0
            _ = dateComponents.hour ?? 0
            _ = dateComponents.minute ?? 0
            
            let content = UNMutableNotificationContent()
            content.title = notification.title
            content.body = notification.body
            content.badge = 1
            
            let identifier = "MealPlanNotification"
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
//            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { error in
                guard error == nil else { return }
                print("Scheduling notification with id: \(notification.id)")
            }
        }
        
    }
    
    func addNotification(title: String, body: String, scheduleDate: Date, recipeID: String) -> Void {
        notifications.append(Notification(id: UUID().uuidString, title: title, body: body, scheduleDate: scheduleDate, recipeID: recipeID))
    }
    
    func directToSettings() {
//        let alertController = UIAlertController(
//                                title: "Please Enable Notification",
//                                message: "Our magic work with notification. We will help you to stay focus with this.",
//                                preferredStyle: .alert)
//        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
//            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
//                return
//            }
//            if UIApplication.shared.canOpenURL(settingsUrl) {
//                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
//                })
//            }
//        }
//        alertController.addAction(settingsAction)
//
//        DispatchQueue.main.async {
//            self.present(alertController, animated: true, completion: nil)
//        }
    }
    

}
