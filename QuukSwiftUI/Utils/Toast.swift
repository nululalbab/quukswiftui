//
//  Toast.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 23/10/20.
//

//import ToastUI
//import SwiftUI
//
//struct LoadingToast: View {
//    
//    @Binding var presentingToast: Bool
//    
//    var body: some View {
//        toast(isPresented: $presentingToast) {
//            print("Toast dismissed")
//        } content: {
//            ToastView("Loading...")
//                .toastViewStyle(IndefiniteProgressToastViewStyle())
//        }
//    }
//}
