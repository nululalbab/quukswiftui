//
//  HTTPCode.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 07/10/20.
//

import Foundation
struct HTTPCode {
    static let OK_200 = 200
    static let PAYMENT_IS_CREATED = 201
    static let NO_CONTENT_204 = 204
    static let BAD_REQUEST_400 = 400
    static let UNAUTHORIZED_401 = 401
    static let FORBIDDEN_403 = 403
    static let NOT_FOUND_404 = 404
    static let EVENT_ALREADY_CREATED_422 = 422
    static let SERVER_ERROR_500 = 500
}
