//
//  MealPlanModel.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 15/10/20.
//

import Foundation

struct CreatePlanResult: Codable {
    let status: Bool?
    let message: String
    let mealPlan: NewMealPlan
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case mealPlan = "bundle"
    }
}

// MARK: - Bundle
struct NewMealPlan: Codable {
    let userID: Int?
    let recipe_id: String?
    let name: String?
    let datetime: String?
    let id: String?
    let updatedAt: String?
    let createdAt: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case recipe_id = "recipe_id"
        case name = "name"
        case datetime = "datetime"
        case id = "id"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
    }
}

// MARK: - MealPlanPost
struct CreatePlanPost : Codable {
    let recipeID : String?
    
    let name : String?
    
    let datetime : String?
    
    enum CodingKeys: String, CodingKey {
        case recipeID = "recipe_id"
        case name = "name"
        case datetime = "datetime"
    }
    
}

extension CreatePlanPost{
    
    init?(_ vm: MealPlanViewModel?) {
        guard let recipe_id = vm?.recipe_id,

              let name = vm?.name,

              let datetime = vm?.datetime

        else  {
            return nil
        }

        self.recipeID = recipe_id
        self.name = name
        self.datetime = datetime
    }
    
}

// MARK: - FetchMealPlanResult
struct FetchMealPlanResult: Codable {
    let status: Bool
    let message: String
    let mealPlan: [MealPlan]
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case mealPlan = "plans"
    }
}

// MARK: - Plan
struct MealPlan: Codable {
    let id: String
    let userID: Int
    let recipeID: String
    let name: String
    var datetime: String
    let createdAt: String
    let updatedAt: String
    let recipe: RecipeMealPlan

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case userID = "user_id"
        case recipeID = "recipe_id"
        case name = "name"
        case datetime = "datetime"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case recipe = "recipe"
    }
}

extension MealPlan {

//    static func mock(withDate date: Date) -> Visit {
//        Visit(tagColor: Color("secondaryColor"),
//              menuName: UserDefaults.standard.string(forKey: "meal_title") ?? "Unknown user",
//              arrivalDate: date,
//              departureDate: date.addingTimeInterval(60*60))
//    }
    static func mocks(start: Date, end: Date) -> [MealPlan] {
        currentCalendar.generateVisits(
            start: start,
            end: end)
    }

}

fileprivate let planCountRange = 1...20

struct Result {
    var id = UUID()
    var score: Int
}

private extension Calendar {

    func generateVisits(start: Date, end: Date) -> [MealPlan] {
        
        let fetchMealPlanVM : FetchMealPlanVM = FetchMealPlanVM()
        
        let mealPlans : [MealPlan] = [MealPlan]()
        var results = [Result(score: 8), Result(score: 5), Result(score: 10)]

        
        var plans = [MealPlan]()
        
        var temp = String()

//        fetchMealPlanVM.Fetch()
        enumerateDates(
            startingAfter: start,
            matching: .everyDay,
            matchingPolicy: .nextTime) { date, _, stop in
            if let date = date {
                if date < end {
                    
                    
                    for result in fetchMealPlanVM.mealPlan {
//                        plans.append(result)
                        
//                        print("Array : ", plans)
                    }
                    
//                    ForEach(results, id: \.id) { result in
//                        plans.append(result.score)
//                        Text("Result: \(result.score)")
//                    }
                    
//                    for result in results {
//                        print("result : ",result)
//                        results.append(result)
//                    }
                        
//                    for _ in 0..<Int.random(in: planCountRange) {
//                        plans.append(.mock(withDate: date))
//                    }
                } else {
                    stop = true
                }
            }
        }

        return plans
    }

}

// MARK: - Recipe
struct RecipeMealPlan: Codable {
    let id: String
    let label: String
    let source: String
    let uri: String
    let url: String
    let image: String
    let yield: Int
    let calories: Double
    let fats: Double
    let proteins: Double
    let carbo: Double
    let createdAt: String
    let updatedAt: String
    let isFavorite: Bool
    let ingredients: [IngredientMealPlan]

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case source = "source"
        case uri = "uri"
        case url = "url"
        case image = "image"
        case yield = "yield"
        case calories = "calories"
        case fats = "fats"
        case proteins = "proteins"
        case carbo = "carbo"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavorite = "is_favorite"
        case ingredients = "ingredients"
    }
}


// MARK: - Ingredient
struct IngredientMealPlan: Codable {
    let recipeID: String
    let text: String
    let weight: Double
    let image: String?

    enum CodingKeys: String, CodingKey {
        case recipeID = "recipe_id"
        case text = "text"
        case weight = "weight"
        case image = "image"
    }
}

//extension MealPlan {
//
//    static func mock(withDate date: Date) -> Visit {
//        MealPlan(tagColor: Color("secondaryColor"),
//              menuName: UserDefaults.standard.string(forKey: "meal_title") ?? "Unknown user",
//              arrivalDate: date,
//              departureDate: date.addingTimeInterval(60*60))
//    }
//
//    static func mocks(start: Date, end: Date) -> [Visit] {
//        currentCalendar.generateVisits(
//            start: start,
//            end: end)
//    }
//
//}

//fileprivate let visitCountRange = 1...20

//private extension Calendar {
//
//    func generateVisits(start: Date, end: Date) -> [Visit] {
//        var visits = [Visit]()
//
//        enumerateDates(
//            startingAfter: start,
//            matching: .everyDay,
//            matchingPolicy: .nextTime) { date, _, stop in
//            if let date = date {
//                if date < end {
//                    for _ in 0..<Int.random(in: visitCountRange) {
//                        visits.append(.mock(withDate: date))
//                    }
//                } else {
//                    stop = true
//                }
//            }
//        }
//
//        return visits
//    }
//
//}

//fileprivate let colorAssortment: [Color] = [.turquoise, .forestGreen, .darkPink, .darkRed, .lightBlue, .salmon, .military]
//
//private extension Color {
//
//    static var randomColor: Color {
//        let randomNumber = arc4random_uniform(UInt32(colorAssortment.count))
//        return colorAssortment[Int(randomNumber)]
//    }
//
//}
//
//private extension Color {
//
//    static let turquoise = Color(red: 24, green: 147, blue: 120)
//    static let forestGreen = Color(red: 22, green: 128, blue: 83)
//    static let darkPink = Color(red: 179, green: 102, blue: 159)
//    static let darkRed = Color(red: 185, green: 22, blue: 77)
//    static let lightBlue = Color(red: 72, green: 147, blue: 175)
//    static let salmon = Color(red: 219, green: 135, blue: 41)
//    static let military = Color(red: 117, green: 142, blue: 41)
//
//}
//
//fileprivate extension Color {
//
//    init(red: Int, green: Int, blue: Int) {
//        self.init(red: Double(red)/255, green: Double(green)/255, blue: Double(blue)/255)
//    }
//
//}
