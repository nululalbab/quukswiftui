//
//  Riwayat.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 17/11/20.
//

import Foundation

// MARK: - CreateHistoryResult
struct CreateHistoryResult: Codable {
    
    var status: Bool?
    var message: String?
    var History: NewHistoryPost?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case History = "list"
    }
}

// MARK: - NewHistoryPost
struct NewHistoryPost: Codable {
    var id: String?
    var userID: Int?
    var createdAt: String?
    var updatedAt: String?
    var isDone: Bool?
    var ingredients: [PostHistoryIngredient]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case userID = "user_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isDone = "is_done"
        case ingredients = "ingredients"
    }
}

// MARK: - PostHistoryIngredient
struct PostHistoryIngredient: Codable {
    init(name:String,weight:Double) {
        self.name = name
        self.weight = weight
    }
    
    var name: String
    var weight: Double
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case weight = "weight"
    }
}



struct historyPost:Codable {

    var recipes = [String]()
    var ingredients = [PostHistoryIngredient]()
}

extension historyPost{
    init?(_ vm: HistoryViewModel){
        self.recipes = vm.history.recipes
        self.ingredients = vm.history.ingredients
    }
}

//extension NewHistoryPost{
//
//    init?(_ vm: HistoryViewModel?) {
//        guard
//            let id = vm?.id,
//            let user_id = vm?.userID,
//            let created_at = vm?.createdAt,
//            let updated_at = vm?.updatedAt,
//            let is_done = vm?.isDone,
//            let ingredients = vm?.ingredients
//
//        else  {
//            return nil
//        }
//
//        self.id = id
//        self.userID = user_id
//        self.createdAt = created_at
//        self.updatedAt = updated_at
//        self.isDone = is_done
//        self.ingredients = ingredients
//    }
//
//}

// MARK: - FetchHistory
struct FetchHistory: Codable {
    var status: Bool?
    var message: String?
    var history: [History]?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case history = "list"
    }
}

// MARK: - History
struct History: Codable {
    var id: String?
    var label: String?
    var source: String?
    var uri: String?
    var url: String?
    var image: String?
    var yield: Int?
    var calories, fats, proteins, carbo: Double?
    var createdAt: String?
    var updatedAt: String?
    var isFavorite: Bool?
    var ingredients: [FetchIngredientHistory]?
    var pivot: Pivot?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case source = "source"
        case uri = "uri"
        case url = "url"
        case image = "image"
        case yield = "yield"
        case calories = "calories"
        case fats = "fate"
        case proteins = "proteins"
        case carbo = "carbo"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavorite = "is_favorite"
        case ingredients = "ingredients"
        case pivot = "pivot"
    }
}

// MARK: - FetchIngredientHistory
struct FetchIngredientHistory: Codable {
    var recipeID, text: String
    var weight: Double
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case recipeID = "recipe_id"
        case text, weight, image
    }
}

// MARK: - Pivot
struct Pivot: Codable {
    var shopListID, recipeID: String
    
    enum CodingKeys: String, CodingKey {
        case shopListID = "shop_list_id"
        case recipeID = "recipe_id"
    }
}



