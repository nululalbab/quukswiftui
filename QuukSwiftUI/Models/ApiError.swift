//
//  ApiError.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 07/10/20.
//

import Foundation

 struct ApiError: Codable {
   
    let error: String?
    let errorDescription: String?
    let hint: String?
    let message: String?

    enum CodingKeys: String, CodingKey {
        case error = "error"
        case errorDescription = "error_description"
        case hint = "hint"
        case message = "message"
    }
}


// MARK: - APIError
struct ApiErrorDataAlready: Codable {
    let message: String?
    let errors: Errors?

    enum CodingKeys: String, CodingKey {
        case message = "message"
        case errors = "errors"
    }
}

// MARK: - Errors
struct Errors: Codable {
    let email: [String]?
    let username: [String]?

    enum CodingKeys: String, CodingKey {
        case email = "email"
        case username = "username"
    }
}
