//
//  Register.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 07/10/20.
//

import Foundation
// MARK: - Registerresult
struct RegisterResult: Codable {
    let success: Bool?
    let message: String?
    let user: User?

    enum CodingKeys: String, CodingKey {
        case success = "success"
        case message = "message"
        case user = "user"
    }
}

// MARK: - User
struct User: Codable {
    let name: String?
    let username: String?
    let email: String?
    let updatedAt: String?
    let createdAt: String?
    let id: Int?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case username = "username"
        case email = "email"
        case updatedAt = "updated_at"
        case createdAt = "created_at"
        case id = "id"
    }
}


// MARK: - RegisterPost

struct RegisterPost : Codable {
    let name : String?
    
    let username : String?
    
    let email : String?
    
    let password : String?
    
    let password_confirmation : String?
    
    
}

extension RegisterPost{
    
    
    init?(_ vm: RegisterViewModel?) {
        
        guard let name = vm?.name,
              
              let username = vm?.username,
              
              let email = vm?.email,
              
              let password = vm?.password,
              
              let password_confirmation = vm?.password_confirmation
              
        
        else  {
            return nil
        }
        
        self.name = name
        self.username = username
        self.email = email
        self.password = password
        self.password_confirmation = password_confirmation
        
    }
    
}

