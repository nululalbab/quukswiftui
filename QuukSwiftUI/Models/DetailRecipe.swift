//
//  DetailRecipe.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 21/10/20.
//

import Foundation

// MARK: - DetailRecipeResult
struct DetailRecipeResult: Codable {
    init() {
        
    }
    
    var id: String?
    var label: String?
    var uri: String?
    var url : String?
    var image: String?
    var servings: Int?
    var calories: Double?
    var fats: Double?
    var proteins: Double?
    var carbo: Double?
    var createdAt: String?
    var updatedAt: String?
    var isFavorite: Bool?
    var ingredients: [RecipeIngredient]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case uri = "uri"
        case url = "url"
        case image = "image"
        case servings = "yield"
        case calories = "calories"
        case fats = "fats"
        case proteins = "proteins"
        case carbo = "carbo"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavorite = "is_favorite"
        case ingredients = "ingredients"
    }
}

// MARK: - Ingredient
struct RecipeIngredient: Codable {
    let id = UUID()
    let recipeID: String?
    let text: String?
    let weight: Double?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case recipeID = "recipe_id"
        case text = "text"
        case weight = "weight"
        case image = "image"
    }
}
