//
//  TabBarNavigation.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 09/11/20.
//

import Foundation

enum Tab {
    case belanja
    case resep
    case jadwalMenu
    case profil
}
