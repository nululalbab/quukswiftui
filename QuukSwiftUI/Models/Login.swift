//
//  Login.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 06/10/20.
//

import Foundation
// MARK: - LoginResult
struct LoginResult: Codable {
    let tokenType: String?
    let expiresIn: Int?
    let accessToken: String?
    let refreshToken: String?

    enum CodingKeys: String, CodingKey {
        case tokenType = "token_type"
        case expiresIn = "expires_in"
        case accessToken = "access_token"
        case refreshToken = "refresh_token"
    }
}

// MARK: - LoginPost
struct LoginPost: Codable {
    var email : String
    
    var password : String
}

extension LoginPost{
    
    
    init?(_ vm: LoginViewModel?) {
        
        guard let email = vm?.username,
              
              let password = vm?.password
              
        else  {
            return nil
        }
        
        self.email = email
        self.password = password
        
    }
    
}

