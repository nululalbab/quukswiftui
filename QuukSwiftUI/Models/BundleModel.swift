//
//  CategoryModel.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 28/09/20.
//

import Foundation

struct BundleModel: Codable {
    let current_page: Int?
    let data: [BundleData]?
    
    enum CodingKeys: String, CodingKey {
        case current_page = "current_page"
        case data = "data"
    }
}

struct BundleData: Codable {
    let id: Int
    let name: String
    let recipes: [Recipe]
    let image: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case recipes = "recipes"
        case image = "image_url"
    }
}
