//
//  User.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 14/10/20.
//

import Foundation

// MARK: - UserResult
struct UserResult: Codable {
    let status: Bool?
    let message: String?
    let user: UserData?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case message = "message"
        case user = "user"
    }
}

// MARK: - User
struct UserData: Codable {
    let id: Int?
    let name: String?
    let username: String?
    let email: String?
    let emailVerifiedAt: JSONNull?
    let phoneNumber: String?
    let gender: String?
    let createdAt: String?
    let updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case username = "username"
        case email = "email"
        case emailVerifiedAt = "email_verified_at"
        case phoneNumber = "phone_number"
        case gender = "gender"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}



// MARK: - RegisterPost

struct UserUpdatePost : Codable {

    let name : String?
    
    let username : String?
    
    let email : String?
    
    let phone : String?
    
    let gender : String?
    
    
}

extension UserUpdatePost{
    
    
    init?(_ vm: UpdateuserViewModel?) {
        
        guard  let username = vm?.username,
              
              let email = vm?.email,
              
              let phone = vm?.phone,
              
              let gender = vm?.gender
              
        
        else  {
            return nil
        }
        
        self.name = username
        self.username = username
        self.email = email
        self.phone = phone
        self.gender = gender
        
    }
    
}

