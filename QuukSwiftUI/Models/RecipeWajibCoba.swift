//
//  RecipeWajibCoba.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 17/11/20.
//

import SwiftUI


// MARK: - RecipeResult
struct RecipeWajibCobaResult: Codable {
    let q: String?
    let from: Int?
    let to: Int?
    let more: Bool?
    let count: Int?
    let hits: [HitWajibCoba]?

    enum CodingKeys: String, CodingKey {
        case q = "q"
        case from = "from"
        case to = "to"
        case more = "more"
        case count = "count"
        case hits = "hits"
    }
}

// MARK: - Hit
struct HitWajibCoba: Codable {
    let recipe: RecipeWajibCoba?
    let bookmarked: Bool?
    let bought: Bool?

    enum CodingKeys: String, CodingKey {
        case recipe = "recipe"
        case bookmarked = "bookmarked"
        case bought = "bought"
    }
}

// MARK: - Recipe
struct RecipeWajibCoba: Codable {
    let uri: String?
    let label: String?
    let image: String?
    let source: String?
    let url: String?
    let shareAs: String?
    let yield: Double?
    let dietLabels: [String]?
//    let healthLabels: [HealgitthLabel]?
//    let cautions: [String]?
    let ingredientLines: [String]?
    let ingredients: [IngredientWajibCoba]?
    let calories: Double?
    let totalWeight: Double?
    let totalTime: Double?
    let totalNutrients: [String: TotalWajibCoba]?
    let totalDaily: [String: TotalWajibCoba]?
//    let digest: [Digest]?

    enum CodingKeys: String, CodingKey {
        case uri = "uri"
        case label = "label"
        case image = "image"
        case source = "source"
        case url = "url"
        case shareAs = "shareAs"
        case yield = "yield"
        case dietLabels = "dietLabels"
//        case healthLabels = "healthLabels"
//        case cautions = "cautions"
        case ingredientLines = "ingredientLines"
        case ingredients = "ingredients"
        case calories = "calories"
        case totalWeight = "totalWeight"
        case totalTime = "totalTime"
        case totalNutrients = "totalNutrients"
        case totalDaily = "totalDaily"
//        case digest = "digest"
    }
}

// MARK: - Digest
struct DigestWajibCoba: Codable {
    let label: String?
    let tag: String?
    let schemaOrgTag: SchemaOrgTagWajibCoba?
    let total: Double?
    let hasRDI: Bool?
    let daily: Double?
    let unit: UnitWajibCoba?
    let sub: [DigestWajibCoba]?

    enum CodingKeys: String, CodingKey {
        case label = "label"
        case tag = "tag"
        case schemaOrgTag = "schemaOrgTag"
        case total = "total"
        case hasRDI = "hasRDI"
        case daily = "daily"
        case unit = "unit"
        case sub = "sub"
    }
}

enum SchemaOrgTagWajibCoba: String, Codable {
    case carbohydrateContent = "carbohydrateContent"
    case cholesterolContent = "cholesterolContent"
    case fatContent = "fatContent"
    case fiberContent = "fiberContent"
    case proteinContent = "proteinContent"
    case saturatedFatContent = "saturatedFatContent"
    case sodiumContent = "sodiumContent"
    case sugarContent = "sugarContent"
    case transFatContent = "transFatContent"
}

enum UnitWajibCoba: String, Codable {
    case empty = "%"
    case g = "g"
    case kcal = "kcal"
    case mg = "mg"
    case µg = "µg"
}

//enum HealthLabel: String, Codable {
//    case alcoholFree = "Alcohol-Free"
//    case peanutFree = "Peanut-Free"
//    case sugarConscious = "Sugar-Conscious"
//    case treeNutFree = "Tree-Nut-Free"
//    case vegan = "Vegan"
//    case vegetarian = "Vegetarian"
//}

// MARK: - Ingredient
struct IngredientWajibCoba: Codable {
    let text: String?
    let weight: Double?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case text = "text"
        case weight = "weight"
        case image = "image"
    }
}

// MARK: - Total
struct TotalWajibCoba: Codable {
    let label: String?
    let quantity: Double?
    let unit: UnitWajibCoba?

    enum CodingKeys: String, CodingKey {
        case label = "label"
        case quantity = "quantity"
        case unit = "unit"
    }
}
