//
//  ShoppingModel.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 06/10/20.
//

import Foundation
import SwiftUI
import CoreData

struct AddShoopingPlan: Identifiable {
    var id = UUID()
    var recipeTitle : String
    var recipePortion : Float
    var recipeID : String
    
    var ingredientsName: String
    var ingredientsQty: Float
    var checked: Bool
}

class ShoopingPlanList: ObservableObject {
    @Published var items = [AddShoopingPlan]()
}

////////////////////


struct NewRecipes: Identifiable {
    let id = UUID()
    let ingredientsTitle: String
    let ingredientsPortion: String
    let ingredientsQty: Int
    var newResep: [Ingredients]?
    
    //new ax
    static let list1 = NewRecipes(ingredientsTitle: "Bebek Goreng", ingredientsPortion: "3", ingredientsQty: 1, newResep: [Ingredients.apple,Ingredients.apple])
    static let list2 = NewRecipes(ingredientsTitle: "Bebek Purnama", ingredientsPortion: "3", ingredientsQty: 1, newResep: [Ingredients.apple,Ingredients.apple])
    static let list3 = NewRecipes(ingredientsTitle: "Bebek Anjay", ingredientsPortion: "3", ingredientsQty: 1, newResep: [Ingredients.apple,Ingredients.apple])
}


struct Ingredients: Identifiable {
    let id = UUID()
    let ingredientsName: String
    let ingredientsUnit: String
    let checked: Bool
    var items: [Ingredients]?
    
    // some example websites
    static let apple = Ingredients(ingredientsName: "Daging Sapi giling", ingredientsUnit: "3 gr", checked: false)
    static let tepung = Ingredients(ingredientsName: "Tepung Roti", ingredientsUnit: "50 gr", checked: false)
    static let telur = Ingredients(ingredientsName: "Telur", ingredientsUnit: "1 butir", checked: false)
    
    // some example groups
    static let example2 = Ingredients(ingredientsName: "Chesse Burger", ingredientsUnit: "3 gr", checked: false, items: [Ingredients.apple,Ingredients.apple])
    
}

struct Units: Identifiable {
    let id = UUID()
    let unitName: String
    let checked: Bool
    var items: [Units]?
    
    // some example websites
    static let gram = Units(unitName: "Gram", checked: false)
    static let kilogram = Units(unitName: "Kilogram", checked: false)
    static let one = Units(unitName: "One", checked: false)
    static let mililiter = Units(unitName: "Mililiter", checked: false)
    static let liter = Units(unitName: "Liter", checked: false)
    static let sdt = Units(unitName: "Sdt", checked: false)
    static let sdm = Units(unitName: "Sdm", checked: false)
    static let cup = Units(unitName: "Cup", checked: false)
    static let buah = Units(unitName: "Buah", checked: false)
    static let butir = Units(unitName: "Butir", checked: false)
    static let ikat = Units(unitName: "Ikat", checked: false)
    static let potong = Units(unitName: "Potong", checked: false)
    static let secukupnya = Units(unitName: "Secukupnya", checked: false)
    
}

struct Kategories: Identifiable {
    let id = UUID()
    let katagoriName: String
    let checked: Bool
    var items: [Kategories]?
    
    // some example websites
   
    static let protein = Kategories(katagoriName: "Protein", checked: false)
    static let karbohidrat = Kategories(katagoriName: "Karbohidrat", checked: false)
    static let sayuran = Kategories(katagoriName: "Sayuran", checked: false)
    static let buahBuahan = Kategories(katagoriName: "Buah-buahan", checked: false)
    static let dairy = Kategories(katagoriName: "Dairy", checked: false)
    static let bumbu = Kategories(katagoriName: "Bumbu", checked: false)
    
}
