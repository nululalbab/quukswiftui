//
//  Favorite.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 08/10/20.
//

import Foundation

// MARK: - FavoriteResult
// MARK: - FavoriteResult
struct RiwayatResult: Codable {
    init() {

    }
    var status: Bool?
    var action: String?
    var message: String?
    var recipe: RiwayatResultRecipe?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case action = "action"
        case message = "message"
        case recipe = "recipe"
    }
}

// MARK: - Recipe
struct RiwayatResultRecipe: Codable {
    let id: String?
    let label: String?
    let uri: String?
    let image: String?
    let servings: Int?
    let calories: Double?
    let fats: Double?
    let proteins: Double?
    let carbo: Double?
    let createdAt: String?
    let updatedAt: String?
    let isFavorite: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case uri = "uri"
        case image = "image"
        case servings = "servings"
        case calories = "calories"
        case fats = "fats"
        case proteins = "proteins"
        case carbo = "carbo"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavorite = "is_favorite"
    }
}



struct RiwayatPost : Codable {
    let url : String?
}


extension RiwayatPost {
    init?(_ vm : UpdateRiwayatViewModel?) {

        guard let url = vm?.url else { return nil }

        self.url = url
    }
}



// MARK: - FetchFavoriteResult
struct FetchRiwayatResult: Codable {
    let message: String?
    let riwayat: [Riwayat]?

    enum CodingKeys: String, CodingKey {
        case message = "message"
        case riwayat = "riwayat"
    }
}

// MARK: - Favorite
struct Riwayat: Codable {
    let id: String?
    let label: String?
    let uri: String?
    let image: String?
    let servings: Int?
    let calories: Double?
    let fats: Double?
    let proteins: Double?
    let carbo: Double?
    let createdAt: String?
    let updatedAt: String?
    let isFavorite: Bool?
    let favorite: [JSONAny]?
    let ingredients: [IngredientFavorite]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case uri = "uri"
        case image = "image"
        case servings = "yield"
        case calories = "calories"
        case fats = "fats"
        case proteins = "proteins"
        case carbo = "carbo"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavorite = "is_favorite"
        case favorite = "favorite"
        case ingredients = "ingredients"
    }
}

// MARK: - Ingredient
struct IngredientRiwayat: Codable {
    let recipeID: String?
    let text: String?
    let weight: Double?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case recipeID = "recipe_id"
        case text = "text"
        case weight = "weight"
        case image = "image"
    }
}
