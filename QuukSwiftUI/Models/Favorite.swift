//
//  Favorite.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 08/10/20.
//

import Foundation

// MARK: - FavoriteResult
// MARK: - FavoriteResult
struct FavoriteResult: Codable {
    init() {
    }
    
    var status: Bool?
    var action: String?
    var message: String?
    var recipe: FavoriteResultRecipe?

    enum CodingKeys: String, CodingKey {
        case status = "status"
        case action = "action"
        case message = "message"
        case recipe = "recipe"
    }
}

// MARK: - Recipe
struct FavoriteResultRecipe: Codable {
    let id: String?
    let label: String?
    let uri: String?
    let image: String?
    let servings: Int?
    let calories: Double?
    let fats: Double?
    let proteins: Double?
    let carbo: Double?
    let createdAt: String?
    let updatedAt: String?
    let isFavorite: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case uri = "uri"
        case image = "image"
        case servings = "servings"
        case calories = "calories"
        case fats = "fats"
        case proteins = "proteins"
        case carbo = "carbo"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavorite = "is_favorite"
    }
}



struct FavoritePost : Codable {
    let url : String?
}


extension FavoritePost {
    init?(_ vm : UpdateFavoriteViewModel?) {

        guard let url = vm?.url else { return nil }

        self.url = url
    }
}



// MARK: - FetchFavoriteResult
struct FetchFavoriteResult: Codable {
    let message: String?
    let favorite: [Favorite]?

    enum CodingKeys: String, CodingKey {
        case message = "message"
        case favorite = "favorite"
    }
}

// MARK: - Favorite
struct Favorite: Codable {
    let id: String?
    let label: String?
    let uri: String?
    let image: String?
    let servings: Int?
    let calories: Double?
    let fats: Double?
    let proteins: Double?
    let carbo: Double?
    let createdAt: String?
    let updatedAt: String?
    let isFavorite: Bool?
    let favorite: [JSONAny]?
    let ingredients: [IngredientFavorite]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case label = "label"
        case uri = "uri"
        case image = "image"
        case servings = "yield"
        case calories = "calories"
        case fats = "fats"
        case proteins = "proteins"
        case carbo = "carbo"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavorite = "is_favorite"
        case favorite = "favorite"
        case ingredients = "ingredients"
    }
}

// MARK: - Ingredient
struct IngredientFavorite: Codable {
    let recipeID: String?
    let text: String?
    let weight: Double?
    let image: String?

    enum CodingKeys: String, CodingKey {
        case recipeID = "recipe_id"
        case text = "text"
        case weight = "weight"
        case image = "image"
    }
}
