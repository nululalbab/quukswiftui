//
//  daftarBelanjaTabView.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 28/09/20.
//

import SwiftUI

struct daftarBelanjaTabView: View {
    
    @Binding var tab: Tab
    @Binding var curvePos : CGFloat
    @FetchRequest(entity: Cart.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Cart.date, ascending: false)])
    var carts: FetchedResults<Cart>
    
    @FetchRequest(entity: ListCat.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \ListCat.date, ascending: false)])
    private var ListCategory: FetchedResults<ListCat>
    
    @State var action : Int? = 0
    
    var body: some View {
        
        if carts.isEmpty{
            withAnimation{
                VStack{
                    Image("EMPTY STATE 2")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(minWidth: 350, maxWidth: .infinity, minHeight: 350, maxHeight: 350, alignment: .center)
                    Text("Kamu belum punya daftar resep nih.\nYuk cari resep")
                        .fontWeight(.bold)
                        .padding(.top,-35)
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color("primaryColor"))
                    Spacer()
                }
                .navigationBarTitle("Rencana Belanja", displayMode: .large)
                .navigationBarItems(
                    
                                    trailing:
                                        Button(action: {
                                            self.tab = .resep
                                        }) {
                                            Image(systemName: "plus.circle.fill")
                                                .font(.title)
                                                .foregroundColor(Color("primaryColor"))})
            }
            
        }
        
        else if ListCategory.count > 0{
            withAnimation{
            AddShoppingListView()
            }
        }
        
        else if ListCategory.isEmpty{
            withAnimation{
                ShoppingListView(tab:$tab, curvePos: $curvePos)
            }
        }
        
        else{
            ShoppingListView(tab:$tab, curvePos: $curvePos)
        }
        
        
    }
}
