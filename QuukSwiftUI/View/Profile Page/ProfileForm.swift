//
//  ProfileForm.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 12/10/20.
//

import Foundation

import SwiftUI

struct ProfileForm: View {
    
    @Binding var selected : String
    @State var textTitle : String
    
    var body: some View {
        TextField("\(textTitle)", text: $selected)
            .font(.system(size: 17))
            .padding()
            .frame(height: 43, alignment: .center)
            .background(Color(.white))
            .cornerRadius(0)
            .disableAutocorrection(true)
            .padding(.bottom,5)
    }
}

//struct ProfileForm_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileForm()
//    }
//}
