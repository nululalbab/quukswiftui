//
//  ProfileHeaderCard.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 08/10/20.
//

import SwiftUI
import ToastUI

struct ProfileEditView: View {
    
    
    @ObservedObject var updateuserVM : UpdateuserViewModel = UpdateuserViewModel()
    @Environment(\.presentationMode) var presentationMode
    @State var isSucces = false

    var body: some View {
        

        if self.updateuserVM.isError != nil {
          
        }
        
        ScrollView {
            VStack{
            VStack(alignment: .leading, spacing: 30){
                
                ZStack(alignment: Alignment.topTrailing) {
                    VStack{
                        Image("punyaku")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 130, height: 130, alignment: .center)
                            .clipped()
                            .background(Color("secondaryColor"))
                            .cornerRadius(150)
                        
                        HStack{
                            Text(self.updateuserVM.username)
                                .font(.title3)
                                .fontWeight(.regular)
                     
                        }
                        
                        Text(self.updateuserVM.phone)
                            .font(.subheadline)
                            .fontWeight(.medium)
                            .foregroundColor(.gray)
                            .padding(.vertical, 2)
                        
                        Spacer()
                    }
                    .frame(minWidth: 320, maxWidth: .greatestFiniteMagnitude)
                    .padding(.vertical, 10)
                    
                }
                .background(Color("thirdColor"))
                .cornerRadius(20)
                
            }
            .padding(.horizontal, 20)
            .padding(.vertical, 20)

            ProfileForm(selected: self.$updateuserVM.username , textTitle: "Username")
            ProfileForm(selected: self.$updateuserVM.email, textTitle: "Email")
            ProfileForm(selected:self.$updateuserVM.phone, textTitle: "No. Telp")
            ProfileForm(selected: self.$updateuserVM.gender, textTitle: "Gender")
            
            Button(action: {
                
                DefaultsKeys.defaults.removeObject(forKey: "bearer")
                UserDefaults.standard.synchronize()
                
                if  DefaultsKeys.defaults.get(.bearer) == nil {
                    self.presentationMode.wrappedValue.dismiss()
                }
                
            }, label: {
                Text("Log Out")
                    .fontWeight(.bold)
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 55, maxHeight: 55, alignment: .center)
                    .cornerRadius(15)
                    .buttonStyle(PlainButtonStyle())
                    .foregroundColor(.black)
                    .overlay(
                        RoundedRectangle(cornerRadius: 15)
                            .stroke(Color("primaryColor"), lineWidth: 4))
                    .padding(.bottom, 5)
            })
            .padding(EdgeInsets( top: 10, leading: 20, bottom: 0, trailing: 20))
            }
            .padding(.bottom, 100)
        }
        .background(Color(.gray).opacity(0.10))
        .navigationBarTitle("Ubah Profil", displayMode: .inline)
        .navigationBarItems(trailing: Button("Simpan") {
            self.updateuserVM.update { result in
                self.isSucces = result
            }
        })
      
        .toast(isPresented: $isSucces) {
      
        } content: {
            ToastView("Update Success")
                .toastViewStyle(SuccessToastViewStyle())
        }
        
        .toast(isPresented: $updateuserVM.isLoading) {
        
        } content: {
            ToastView("Loading...")
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
    }
}
