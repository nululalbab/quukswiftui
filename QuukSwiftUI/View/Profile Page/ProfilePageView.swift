//  ProfilePageView.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 07/10/20.
//

import SwiftUI
struct ProfilePageView : View {
    
    @ObservedObject var fetchHistoryVM : FetchHistoryViewModel = FetchHistoryViewModel()
    
    @ObservedObject var profilpageviewVM : ProfilePageViewViewModel = ProfilePageViewViewModel()
    
    @State var islogin = false
    var body: some View {
        
        VStack{
            
            ScrollView {
                VStack(alignment: .leading, spacing: 30) {
                    if self.islogin  {
                        
                        HeaderProfilePageTrue()
                        
                    }else {
                        
                        HeaderProfilePageFalse(username: self.profilpageviewVM.username, phone: self.profilpageviewVM.phone)
                        
                    }
                    
                    BodyProfilePageView(historia: fetchHistoryVM.history)
                    
                }
                .padding(.horizontal,20)
                .padding(.vertical,20)
            }
            .background(Color(.gray).opacity(0.10))
            
        } .onAppear{
            
            self.fetchHistoryVM.Fetch()
            
            self.profilpageviewVM.Fetch { result in
                self.islogin = result
            }
                
            
            
        }
        .navigationBarTitle("Profil", displayMode: .inline)
        
    }
}

struct HeaderProfilePageTrue : View {
    
    var body: some View {
        
        ZStack(alignment: Alignment.topTrailing) {
            VStack{
                Image("punyaku")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 130, height: 130, alignment: .center)
                    .clipped()
                    .background(Color("secondaryColor"))
                    .cornerRadius(150)
                
                HStack{
                    Text("Username")
                        .font(.title3)
                        .fontWeight(.regular)
                    
                }
                
                NavigationLink(destination: LoginPageView( isEmptyEmail: false, isEmptyPassword: false) ){
                    Text("Silahkan Login")
                        .font(.subheadline)
                        .fontWeight(.medium)
                        .padding(.vertical,2)
                        .foregroundColor(Color("primaryColor"))
                    
                }
                
                Spacer()
            }
            .frame(minWidth: 320, maxWidth: .greatestFiniteMagnitude)
            .padding(.vertical,10)
            
            NavigationLink(destination: LoginPageView( isEmptyEmail: false, isEmptyPassword: false) ){
                Image(systemName: "pencil")
                    .resizable()
                    .frame(width: 25, height: 25)
                    .foregroundColor(Color("primaryColor"))
                    .padding(.all,15)
            }
            
        }
        .background(Color("thirdColor"))
        .cornerRadius(20)
    }
}


struct HeaderProfilePageFalse : View {
    
    var username : String
    var phone : String
    
    var body: some View {
        
        
        ZStack(alignment: Alignment.topTrailing) {
            VStack{
                Image("punyaku")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 130, height: 130, alignment: .center)
                    .clipped()
                    .background(Color("secondaryColor"))
                    .cornerRadius(150)
                
                HStack{
                    Text(username)
                        .font(.title3)
                        .fontWeight(.regular)
                    
                }
                
                Text(phone)
                    .font(.subheadline)
                    .fontWeight(.medium)
                    .foregroundColor(.gray)
                    .padding(.vertical,2)
                
                Spacer()
            }
            .frame(minWidth: 320, maxWidth: .greatestFiniteMagnitude)
            .padding(.vertical,10)
            
            
            NavigationLink(destination: ProfileEditView() ){
                Image(systemName: "pencil")
                    .resizable()
                    .frame(width: 25, height: 25)
                    .foregroundColor(Color("primaryColor"))
                    .padding(.all,15)
            }
            
        }
        .background(Color("thirdColor"))
        .cornerRadius(20)
    }
}


struct BodyProfilePageView : View {
    
    var historia : [History]
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 20) {
            NavigationLink(destination: HistoryPage()){
                ProfileMenuCard(title: "Riwayat Belanja", subTitle: "Lihat apa yang sudah kamu masak!", subs: "Resep", qty: historia.count)
                    .buttonStyle(PlainButtonStyle())
            }
            
            NavigationLink(destination: Text("Coming Soon")){
                ProfileMenuCard(title: "Order Terdahulu", subTitle: "Kamu dapat melihat order terdahulu!", subs: "Order", qty: historia.count)
                    .buttonStyle(PlainButtonStyle())
            }
            
        }
        
        
    }
    
    func countHistory(history: [History]) -> Int {
        var asd = 0
        
        for item in history {
            asd = history.count
        }
        
        return asd
    }
    
}
