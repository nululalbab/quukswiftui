//
//  ProfileMenuCard.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 07/10/20.
//

import SwiftUI

struct ProfileMenuCard: View {
    let title: String
    let subTitle: String
    let subs: String
    var qty: Int
    
    var body: some View {
        VStack {
            HStack{
                VStack(alignment: .leading, spacing: 0) {
                    
                    Text(self.title)
                        .font(.title2)
                        .fontWeight(.semibold)
                        .padding(.bottom,3)
                        .foregroundColor(.black)
                    
                    Text(self.subTitle)
                        .font(.footnote)
                        .foregroundColor(.gray)
                    
                }
                
                Spacer()
                
                VStack(alignment: .center, spacing: 25) {
                    Text("\(self.qty)")
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .background(
                            Circle()
                                .fill(Color("primaryColor"))
                                .frame(width: 65, height: 65, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        )
                        .padding(.vertical,5)
                    
                    Text(self.subs)
                        .font(.subheadline)
                        .foregroundColor(.gray)
                }
                .padding(.horizontal,10)
            }
            .padding(.vertical, 30)
            .padding(.horizontal)
            .padding(.bottom,-10)
            
        }
        .background(Color("thirdColor"))
        .cornerRadius(16)
    }
}
//
//struct ProfileMenuCard_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileMenuCard(title: "Riwayat belanja", subTitle: "lihat ada kau disini", subs: "resep", qty: 69)
//    }
//}
