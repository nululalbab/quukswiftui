//
//  HistoryPage.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 17/11/20.
//

import SwiftUI

struct HistoryPage: View {
    
    @ObservedObject var fetchHistoryVM : FetchHistoryViewModel = FetchHistoryViewModel()
    
    @State var isLogin = false
    
    var body: some View {
        
        VStack{
            if self.fetchHistoryVM.isEmpty {
                VStack{
                    ZStack {
                        Image("EMPTY STATE 2")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 350, maxWidth: .infinity, minHeight: 350, maxHeight: 350, alignment: .center)
                    }
                    Text("Kamu belum memasukkan resep ke dalam bookmark")
                        .fontWeight(.bold)
                        .padding(.top,-35)
                        .padding(.bottom,200)
                        .multilineTextAlignment(.center)
                        .lineLimit(nil)
                        .fixedSize(horizontal: false, vertical: true)
                }
                .padding(.horizontal,10)
                .padding(.vertical,70)
                .foregroundColor(Color("secondaryColor"))
            }
            
//            if self.fetchfavoriteVM.isNeverLogin {
//                NavigationLink(destination: LoginPageView()) {
//                    Text("Login")
//                }
//
//            }else {
                
            List(self.fetchHistoryVM.history, id: \.id) { history in
                
                VStack{
                    ZStack{
                        NavigationLink(destination: DetailRecipeView(id:"http://www.edamam.com/recipe/\(history.uri ?? "")/lumpia")){
                        }
                    HistoryLongCardView(favorite: history)
                        .buttonStyle(PlainButtonStyle())
                    
                    }
                    
                }
                    
                
            }.listStyle(PlainListStyle())
            
        } .padding(.trailing, 20)
        .navigationBarTitle("Riwayat Belanja", displayMode: .automatic)
        .onAppear {
            
            
            self.fetchHistoryVM.Fetch()
        }
    }
}

struct HistoryLongCardView: View {
    
    let favorite : History
    
    var body: some View {
        
        VStack(alignment: .center) {
            ZStack(alignment: .leading) {
                
                VStack(alignment: .leading){
                    
                }
                .frame(width: UIScreen.main.bounds.size.width * 0.8, height: 120)
                .background(Color(#colorLiteral(red: 0.9960784314, green: 0.9764705882, blue: 0.7607843137, alpha: 1)))
                .cornerRadius(15)
                .offset(x: 40, y: 0)
                
                
                HStack {
                    URLImage(url: favorite.image!)
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                    
                    VStack (alignment: .leading, spacing: 5){
                        Text(favorite.label!)
                            .font(.system(size: 17))
                            .fontWeight(.bold)
                            .foregroundColor(Color.black)
                            .frame(width: 220, alignment: .leading)
                            .lineLimit(2)
                            
                        
                        Text(favorite.createdAt!)
                            .font(.system(size: 13))
                            .fontWeight(.regular)
                            .foregroundColor(Color.black)
                            .lineLimit(2)
                            .frame(width: 220, alignment: .leading)
                        
                        HStack {
                            Text("\(favorite.ingredients!.count) Bahan")
                                .foregroundColor(Color.gray)
                                .font(.system(size: 13))
                                .fontWeight(.regular)
                                .foregroundColor(Color.gray)
                            
                            Spacer()
                            
                            VStack {
                                
                                    Text("Masak!")
                                        .font(.system(size: 13))
                                        .fontWeight(.bold)
                                        .foregroundColor(Color.white)
                                .frame(width: 80, height: 30, alignment: .center)
                                .background(Color(#colorLiteral(red: 0.6156862745, green: 0.7647058824, blue: 0.4509803922, alpha: 1)))
                                .cornerRadius(20)
                            }
//                            .padding(.trailing)
                        }
                    }
                }
            }
//            .padding(.trailing)
            
        }
        // Spacing between each CardView
        // Need : 16. Already have 8 add 8 more
        .padding(.bottom, 8)
    }
}

//struct HistoryLongCardView : View {
//
//    let favorite : History
//
//    var body: some View {
//        VStack{
//            ZStack{
//
//
//                VStack(alignment: .leading, spacing: 2){
//                    Text(favorite.label!).font(.headline)
//                        .lineSpacing(0)
//                        .padding(EdgeInsets(top: 70, leading: 4, bottom: 0, trailing: 4))
//                    Text(favorite.createdAt!)
//                        .font(.caption)
//                        .padding(EdgeInsets(top: 2, leading: 4, bottom: 0, trailing: 4))
//
//                    Text("Porsi \(String(format: "%.0f",favorite.yield ?? 0))  |  \(favorite.ingredients!.count) Bahan")
//                        .foregroundColor(Color.gray)
//                        .font(.caption)
//                        .padding(EdgeInsets(top: 10, leading: 4, bottom: 0, trailing: 4))
//
//                    Spacer()
//                    HStack{
//                        Spacer()
//                        Text("Masak")
//                            .font(.caption)
//                            .frame(width: 130, height: 24, alignment: .center)
//                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 4, trailing: 0))
//                            .foregroundColor(.white)
//                            .background(Color("primaryColor"))
//                            .cornerRadius(15)
//                        Spacer()
//                    }
//
//
//                }.frame(width: 150, height: 200)
//                .background(Color("thirdColor"))
//                .cornerRadius(15)
//
//                URLImage(url: favorite.image!)
//                    .frame(width: 120, height: 120)
//                    .cornerRadius(20)
//                    .offset( y: -100)
//            }
//
//
//        }.frame(width: 150, height: 250)
//        .padding(EdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0))
//    }
//}

//struct HistoryLongCardView: View {
//
//    let favorite : History
//
//    var body: some View {
//
//        VStack(alignment: .center) {
//            ZStack(alignment: .leading) {
//
//                VStack(alignment: .leading){
//
//                }
//                .frame(width: UIScreen.main.bounds.size.width * 0.8, height: 120)
//                .background(Color(#colorLiteral(red: 0.9960784314, green: 0.9764705882, blue: 0.7607843137, alpha: 1)))
//                .cornerRadius(15)
//                .offset(x: 40, y: 0)
//
//
//                HStack {
//                    URLImage(url: favorite.image ?? "")
//                        .aspectRatio(contentMode: .fill)
//                        .frame(width: 100, height: 100)
//                        .clipShape(RoundedRectangle(cornerRadius: 10))
//
//                    VStack (alignment: .leading, spacing: 10){
//                        Text(favorite.label!)
//                            .font(.system(size: 17))
//                            .fontWeight(.bold)
//                            .foregroundColor(Color.black)
//                            .frame(width: 220, alignment: .leading)
//                            .lineLimit(2)
//
//                        Text(favorite.createdAt ?? "")
//                            .font(.system(size: 13))
//                            .fontWeight(.regular)
//                            .foregroundColor(Color.black)
//                            .lineLimit(2)
//                            .frame(width: 220, alignment: .leading)
//
//                        HStack {
//                            Text("  \(favorite.ingredients?.count ?? 0) Bahan")
//                                .foregroundColor(Color.gray)
//                                .font(.system(size: 13))
//                                .fontWeight(.regular)
//                                .foregroundColor(Color.gray)
//
//                            Spacer()
//
//                            VStack {
//
//                                Text("Masak!")
//                                    .font(.system(size: 13))
//                                    .fontWeight(.bold)
//                                    .foregroundColor(Color.white)
//                                    .frame(width: 80, height: 30, alignment: .center)
//                                    .background(Color(#colorLiteral(red: 0.6156862745, green: 0.7647058824, blue: 0.4509803922, alpha: 1)))
//                                    .cornerRadius(20)
//                            }
//                            .padding(.trailing)
//                        }
//                    }
//                }
//            }
//            .padding(.trailing)
//
//        }
//    }
//}
