//
//  SearchCategoryButton.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 28/09/20.
//

import SwiftUI

struct SearchBundleButton: View {
    let bundleData: BundleData
        
    var body: some View {
        
        VStack {
            
            URLImage(url:  bundleData.image)
                .aspectRatio(contentMode: .fill)
                .frame(width: 70, height: 70)
                .clipShape(Circle())
            
            Text(bundleData.name)
                .fontWeight(.regular)
                .foregroundColor(.black)
                .multilineTextAlignment(.center)
                .lineLimit(2)
                .frame(maxWidth: 90,maxHeight: 100, alignment: .top)
                .fixedSize(horizontal: false, vertical: true)
        }
    }
}
