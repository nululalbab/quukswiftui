//
//  CheckboxIngredients.swift
//  QuukSwiftUI
//
//  Created by Najibullah Ulul Albab on 27/09/20.
//

import SwiftUI

struct CheckboxIngredients: View {
    
    var cartData : ShoppingViewModel
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @State var checked: Bool
    var updatePortion: Float
    
    var candy:Bahan
    
    var body: some View {
        
        Button(action: {
            
            self.checked.toggle()
            cartData.updateCheckbox(candy, context: viewContext)
        }) {
            
            HStack(spacing:-5){
                
                Image(systemName: self.checked ? "checkmark.square.fill" : "square")
                    .foregroundColor(self.checked ? .green : .gray)
                    .frame(width: 40, height: 40, alignment: .center)
                
                Text("\(candy.wrappedBahanName)")
                    .font(.system(size: 16, weight: .regular, design: .rounded))
                    .foregroundColor(self.checked ? .black : .gray)
                    .padding(.vertical,10)
                    .lineLimit(nil)
                    .fixedSize(horizontal: false, vertical: true)
                
                Spacer()
                Text("\(String(format: "%.1f" ,self.cartData.sumTotal(context: viewContext, portion: updatePortion, qty: candy.wrappedBahanQty))) g")

                    .font(.caption)
                    .foregroundColor(Color.secondary)
                    .padding(.leading,10)
           
            }
        }
    }
}
