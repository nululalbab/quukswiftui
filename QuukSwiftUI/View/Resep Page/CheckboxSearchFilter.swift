//
//  CheckboxSearchFilter.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 01/10/20.
//

import SwiftUI

struct CheckboxSearchFilter: View {
    @State var checked: Bool
    var filterCategory: String
    var filterDescription : String
    
    var body: some View {
        Button(action: {
            self.checked.toggle()
        }) {
            HStack{
                Image(systemName: self.checked ? "checkmark.square.fill" : "square")
                    .foregroundColor(self.checked ? Color(.systemGreen) : Color(.lightGray))
                    .frame(width: 20, height: 20, alignment: .center)
                Text(self.filterCategory)
                    .font(.system(size: 13, weight: .regular, design: .rounded))
                Text(self.filterDescription)
                    .font(.system(size: 10, weight: .regular, design: .rounded))
                    .foregroundColor(Color.secondary)
            }
        }
    }
}
