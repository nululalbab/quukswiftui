//
//  SearchBottomSheetView.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 28/09/20.
//

import SwiftUI

fileprivate enum Constants {
    static let radius: CGFloat = 16
    static let indicatorHeight: CGFloat = 5
    static let indicatorWidth: CGFloat = 90
    static let snapRatio: CGFloat = 0.25
    static let minHeightRatio: CGFloat = 0.3
}

struct SearchBottomSheetView<Content: View>: View {
    @Binding var isOpen: Bool
    @State var position = CardPosition.bottom
    @State var query : String? = "kacang"
    
    let maxHeight: CGFloat
    let minHeight: CGFloat
    let content: Content

    @GestureState private var translation: CGFloat = 0

    private var offset: CGFloat {
        isOpen ? 0 : maxHeight - minHeight
    }

    private var indicator: some View {
        RoundedRectangle(cornerRadius: Constants.radius)
            .fill(Color.white)
            .frame(
                width: Constants.indicatorWidth,
                height: Constants.indicatorHeight
        ).onTapGesture {
            self.isOpen.toggle()
        }
    }
    
    @State private var searchText : String = ""
    @State private var searchIngredient : String = ""
    @State private var searchCalorie : String = ""
    @State var isFilterExpanded = false
    @State var balancedFilter = false
    @State var highFiberFilter = false 
    @State var highProteinFilter = false
    @State var filter4Checked = false
    @State var lowFatFilter = false
    @State var lowSodiumFilter = false
    @State var isShowingTitle = false
    
    @ObservedObject var searchrecipeVM = SearchRecipeViewModel()
    
    private var searchMenu: some View {
        VStack(spacing: 11) {
            
            VStack(spacing: 12) {
                
                Section {
                    TextField("Cari Resep, Bahan, Kandungan Gizi...", text: $searchrecipeVM.recipeName)
                        .font(.system(size: 13))
                        .padding()
                        .frame(height: 35, alignment: .center)
                        .background(Color("thirdColor"))
                        .cornerRadius(20)
                        .disableAutocorrection(true)
                        .onTapGesture {
                            position = CardPosition.top
                            isShowingTitle = true
                            isFilterExpanded = true
                        }
                    
                    if isShowingTitle {
                        VStack(alignment: .trailing) {
                            DisclosureGroup(isExpanded: $isFilterExpanded) {
                                
                                if isFilterExpanded {
                                    VStack(alignment: .leading, spacing: 11) {
                                        Button(action: {
                                            balancedFilter.toggle()
                                        }) {
                                            HStack{
                                                Image(systemName: balancedFilter ? "checkmark.square.fill" : "square")
                                                    .foregroundColor(balancedFilter ? Color(.systemGreen) : Color(.lightGray))
                                                    .frame(width: 20, height: 20, alignment: .center)
                                                Text(Filters.category1.filterCategory)
                                                    .font(.system(size: 13, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.black)
                                                Text(Filters.category1.filterDescription)
                                                    .font(.system(size: 10, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.gray)
                                            }
                                        }
                                        
                                        Button(action: {
                                            highFiberFilter.toggle()
                                        }) {
                                            HStack{
                                                Image(systemName: highFiberFilter ? "checkmark.square.fill" : "square")
                                                    .foregroundColor(highFiberFilter ? Color(.systemGreen) : Color(.lightGray))
                                                    .frame(width: 20, height: 20, alignment: .center)
                                                Text(Filters.category2.filterCategory)
                                                    .font(.system(size: 13, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.black)
                                                Text(Filters.category2.filterDescription)
                                                    .font(.system(size: 10, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.gray)
                                            }
                                        }
                                        
                                        Button(action: {
                                            highProteinFilter.toggle()
                                        }) {
                                            HStack{
                                                Image(systemName: highProteinFilter ? "checkmark.square.fill" : "square")
                                                    .foregroundColor(highProteinFilter ? Color(.systemGreen) : Color(.lightGray))
                                                    .frame(width: 20, height: 20, alignment: .center)
                                                Text(Filters.category3.filterCategory)
                                                    .font(.system(size: 13, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.black)
                                                Text(Filters.category3.filterDescription)
                                                    .font(.system(size: 10, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.gray)
                                            }
                                        }
                                        
                                        Button(action: {
                                            filter4Checked.toggle()
                                        }) {
                                            HStack{
                                                Image(systemName: filter4Checked ? "checkmark.square.fill" : "square")
                                                    .foregroundColor(filter4Checked ? Color(.systemGreen) : Color(.lightGray))
                                                    .frame(width: 20, height: 20, alignment: .center)
                                                Text(Filters.category4.filterCategory)
                                                    .font(.system(size: 13, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.black)
                                                Text(Filters.category4.filterDescription)
                                                    .font(.system(size: 10, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.gray)
                                            }
                                        }
                                        
                                        Button(action: {
                                            lowFatFilter.toggle()
                                        }) {
                                            HStack{
                                                Image(systemName: lowFatFilter ? "checkmark.square.fill" : "square")
                                                    .foregroundColor(lowFatFilter ? Color(.systemGreen) : Color(.lightGray))
                                                    .frame(width: 20, height: 20, alignment: .center)
                                                Text(Filters.category5.filterCategory)
                                                    .font(.system(size: 13, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.black)
                                                Text(Filters.category5.filterDescription)
                                                    .font(.system(size: 10, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.gray)
                                            }
                                        }
                                        
                                        Button(action: {
                                            lowSodiumFilter.toggle()
                                        }) {
                                            HStack{
                                                Image(systemName: lowSodiumFilter ? "checkmark.square.fill" : "square")
                                                    .foregroundColor(lowSodiumFilter ? Color(.systemGreen) : Color(.lightGray))
                                                    .frame(width: 20, height: 20, alignment: .center)
                                                Text(Filters.category6.filterCategory)
                                                    .font(.system(size: 13, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.black)
                                                Text(Filters.category6.filterDescription)
                                                    .font(.system(size: 10, weight: .regular, design: .rounded))
                                                    .foregroundColor(Color.gray)
                                            }
                                        }
                                    }
                                    .padding(.top, 5)
                                    .onAppear() {
                                        if isFilterExpanded {
                                            position = CardPosition.top
                                        }
                                    }
                                    .onDisappear() {
                                        if !isFilterExpanded && isShowingTitle {
                                            position = CardPosition.middle
                                        }
                                    }
                                }
                            }
                            label: {
                                Text("Label Pola Makan")
                                    .font(.system(size: 13))
                                    .padding(.leading, 15)
                            }
                            .padding(.vertical, 10)
                            .padding(.trailing, 20)
                            .background(Color("thirdColor"))
                            .cornerRadius(20)
                            
                            TextField("Kalori yang dibutuhkan...", text: self.$searchrecipeVM.calorie)
                                .font(.system(size: 13))
                                .padding()
                                .frame(height: 35, alignment: .center)
                                .background(Color("thirdColor"))
                                .cornerRadius(20)
                                .disableAutocorrection(true)
                            
                            NavigationLink(destination: SearchResultPage(searchrecipeVM: self.searchrecipeVM)){
                                Text("Cari!")
                                    .font(.system(size: 13))
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.white)
                                    .frame(width: 70, height: 30, alignment: .center)
                                    .background(Color("primaryColor"))
                                    .cornerRadius(15)
                            }
                        }
                        .transition(.asymmetric(insertion: .scale, removal: .opacity))
                    }
                }
            }
            .padding(.horizontal)
            .frame(width: UIScreen.main.bounds.size.width, height: 380, alignment: .top)
            .cornerRadius(20)
        }
    }

    init(isOpen: Binding<Bool>, maxHeight: CGFloat, @ViewBuilder content: () -> Content) {
        self.minHeight = maxHeight - 200
        self.maxHeight = 850
        self.content = content()
        self._isOpen = isOpen
    }

    var body: some View {
        GeometryReader { geometry in
            VStack(spacing: 0) {
                self.indicator.padding(.vertical, 13)
                self.searchMenu
                self.content
            }
            .frame(width: geometry.size.width, height: self.maxHeight, alignment: .top)
            .background(Color("bottomSheetColor"))
            .cornerRadius(Constants.radius)
            .frame(height: geometry.size.height, alignment: .bottom)
            .offset(y: max(self.position.rawValue + self.translation, 0))
            .animation(.interactiveSpring())
            .gesture(
                DragGesture().updating(self.$translation) { value, state, _ in
                    state = value.translation.height
                }.onEnded { value in
                  
                    let positionAbove: CardPosition
                    let positionBelow: CardPosition
                    let closestPosition: CardPosition
                    
                    let cardTopEdgeLocation = self.position.rawValue + value.translation.height
                    let verticalDirection = value.predictedEndLocation.y - value.location.y
                    
                    if cardTopEdgeLocation <= CardPosition.middle.rawValue {
                        positionAbove = .top
                        positionBelow = .middle
                    } else {
                        positionAbove = .middle
                        positionBelow = .bottom
                    }
                    
                    if (cardTopEdgeLocation - positionAbove.rawValue) < (positionBelow.rawValue - cardTopEdgeLocation) {
                        closestPosition = positionAbove
                    } else {
                        closestPosition = positionBelow
                    }
                    
                    if verticalDirection > 0 {
                        self.position = positionBelow
                    } else if verticalDirection < 0 {
                        self.position = positionAbove
                    } else {
                        self.position = closestPosition
                    }
                    
                    // Current Card Position
                    if self.position == .bottom{
                        self.endEditing(true)
                        isShowingTitle = false
                        isFilterExpanded = false
                    }else if self.position == .middle{
                        self.endEditing(true)
                        isShowingTitle = true
                        isFilterExpanded = false
                    }else{
                        isShowingTitle = true
                        isFilterExpanded = true
                    }
                }
            )
        }
    }
}

enum CardPosition: CGFloat {
    case top = 400
    case middle = 580
    case bottom = 710
}

struct SearchBottomSheetView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBottomSheetView(isOpen: .constant(false), maxHeight: 600) {
            Rectangle().fill(Color.red)
        }.edgesIgnoringSafeArea(.all)
    }
}
