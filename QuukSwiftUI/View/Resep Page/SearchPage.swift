//
//  SearchPage.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 28/09/20.
//

import SwiftUI

struct Filters: Identifiable {
    let id = UUID()
    let filterCategory: String
    let filterDescription: String
    let checked: Bool
    var searchFilter: [Filters]?
    
    // some example websites
    static let category1 = Filters(filterCategory: "Balanced", filterDescription: "Protein/Fat/Carb values in 15/35/50 ratio", checked: false)
    static let category2 = Filters(filterCategory: "High-Fiber", filterDescription: "More than 5g fiber per serving", checked: false)
    static let category3 = Filters(filterCategory: "High-Protein", filterDescription: "More than 50% of total calories from proteins", checked: false)
    static let category4 = Filters(filterCategory: "Low-Carb", filterDescription: "Less than 20% of total calories from carbs", checked: false)
    static let category5 = Filters(filterCategory: "Low-Fat", filterDescription: "Less than 15% of total calories from fat", checked: false)
    static let category6 = Filters(filterCategory: "Low-Sodium", filterDescription: "Less than 140mg Na per serving", checked: false)
    
    static let example1 = Filters(filterCategory: "Label Pola Makan", filterDescription: "", checked: false, searchFilter: [Filters.category1, Filters.category2, Filters.category3, Filters.category4, Filters.category5, Filters.category6])
}

struct SearchPage: View {
    @State private var searchBottomSheetShown = false
    @State private var searchText : String = ""
    @State var isFilterExpanded = false
    @State var filter1Checked = false
    @State var filter2Checked = false
    @State var filter3Checked = false
    @State var filter4Checked = false
    @State var filter5Checked = false
    @State var filter6Checked = false
    @State var isLogin = false
    
    let searchFilter: [Filters] = [.category1, .category2, .category3, .category4, .category5, .category6]
    
    
    @ObservedObject var recipeBundleVM : RecipeBundleVM = RecipeBundleVM()
    @ObservedObject var recipeFeedVM : RecipeFeedViewModel = RecipeFeedViewModel()
    @ObservedObject var searchrecipeVM : SearchRecipeViewModel = SearchRecipeViewModel()
    
    var body: some View {
        ZStack(alignment: .top){
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    
                    // List Bundle
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack(alignment: .top) {
                            ForEach(recipeBundleVM.bundles, id: \.name) { bundle in
                                NavigationLink(destination: BundlePage(bundleData: bundle)){
                                    SearchBundleButton(bundleData: bundle)
                                }.buttonStyle(PlainButtonStyle())
                            }
                        }
                        .padding(.leading, 20)
                        .padding(.vertical, 20)
                        
                        Spacer()
                    }
                    .frame(height: 100)
                    
                    // Content
                    VStack {
                        HStack {
                            Text("Wajib Coba")
                                .font(.title2)
                                .fontWeight(.bold)
                                .foregroundColor(.black)
                            
                            Spacer(minLength: 0)
                        }
                        .padding(.top, 20)
                        
                        // List Feed
                        ForEach(recipeFeedVM.recipes, id: \.uri) { recipe in
                            NavigationLink(destination: DetailRecipeView(id: recipe.shareAs!)){
                                RecipeLongCardView(recipe: recipe, ingredient: recipe.ingredients!)
                            }.buttonStyle(PlainButtonStyle())
                        }
                        
                    }
                    .padding(.leading, 20)
                }
                .padding(.bottom, 100)
            }
//            .frame(height: 630)
            .onAppear{
                if recipeBundleVM.bundles.count == 0 {
                    self.recipeBundleVM.fetchListBundleRecipe()
                }
                
                if recipeFeedVM.recipes.count == 0 {
                self.recipeFeedVM.fetchListFeedRecipe(by: "Kari")
                }
            }
            
            // Bottom Sheet View
            GeometryReader{ geomtery in
                SearchBottomSheetView(
                    isOpen: $searchBottomSheetShown,
                    maxHeight: geomtery.size.height * 0.7
                ) {
                    
                }
            }
            
//            if(onboard.showOnboardScreen) {
//                ProgressiveOnboardView.init(withProgressiveOnboard: self.onboard)
//            }
        }
        .navigationBarTitle("Cari Resep", displayMode: .large)
        .navigationBarItems(
                    trailing:
                        HStack {
                            if self.isLogin{
                                
                                NavigationLink(destination: LoginPageView( isEmptyEmail: false, isEmptyPassword: false)) {
                                    Image(systemName: "bookmark")
                                        .font(.title)
                                        .foregroundColor(Color(#colorLiteral(red: 0.6156862745, green: 0.7647058824, blue: 0.4509803922, alpha: 1)))
                                }
                                
                                
                            }else{
                                NavigationLink(destination: BookmarkPage()) {
                                    Image(systemName: "bookmark")
                                        .font(.title)
                                        .foregroundColor(Color(#colorLiteral(red: 0.6156862745, green: 0.7647058824, blue: 0.4509803922, alpha: 1)))
                                }
                                
                            }
                            
                        } )

        .onAppear {
            if DefaultsKeys.defaults.get(.bearer) == nil{
                self.isLogin = true
            }else{
                self.isLogin = false
            }
            
            if  DefaultsKeys.defaults.get(.beginning) == nil {

//                self.onboard.showOnboardScreen = true
            }
            
            
        }
    }
}

struct SearchPage_Previews: PreviewProvider {
    static var previews: some View {
        SearchPage()
    }
}
