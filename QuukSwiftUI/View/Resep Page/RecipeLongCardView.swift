//
//  RecipeLongCardView.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 28/09/20.
//

import SwiftUI

struct RecipeLongCardView: View {
    
    let recipe : RecipeWajibCoba
    let ingredient : [IngredientWajibCoba]
    
    var body: some View {
        
        VStack(alignment: .center) {
            ZStack(alignment: .leading) {
                
                VStack(alignment: .leading){
                    
                }
                .frame(width: UIScreen.main.bounds.size.width * 0.8, height: 120)
                .background(Color("thirdColor"))
                .cornerRadius(15)
                .offset(x: 40, y: 0)
                
                HStack {
                    URLImage(url: recipe.image ?? "")
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                    
                    VStack(alignment: .leading, spacing: 5){
                        Text(recipe.label!)
                            .font(.system(size: 17))
                            .fontWeight(.bold)
                            .foregroundColor(Color.black)
                            .frame(width: 220, alignment: .leading)
                            .lineLimit(2)
                            
                        
                        Text(recipe.source!)
                            .font(.system(size: 13))
                            .fontWeight(.regular)
                            .foregroundColor(Color.black)
                            .lineLimit(2)
                            .frame(width: 220, alignment: .leading)
                        
                        HStack {
                            Text("Porsi: \(String(format: "%.0f",recipe.yield ?? 0))  |  \(ingredient.count) Bahan")
                                .foregroundColor(Color.gray)
                                .font(.system(size: 13))
                                .fontWeight(.regular)
                                .foregroundColor(Color.gray)
                            
                            Spacer()
                            
                            VStack {
                                
                                    Text("Masak!")
                                        .font(.system(size: 13))
                                        .fontWeight(.bold)
                                        .foregroundColor(Color.white)
                                .frame(width: 80, height: 30, alignment: .center)
                                .background(Color("primaryColor"))
                                .cornerRadius(20)
                            }
                            .padding(.trailing)
                        }
                    }
                }
            }
            .padding(.trailing)
            
        }
        // Spacing between each CardView
        // Need : 16. Already have 8 add 8 more
        .padding(.bottom, 8)
    }
}

