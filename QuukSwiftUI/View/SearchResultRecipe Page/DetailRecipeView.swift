import Foundation
import SwiftUI
import ToastUI

struct DetailRecipeView : View {
    
    @State var selection: Tab = .belanja
    @State var curvePos : CGFloat = 0
    
    @StateObject var cartData = ShoppingViewModel()
    
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(entity: Cart.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Cart.date, ascending: false)])
        private var carts: FetchedResults<Cart>
    
    @ObservedObject var fetchdetailVM : FetchDetailViewModel =  FetchDetailViewModel()
    
    @ObservedObject  var updatefavoriteVM : UpdateFavoriteViewModel = UpdateFavoriteViewModel()
    
    @State private var isPresenting : Bool = false
    @State private var isLogin = false
    
    @State var action : Int? = 0
    
    let id : String
    
    var body : some View {
        
        ScrollView {
            
            ZStack(alignment: Alignment.bottom){
                
                URLImage(url: self.fetchdetailVM.recipe.image ?? "")
                    .aspectRatio(contentMode: .fill)
                    .frame(height: 300, alignment: Alignment.top)
                    .offset(y: -50 )
                    .clipped()
                
                Rectangle()
                    .frame(height: 33)
                    .cornerRadius(20)
                    .offset(y: 16)
                    .foregroundColor(.white)
                
            }
            
            VStack(alignment: .leading){
                
                HStack( spacing: 10){
                    Text(self.fetchdetailVM.recipe.label ?? "")
                        .foregroundColor(Color.black)
                        .font(.title2)
                        .bold()
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                    
                    Spacer()
                    
                    Button(action: {
                        
                        self.isPresenting = true
                    }) {
                        Image(systemName: "info.circle")
                            .resizable()
                            .frame(width: 30, height: 30)
                            .accentColor(Color("secondaryColor"))

                    }.sheet(isPresented: $isPresenting, content: {
                        InstructionDetailRecipe(isPresenting: self.$isPresenting, url: self.fetchdetailVM.recipe.url ?? "")
                    })
                    
                    
                    if  self.isLogin {
                        NavigationLink(destination: LoginPageView( isEmptyEmail: false, isEmptyPassword: false)){
                             imageFavorite(isFavorite: self.$fetchdetailVM.recipe.isFavorite)
                           }
                    }else{
                        
                        Button(action: {
                            
                            self.updatefavoriteVM.url = id
                            self.updatefavoriteVM.Update(vm: self.fetchdetailVM)
                            
                        }) {
                            imageFavorite(isFavorite: self.$fetchdetailVM.recipe.isFavorite)

                        }
                    }
                    
                    
                }
                
                Text("Porsi \(self.fetchdetailVM.recipe.servings ?? 0)  |  \(self.fetchdetailVM.recipe.ingredients?.count ?? 0 ) Bahan")
                    .foregroundColor(Color.gray)
                    .font(.footnote)
                
                HStack{
                    
                    nutritionview(value: String(format: "%.0f",self.fetchdetailVM.recipe.calories ?? 0), unitofmasurement:"kcal" , nutrition: "Energy")
                        .padding(.leading,10)
                    Spacer()
                    
                    nutritionview(value: String(format: "%.0f", self.fetchdetailVM.recipe.proteins ?? 0), unitofmasurement:"g"  , nutrition: "Protein" )
                    Spacer()
                    nutritionview(value: String(format: "%.0f", self.fetchdetailVM.recipe.fats ?? 0), unitofmasurement:"g"  , nutrition: "Fat" )
                    Spacer()
                    nutritionview(value: String(format: "%.0f", self.fetchdetailVM.recipe.carbo ?? 0), unitofmasurement:"g"  , nutrition: "Carbs")
                    
                }
                
                Text("Bahan")
                    .font(.system(.headline))
                
                VStack(alignment: .leading){
                    ForEach(self.fetchdetailVM.inggridients, id: \.id) { ingridients in
                        HStack{
                            Text(ingridients.text ?? "")
                                .font(.footnote)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            Text("\(String(format: "%.1f",ingridients.weight  ?? "")) g")
                                .font(.caption2)
                                .foregroundColor(.secondary)
                        }
                        
                    }.padding(4)
                    
                }.padding(8)
                .background(Color("thirdColor"))
                .cornerRadius(8)
                .frame(minWidth: 0, maxWidth: .infinity)
               
                if self.isLogin {
                    NavigationLink(
                        destination: LoginPageView( isEmptyEmail: false, isEmptyPassword: false),
                        label: {
                            Text("Masukkan ke Jadwal Menu")
                                .padding(12)
                                .frame(minWidth: 0, maxWidth: .infinity, maxHeight: 50, alignment: .center)
                                .foregroundColor(Color.black)
                                .font(Font.headline.bold())
                                .cornerRadius(8)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 8)
                                        .stroke(Color("secondaryColor"), lineWidth: 3)
                                )
                        })
                        .padding(.top, 30)
                } else{
                    NavigationLink(
                        destination: AddMealPlan(recipe: fetchdetailVM.recipe),
                        label: {
                            Text("Masukkan ke Jadwal Menu")
                                .padding(12)
                                .frame(minWidth: 0, maxWidth: .infinity, maxHeight: 50, alignment: .center)
                                .foregroundColor(Color.black)
                                .font(Font.headline.bold())
                                .cornerRadius(8)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 8)
                                        .stroke(Color("secondaryColor"), lineWidth: 3)
                                )
                        })
                        .padding(.top, 30)
                }
                
                
                
                
                NavigationLink(destination: ShoppingListView(tab: $selection, curvePos: $curvePos)){
                    Text("Tambah ke Daftar Resep")
                        .padding(12)
                        .frame(minWidth: 0, maxWidth: .infinity, maxHeight: 50, alignment: .center)
                        .background(Color.orange)
                        .font(Font.headline.bold())
                        .foregroundColor(Color.black)
                        .cornerRadius(8)
                }.simultaneousGesture(TapGesture().onEnded
                {
                    cartData.writeData(context: viewContext, title: self.fetchdetailVM.recipe.label ?? "", portion: Int(self.fetchdetailVM.recipe.servings ?? 0), items: self.fetchdetailVM.inggridients, fetch: carts)
                    
                }).padding(.top, 8)
                
            }
            .padding(EdgeInsets( top: 0, leading: 20, bottom:64, trailing: 20))
            .background(Color.white)
        }
        .toast(isPresented: $updatefavoriteVM.isLoading) {
            print("Toast dismissed")
        } content: {
            ToastView("Update...")
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
        
         
        .onAppear {
                self.fetchdetailVM.Fetch(id: id)
            if  DefaultsKeys.defaults.get(.bearer) == nil {
                self.isLogin = true
            } else {
                
                self.isLogin = false
            }
        }
        
    }
    
}

struct nutritionview : View {
    
    let value : String
    let unitofmasurement : String
    let nutrition : String
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 30)
                .fill(Color("thirdColor"))
                .frame(width: 65, height: 120)
            VStack(spacing: 10){
                ZStack{
                    Circle()
                        .stroke(Color.secondary, lineWidth: 1)
                        .background(Circle().foregroundColor(Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))))
                        .frame(width: 55, height: 55)
                        
                    VStack{
                        Text(self.value)
                            .font(.body)
                            .bold()
                        
                        Text(self.unitofmasurement)
                            .font(.caption2)
                    }
                }
                Text(self.nutrition)
                    .font(.caption)
                    .bold()
            }
            
        }
    }
}


struct imageFavorite : View {
    
    @Binding var isFavorite : Bool?

    
    var body: some View {
        
        if isFavorite ?? false {
        Image(systemName: "bookmark.fill")
            .resizable()
            .frame(width: 30, height: 45)
            .accentColor(Color("secondaryColor"))
        }else {
            Image(systemName: "bookmark")
                .resizable()
                .frame(width: 30, height: 45)
                .accentColor(Color(.secondaryLabel))
        }
        
    }
    
}




//Maek :- Eret eretan ail

//                    nutritionview(value: String(format: "%.0f", totalnutrients["ENERC_KCAL"]!.quantity!), unitofmasurement:totalnutrients["ENERC_KCAL"]!.unit!.rawValue  , nutrition:totalnutrients["ENERC_KCAL"]!.label! )
//                        .padding(.leading,10)
//                    Spacer()
//
//                    nutritionview(value: String(format: "%.0f", totalnutrients["PROCNT"]!.quantity!), unitofmasurement:totalnutrients["PROCNT"]!.unit!.rawValue  , nutrition:totalnutrients["PROCNT"]!.label! )
//                    Spacer()
//                    nutritionview(value: String(format: "%.0f", totalnutrients["FAT"]!.quantity!), unitofmasurement:totalnutrients["FAT"]!.unit!.rawValue  , nutrition:totalnutrients["FAT"]!.label! )
//                    Spacer()
//                    nutritionview(value: String(format: "%.0f", totalnutrients["CHOCDF"]!.quantity!), unitofmasurement:totalnutrients["CHOCDF"]!.unit!.rawValue  , nutrition:totalnutrients["CHOCDF"]!.label! )
