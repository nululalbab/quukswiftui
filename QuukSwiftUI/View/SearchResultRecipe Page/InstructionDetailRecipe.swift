//
//  WebViewLoader.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 02/10/20.

import SwiftUI

struct InstructionDetailRecipe: View {
    @Binding var isPresenting : Bool
    let url : String
    
    var body: some View {
        VStack(alignment: .leading){
            
            Button(action: {
                self.isPresenting = false
                
            }
            ,label: {
                HStack{
                    Image(systemName: "xmark")
                    Text("Close")
                }
            })
            
            Divider()
            
            WebViewLoader(url: url)
        }
        .padding()
        
    }
}

struct WebViewLoader_Previews: PreviewProvider {
    static var previews: some View {
        InstructionDetailRecipe(isPresenting: .constant(false), url: "")
    }
}
