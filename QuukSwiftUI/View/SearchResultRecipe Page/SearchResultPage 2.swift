//
//  SearchResultPage.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 05/10/20.
//

import SwiftUI
import Combine
import ToastUI

struct SearchResultPage: View {
    
    
    @ObservedObject var searchrecipeVM : SearchRecipeViewModel = SearchRecipeViewModel()
    
    let columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
    var body: some View {

        ScrollView {
            VStack{
                HStack {
                    Button {
                        self.searchrecipeVM.search()
                    } label: {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(Color("secondaryColor"))
                            .frame(width: 40, height: 40)  }
                    
                    TextField("", text:  self.$searchrecipeVM.recipeName, onCommit: {
                        
                    })
                    
                }.background(Color("thirdColor"))
                .cornerRadius(10)
                .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                
                if self.searchrecipeVM.isEmpty {
                    ZStack {
                        Image("EMPTY STATE 2")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 350, maxWidth: .infinity, minHeight: 350, maxHeight: 350, alignment: .center)
                    }
                    
                    Text("Tidak Ada Yang Cocok")
                        .fontWeight(.bold)
                        .padding(.top,-35)
                        .padding(.bottom,200)
                        .multilineTextAlignment(.center)
                        .lineLimit(nil)
                        .fixedSize(horizontal: false, vertical: true)
                }
                LazyVGrid(columns: columns) {
                    ForEach(self.searchrecipeVM.recipes, id : \.uri) { recipe in
                        NavigationLink(destination:
                                        DetailRecipeView(id: recipe.shareAs!)){
                            SearchResultCell(recipe: recipe, 
                                             image: recipe.image ?? "",
                                             title: recipe.label ?? "",
                                             description: recipe.source ?? "",
                                             ingridients: recipe.ingredients!)
                        }.buttonStyle(PlainButtonStyle())
                    }
                }.padding(EdgeInsets(top: 50, leading: 0, bottom: 50, trailing: 0))
            }
        }
        .onAppear{
            self.searchrecipeVM.search()
        }
        .toast(isPresented: self.$searchrecipeVM.isLoading , dismissAfter: 2.0) {
            print("Toast dismissed")
        } content: {
            ToastView("Loading...")
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
    }
}

struct SearchResultPage_Previews: PreviewProvider {
    static var previews: some View {
        SearchResultPage()
        
    }
}

struct SearchResultCell : View {
    
    let recipe : Recipe
    let image : String
    let title : String
    let description : String
    let ingridients : [Ingredient]
    
    var body: some View {
        VStack{
            ZStack{
                
                VStack(alignment: .leading, spacing: 2){
                    Text(title).font(.headline)
                        .lineSpacing(0)
                        .padding(EdgeInsets(top: 70, leading: 4, bottom: 0, trailing: 4))
                    Text(description)
                        .font(.caption)
                        .padding(EdgeInsets(top: 2, leading: 4, bottom: 0, trailing: 4))
                    
                    Text("Porsi \(String(format: "%.0f",recipe.yield ?? 0))  |  \(ingridients.count) Bahan")
                        .foregroundColor(Color.gray)
                        .font(.caption)
                        .padding(EdgeInsets(top: 10, leading: 4, bottom: 0, trailing: 4))
                    
                    Spacer()
                    HStack{
                        Spacer()
                        Text("Masak")
                            .font(.caption)
                            .frame(width: 130, height: 24, alignment: .center)
                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 4, trailing: 0))
                            .background(Color("primaryColor"))
                            .cornerRadius(15)
                        Spacer()
                    }
                    
                    
                }.frame(width: 150, height: 200)
                .background(Color("thirdColor"))
                .cornerRadius(15)
                
                URLImage(url: image)
                    .frame(width: 120, height: 120)
                    .cornerRadius(20)
                    .offset( y: -100)
            }
            
            
        }.frame(width: 150, height: 250)
        .padding(EdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0))
    }
}
