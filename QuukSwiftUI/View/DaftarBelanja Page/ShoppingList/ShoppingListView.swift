//
//  ShoppingListView.swift
//  QuukSwiftUI
//
//  Created by Najibullah Ulul Albab on 27/09/20.
//

import SwiftUI

struct ShoppingListView: View {
    @ObservedObject var cartData = ShoppingViewModel()
    
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(entity: Cart.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Cart.date, ascending: false)])
    var carts: FetchedResults<Cart>
    
    @FetchRequest(entity: ListCat.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \ListCat.date, ascending: false)])
    private var ListCategory: FetchedResults<ListCat>
    
    let resepList: [NewRecipes] = [.list1,.list2,.list3]
    
    @ObservedObject var shoopingPlan = ShoopingPlanList()
    
    @Binding var tab: Tab
    @Binding var curvePos : CGFloat
    
    @State var portion : Float = 0
    
    @State var action : Int? = 0
    
    var body: some View {
        
        List {
            
            ForEach(carts) { row in
                VStack(spacing: 10) {
                    
                    HStack{
                        VStack(alignment: .leading){
                            Text(row.wrappedTitle)
                                .font(.body)
                                .fontWeight(.bold)
                            Text("\(Int(row.portion)) Porsi / Resep")
                                .font(.caption)
                                .foregroundColor(Color.secondary)
                        }
                        Spacer()
                    }
                    
                    VStack {
                        ForEach(row.candyArray,id: \.id) { candy in
                            HStack{
                                CheckboxIngredients(cartData: cartData, checked: candy.check, updatePortion: row.portion, candy:candy)
                                
                                Spacer()
                                
                            }
                        }.padding(.bottom,-20)
                    }
                    .padding(.all,10)
                    .padding(.bottom,20)
                    .background(Color("thirdColor"))
                    .foregroundColor(.black)
                    .cornerRadius(20)
                    
                    HStack{
                        Text("Jumlah resep yang mau kamu buat")
                            .font(.subheadline)
                        Spacer()
                        
                        HStack(spacing: 10){
                            Button(action: {
                                if row.portion > 1 {
                                    row.portion-=1
//                                    self.portion = row.portion
                                    self.cartData.updateQty(row, context: viewContext, portion: row.portion)
                                }
                                
                            }, label: {
                                Image(systemName: "minus")
                                    .font(.system(size: 16, weight: .bold))
                                    .padding(.all,7)
                                    .buttonStyle(PlainButtonStyle())
                            })
                            .buttonStyle(PlainButtonStyle())
                            
                            
                            Text("\(String(format: "%.0f",row.portion))")
                                .font(.subheadline)
                                .onAppear(perform: {
//                                    self.portion = row.portion
                                    self.cartData.updateQty(row, context: viewContext, portion: row.portion)
                                })
                            
                            Button(action: {
                                row.portion+=1
//                                self.portion = row.portion
                                self.cartData.updateQty(row, context: viewContext, portion: row.portion)
                            }, label: {
                                Image(systemName: "plus")
                                    .font(.system(size: 16, weight: .bold))
                                    .padding(.all,7)
                                    .buttonStyle(PlainButtonStyle())
                            })
                            .buttonStyle(PlainButtonStyle())
                        }
                        .foregroundColor(.black)
                        .background(Color("secondaryColor"))
                        .cornerRadius(20)
                    }
                    .padding(.vertical, 5)
                    
                }
            }
            
            .onDelete(perform: deleteCart)
            .buttonStyle(PlainButtonStyle())
            .onAppear{
                cartData.deleteCoreData(entity: "ListCat", context: viewContext)
            }
            
            if(carts.isEmpty){
                VStack{
                    Image("EMPTY STATE 2")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(minWidth: 350, maxWidth: .infinity, minHeight: 350, maxHeight: 350, alignment: .center)
                    Text("Kamu belum punya daftar resep nih.\nYuk cari resep")
                        .fontWeight(.bold)
                        .padding(.top,-35)
                        .multilineTextAlignment(.center)
                    
                }
                .foregroundColor(Color("primaryColor"))
                
            } else {
                VStack{
                    ZStack {
                        
                        NavigationLink(destination: AddShoppingListView(), tag: 1, selection: $action){
                            
                        }
//                        .buttonStyle(PlainButtonStyle())
                        
                        Text("Buat Daftar Belanja")
                            .fontWeight(.bold)
                            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 55, maxHeight: 55, alignment: .center)
                            .background(Color("secondaryColor"))
                            .cornerRadius(15)
                    }.simultaneousGesture(TapGesture().onEnded
                    {
                        self.cartData.moveToDaftarBelanja(context: viewContext, carts: carts, object: shoopingPlan, listCat: ListCategory)
                        self.action = 1
                        
                    })
                    
                }
                .padding(.bottom,70)
                .foregroundColor(.black)
            }
        }
        .listStyle(PlainListStyle())
        
        .navigationBarTitle("Rencana Belanja", displayMode: .automatic)
        .navigationBarItems(
            
            //MARK: For testing [CoreData]
//            leading:
//                                Button(action: {
//
//                                    let newTask = Bahan(context: viewContext)
//                                    newTask.name = "asd"
//                                    newTask.qty = 200 / 2
//                                    newTask.check = true
//                                    newTask.id = UUID()
//
//                                    newTask.origin = Cart(context: viewContext)
//                                    newTask.origin?.title = "title"
//                                    newTask.origin?.portion = 2
//                                    newTask.origin?.date = Date()
//                                    newTask.origin?.id = UUID()
//
//                                    try! viewContext.save()
//
//                                }) {
//                                    Text("test add data")
//                                        .font(.body)
//                                        .fontWeight(.medium)
//                                        .foregroundColor(Color("secondaryColor"))
//                                }
//                            ,
            
                            trailing:
                                Button(action: {
                                    self.tab = .resep
                                }) {
                                    Image(systemName: "plus.circle.fill")
                                        .font(.title)
                                        .foregroundColor(Color("primaryColor"))})
    }
    
    private func saveContext() {
        do {
            try viewContext.save()
        } catch  {
            let error = error as NSError
            fatalError("Unresolved error: \(error)")
        }
    }
    
    private func deleteCart(offsets: IndexSet){
        withAnimation{
            offsets.map{ carts[$0] }.forEach(viewContext.delete)
            saveContext()
        }
    }
}
