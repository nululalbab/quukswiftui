//
//  AddShoppingModalCard.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 06/10/20.
//

import SwiftUI

struct AddShoppingModalCard: View {
    
    @ObservedObject var searchrecipeVM = SearchRecipeViewModel()
    
    @State var isUnitExpanded = false
    @State var isKategoriExpanded = false
    
    @Binding var NameBahan : String
    @Binding var QtyBahan : String
    @Binding var titleBahan : String
    
    @Binding var sort : Int
    
    let units: [Units] = [.gram,.kilogram,.one,.mililiter,.liter,.sdt,.sdm,.cup,.buah,.butir,.ikat,.potong,.secukupnya]
    let kategories: [Kategories] = [.protein,.karbohidrat,.sayuran,.buahBuahan,.dairy,.bumbu]
    
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Tambahkan bahan belanjaanmu!")
                .font(.subheadline)
                .foregroundColor(.black)
            
            TextField("Masukkan nama bahan...", text: self.$NameBahan)
                .font(.system(size: 13))
                .padding()
                .frame(height: 35, alignment: .center)
                .background(Color(#colorLiteral(red: 0.9960784314, green: 0.9764705882, blue: 0.7607843137, alpha: 1)))
                .cornerRadius(20)
                .disableAutocorrection(true)
            
            HStack(alignment:.top){
                TextField("Masukkan jumlah...", text: self.$QtyBahan)
                    .font(.system(size: 13))
                    .padding()
                    .frame(height: 35, alignment: .center)
                    .background(Color(#colorLiteral(red: 0.9960784314, green: 0.9764705882, blue: 0.7607843137, alpha: 1)))
                    .cornerRadius(20)
                    .disableAutocorrection(true)
                    .textContentType(.oneTimeCode)
                        .keyboardType(.numberPad)
                
                Spacer()
                
//                DisclosureGroup(isExpanded: $isUnitExpanded) {
//                    
//                    if isUnitExpanded {
//                        VStack(alignment: .leading, spacing: -10) {
//                            
//                            ForEach(units,id: \.id) { row in
//                                HStack{
//                                    AddShoppingModalCheckbox(checked: row.checked, units: row.unitName)
//                                    Spacer()
//                                }
//                            }
//                        }
//                    }
//                    
//                }
//                label: {
//                    Text("Pilih Unit")
//                        .font(.system(size: 13))
//                        .padding(.leading, 15)
//                }
//                .padding(.vertical, 10)
//                .padding(.trailing, 20)
//                .background(Color(#colorLiteral(red: 0.9960784314, green: 0.9764705882, blue: 0.7607843137, alpha: 1)))
//                .cornerRadius(20)
//                .foregroundColor(.black)
            }
            
//            Menu {
//                                    Picker(selection: $sort, label: Text("Sorting options")) {
//                                        Text("Proteion").tag(0)
//                                        Text("Karbohidrat").tag(1)
//                                        Text("Sayuran").tag(2)
//                                    }
//                                }
//            label: {
//                Text("Pilih Kategori")
//                    .font(.system(size: 13))
//                    .padding(.leading, 15)
//            }
//            .padding(.vertical, 10)
//            .padding(.trailing, 20)
//            .background(Color(#colorLiteral(red: 0.9960784314, green: 0.9764705882, blue: 0.7607843137, alpha: 1)))
//            .cornerRadius(20)
//            .foregroundColor(.black)
            
//            DisclosureGroup(isExpanded: $isKategoriExpanded) {
//
//                if isKategoriExpanded {
//                    VStack(alignment: .leading, spacing: -10) {
//
//                        ForEach(kategories,id: \.id) { row in
//                            HStack{
//                                AddShoppingModalCheckbox(checked: row.checked, units: row.katagoriName)
//                                Spacer()
//                            }
//                        }
//                    }
//                }
//
//            }
//            label: {
//                Text("Pilih Kategori")
//                    .font(.system(size: 13))
//                    .padding(.leading, 15)
//            }
//            .padding(.vertical, 10)
//            .padding(.trailing, 20)
//            .background(Color(#colorLiteral(red: 0.9960784314, green: 0.9764705882, blue: 0.7607843137, alpha: 1)))
//            .cornerRadius(20)
//            .foregroundColor(.black)
            
        }
    }
}

//struct AddShoppingModalCard_Previews: PreviewProvider {
//    static var previews: some View {
//        AddShoppingModalCard()
//    }
//}
