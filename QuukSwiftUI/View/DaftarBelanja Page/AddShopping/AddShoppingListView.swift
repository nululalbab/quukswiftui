//
//  AddShoppingListView.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 06/10/20.
//

import SwiftUI

struct AddShoppingListView: View {
    
    @ObservedObject var HistoryVM = HistoryViewModel()
    @State private var confirmationMessage = ""
    @State private var showingConfirmation = false
    
    @ObservedObject var cartData = ShoppingViewModel()
    
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(entity: ListCat.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \ListCat.date, ascending: false)])
    private var ListCategory: FetchedResults<ListCat>
    
    @FetchRequest(entity: Cart.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Cart.date, ascending: false)])
    var carts: FetchedResults<Cart>
    
    @State private var isNotFullScreen = false
    
    @ObservedObject var shoopingPlan = ShoopingPlanList()
    
    @State var action : Int? = 0
//    @Binding var tab: Tab
    @State var isLogin = false
    
    var body: some View {
//        ScrollView(showsIndicators: false){
            List {
                
                ForEach(ListCategory,id: \.id) { row in
                    AddShoppingListCard(cartData: cartData, candy: row, ingredientTitle: row.title ?? "")
//                    Text("\(row.recipeID!)")
                }
                    .onDelete(perform: deleteCart)
                    .buttonStyle(PlainButtonStyle())
                
                
                    VStack{
                    ZStack {
                        
                        NavigationLink(destination: HistoryPage(), tag: 2, selection: $action){
                            
                        }
                        
                        NavigationLink(destination: LoginPageView(isEmptyEmail: false, isEmptyPassword: false), tag: 3, selection: $action){
                            
                        }
                        
                    Text("Selesai Belanja")
                        .fontWeight(.bold)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 55, maxHeight: 55, alignment: .center)
                        .cornerRadius(15)
                        .buttonStyle(PlainButtonStyle())
                        .foregroundColor(.black)
                        .overlay(
                            RoundedRectangle(cornerRadius: 15)
                                .stroke(Color("secondaryColor"), lineWidth: 4))
                        .padding(.bottom,5)
                    }
                    .simultaneousGesture(TapGesture().onEnded{
                        
                        if self.isLogin{
                            
                            self.action = 3
                            }
                            
                            
                        else{
                            
                            for item in ListCategory.indices{
                                
                                if HistoryVM.history.recipes.count == 0 {
                                    HistoryVM.history.recipes.append(ListCategory[item].recipeID ?? "")
                                } else {
                                    continue
                                }
                                
                                for item2 in ListCategory[item].listCatArray{
                                    
                                    HistoryVM.history.ingredients.append(PostHistoryIngredient(name: item2.wrappedListName, weight: Double(item2.wrappedListQty)))
                                }
                                
                            }
                            
                            self.HistoryVM.CreateHistory()
        
                            
                            cartData.deleteCoreData(entity: "ListCat", context: viewContext)
                            cartData.deleteCoreData(entity: "Cart", context: viewContext)
                        
                            self.action = 2
                            
                        }
                        
                        
                        
                    })
                    }
                
                
                
                VStack{
                ZStack {
                    Text("Pesan Online")
                        .fontWeight(.bold)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 55, maxHeight: 55, alignment: .center)
                        .background(Color("secondaryColor"))
                        .cornerRadius(15)
                    NavigationLink(destination: Text("Coming Soon")){
                    }
                    .buttonStyle(PlainButtonStyle())
                }
                }
            }
            .onAppear{
                
                
                
                if DefaultsKeys.defaults.get(.bearer) == nil{
                    self.isLogin = true
                }else{
                    self.isLogin = false
                }

            }
            .listStyle(PlainListStyle())
            .navigationBarTitle("Daftar Belanja", displayMode: .inline)
            .navigationBarItems(trailing: HStack {
                Button(action: {}) {
                    Image(systemName: "plus.circle.fill")
                        .font(.title)
                        .foregroundColor(Color("primaryColor"))
                        .onTapGesture{
                            isNotFullScreen.toggle()
                        }
                        .sheet(isPresented: $isNotFullScreen, content: {
                            NotFullScreen(isNotFullScreen: $isNotFullScreen)
                        })
                }
            })
            .padding(.bottom,60)
//        }.padding()
    }
    
    private func saveContext() {
        do {
            try viewContext.save()
        } catch  {
            let error = error as NSError
            fatalError("Unresolved error: \(error)")
        }
    }
    
    private func deleteCart(offsets: IndexSet){
        withAnimation{
            offsets.map{ ListCategory[$0] }.forEach(viewContext.delete)
            saveContext()
        }
    }
    
    
    
}
