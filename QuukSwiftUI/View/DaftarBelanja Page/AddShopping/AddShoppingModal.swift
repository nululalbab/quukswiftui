//
//  AddShoppingModal.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 06/10/20.
//

import SwiftUI

struct AddShoppingModal: View {
    @State private var isNotFullScreen = false
    
    var body: some View {
        Text("Hello, not full screen!")
            .padding()
            .onTapGesture{
                isNotFullScreen.toggle()
            }
            .sheet(isPresented: $isNotFullScreen, content: {
                NotFullScreen(isNotFullScreen: $isNotFullScreen)
            })
    }
}

struct AddShoppingModal_Previews: PreviewProvider {
    static var previews: some View {
        AddShoppingModal()
    }
}

//MARK: asdasd
struct NotFullScreen: View {
    @Environment(\.managedObjectContext) private var viewContext
    @ObservedObject var cartData = ShoppingViewModel()
    @FetchRequest(entity: ListCat.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \ListCat.date, ascending: false)])
    private var ListCategory: FetchedResults<ListCat>
    
    @Binding var isNotFullScreen: Bool
    @State var NameBahan = ""
    @State var QtyBahan = ""
    @State var titleBahan = ""
    @State var sort = 0
    
    var body: some View {
        NavigationView{
            ScrollView{
                VStack(alignment: .leading) {
                    
                    AddShoppingModalCard(NameBahan: $NameBahan, QtyBahan: $QtyBahan, titleBahan: $titleBahan, sort: $sort)
                    Spacer()
                    
                }
                .padding()
                
                
            }
            .navigationBarTitle("Tambah Belanjaan")
            .navigationBarItems(trailing: Button(action: {
                cartData.addToListCat(context: viewContext, entity: ListCategory, namaBahan: NameBahan, QtyBahan: Float(QtyBahan)!)
                
                self.isNotFullScreen.toggle()
            }, label: {
                Text("Simpan")
                    .foregroundColor(Color("primaryColor"))
                
            }))
        }
        .buttonStyle(PlainButtonStyle())
    }
}
