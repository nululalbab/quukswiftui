
//
//  AddShoppingListCard.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 06/10/20.
//

import SwiftUI

struct AddShoppingListCard: View {
    
    @ObservedObject var cartData : ShoppingViewModel
        
        var candy:FetchedResults<ListCat>.Element
    
//    let items: [Ingredients] = [.apple,.tepung,.telur]
    
    var ingredientTitle: String
    
    var body: some View {
        VStack {
            VStack(spacing: 10) {
                
                HStack{
                    VStack(alignment: .leading){
                        Text(ingredientTitle)
                            .font(.body)
                            .fontWeight(.bold)
                    }
                    Spacer()
                }
                
                VStack(alignment:.leading) {
                    ForEach(candy.listCatArray,id: \.id) { row in
                        HStack{
                            AddShoppingCheckbox(cartData: cartData, candy: row, checked: row.check)
                            Spacer()
                        }
                    }
                    .padding(.bottom,-20)
                }
                .padding(.all,10)
                .padding(.bottom,20)
                .background(Color("thirdColor"))
                .foregroundColor(.black)
                .cornerRadius(20)
            }
            .padding(.bottom,10)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            //                .background(Color.red)
            
        }
    }
}

