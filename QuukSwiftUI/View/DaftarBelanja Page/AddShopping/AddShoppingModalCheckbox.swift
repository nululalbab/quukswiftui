//
//  AddShoppingModalCheckbox.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 06/10/20.
//

import SwiftUI

struct AddShoppingModalCheckbox: View {
    
    @State var checked: Bool
    var units : String
    
    var body: some View {
        Button(action: {
            self.checked.toggle()
        }) {
            
            HStack(spacing:-5){
                
                Image(systemName: self.checked ? "checkmark.square.fill" : "square")
                    .foregroundColor(self.checked ? .green : .green)
                    .frame(width: 40, height: 40, alignment: .center)
                
                Text(self.units)
                    .font(.system(size: 16, weight: .regular, design: .rounded))
                    .foregroundColor(.black)
            }
        }
    }
}

struct AddShoppingModalCheckbox_Previews: PreviewProvider {
    static var previews: some View {
        AddShoppingModalCheckbox(checked: false, units: "gram")
    }
}
