//
//  AddShoppingCheckbox.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 06/10/20.
//

import SwiftUI

struct AddShoppingCheckbox: View {
    
    @ObservedObject var cartData : ShoppingViewModel
    @Environment(\.managedObjectContext) private var viewContext
    
    var candy:ListItem
    
    @State var checked: Bool
    
    var body: some View {
        
        Button(action: {
            self.checked.toggle()
            self.candy.check.toggle()
            try! viewContext.save()
        }) {
            
            HStack(spacing:-5){
                
                Image(systemName: self.candy.check ? "checkmark.seal.fill" : "square")
                    .foregroundColor(self.checked ? .green : .green)
                    .frame(width: 40, height: 40, alignment: .center)
                
                Text(candy.wrappedListName)
                    .font(.system(size: 16, weight: .regular, design: .rounded))
                    .strikethrough(self.checked ? true : false, color: Color(.black))
                    .padding(.vertical,10)
                    .lineLimit(nil)
                    .fixedSize(horizontal: false, vertical: true)
                
                Spacer()
                
                Text("\(String(format: "%.1f" ,candy.wrappedListQty)) g")
                    
                    .font(.caption)
                    .foregroundColor(Color.secondary)
                    .strikethrough(self.checked ? true : false, color: Color(.black))
            }
  
        }
    }
}

