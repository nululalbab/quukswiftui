//
//  BookmarkPage.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 06/10/20.
//

import SwiftUI
import ToastUI

struct BookmarkPage: View {

    @ObservedObject var fetchfavoriteVM  : FetchFavoriteViewModel = FetchFavoriteViewModel()

    @State var isLogin = false

    var body: some View {

        VStack{
            if self.fetchfavoriteVM.isEmpty {
                VStack{
                    ZStack {
                        Image("EMPTY STATE 2")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(minWidth: 350, maxWidth: .infinity, minHeight: 350, maxHeight: 350, alignment: .center)
                    }
                    Text("Kamu belum memasukkan resep ke dalam bookmark")
                        .fontWeight(.bold)
                        .padding(.top,-35)
                        .padding(.bottom,200)
                        .multilineTextAlignment(.center)
                        .lineLimit(nil)
                        .fixedSize(horizontal: false, vertical: true)
                }
                .padding(.horizontal,10)
                .padding(.vertical,70)
                .foregroundColor(Color("secondaryColor"))
            }else{

                List(self.fetchfavoriteVM.favorite, id: \.id) { favorite in

                    NavigationLink(destination: DetailRecipeView(id:"http://www.edamam.com/recipe/\(favorite.uri ?? "")")){
                        FavoriteLongCardView(favorite: favorite)

                    }
                    .buttonStyle(PlainButtonStyle())
                }
            }

        } .padding(.trailing, 20)
        .navigationBarTitle("Bookmark", displayMode: .automatic)
        .onAppear {
            self.fetchfavoriteVM.Fetch()
        }
        .toast(isPresented: self.$fetchfavoriteVM.isLoading) {

        } content: {
            ToastView("Loading...")
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
    }
}

struct BookmarkPage_Previews: PreviewProvider {
    static var previews: some View {
        BookmarkPage()
    }
}

struct FavoriteLongCardView: View {

    let favorite : Favorite

    var body: some View {

        VStack(alignment: .center) {
            ZStack(alignment: .leading) {

                VStack(alignment: .leading){

                }
                .frame(width: UIScreen.main.bounds.size.width * 0.8, height: 120)
                .background(Color("thirdColor"))
                .cornerRadius(15)
                .offset(x: 40, y: 0)


                HStack {
                    URLImage(url: favorite.image ?? "")
                        .aspectRatio(contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipShape(RoundedRectangle(cornerRadius: 10))

                    VStack (alignment: .leading, spacing: 5){
                        Text(favorite.label!)
                            .font(.system(size: 17))
                            .fontWeight(.bold)
                            .foregroundColor(Color.black)
                            .frame(width: 220, alignment: .leading)
                            .lineLimit(2)

                        Text(favorite.image ?? "")
                            .font(.system(size: 13))
                            .fontWeight(.regular)
                            .foregroundColor(Color.black)
                            .lineLimit(2)
                            .frame(width: 220, alignment: .leading)

                        HStack {
                            Text("Porsi: \(favorite.servings ?? 0)  |  \(favorite.ingredients?.count ?? 0) Bahan")
                                .foregroundColor(Color.gray)
                                .font(.system(size: 13))
                                .fontWeight(.regular)
                                .foregroundColor(Color.gray)

                            Spacer()

                            VStack {

                                Text("Masak!")
                                    .font(.system(size: 13))
                                    .fontWeight(.bold)
                                    .foregroundColor(Color.white)
                                    .frame(width: 80, height: 30, alignment: .center)
                                    .background(Color("primaryColor"))
                                    .cornerRadius(20)
                            }
                            .padding(.trailing)
                        }
                    }
                }
            }
            .padding(.trailing)

        }
    }
}
