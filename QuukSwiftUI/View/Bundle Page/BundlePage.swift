//
//  BundlePage.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 20/10/20.
//

import SwiftUI

struct BundlePage: View {
    
    let bundleData: BundleData
    @ObservedObject var searchrecipeVM : SearchRecipeViewModel = SearchRecipeViewModel()
    
    let columns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)
    @State private var searchText : String = ""
    @State private var isEditing = false
    
    
    var body: some View {
        
        ScrollView {
            URLImage(url: bundleData.image)
                .aspectRatio(contentMode: .fill)
                .frame(height: 185, alignment: Alignment.top)
                .offset(y:-50)
                .clipped()
            VStack{
                
                // Search Bar
                HStack {
                    TextField("Cari Resep, Bahan..", text: $searchText)
                        .padding(7)
                        .padding(.horizontal, 25)
                        .background(Color("thirdColor"))
                        .cornerRadius(8)
                        .disableAutocorrection(true)
                        .overlay(
                            HStack {
                                Image(systemName: "magnifyingglass")
                                    .foregroundColor(.gray)
                                    .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                                    .padding(.leading, 8)
                                
                                if isEditing {
                                    Button(action: {
                                        self.searchText = ""
                                        
                                    }) {
                                        Image(systemName: "multiply.circle.fill")
                                            .foregroundColor(.gray)
                                            .padding(.trailing, 8)
                                    }
                                }
                            }
                        )
                        .padding(.horizontal, 10)
                        .onTapGesture {
                            self.isEditing = true
                        }
                    
                    if isEditing {
                        Button(action: {
                            self.isEditing = false
                            self.searchText = ""
                            
                            // Dismiss the keyboard
                            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                        }) {
                            Text("Cancel")
                        }
                        .padding(.trailing, 10)
                        .transition(.move(edge: .trailing))
                        .animation(.default)
                        .foregroundColor(Color("secondaryColor"))
                    }
                }
                .cornerRadius(20)
                .padding(.horizontal, 20)
                .padding(.top, 41)
                
                // Grid View
                LazyVGrid(columns: columns) {
                    ForEach(bundleData.recipes.filter({ searchText.isEmpty ? true : $0.label!.contains(searchText) }), id : \.label) { recipe in
                        let idRecipe = "http://www.edamam.com/recipe/\(recipe.uri ?? "")"
                        
                        NavigationLink(destination: DetailRecipeView(id: idRecipe)){
                        BundlePageCell(recipe: recipe,image: recipe.image ?? "", title: recipe.label ?? "", description: recipe.source ?? "", ingridients: recipe.ingredients!)
                        }.buttonStyle(PlainButtonStyle())
                    }
                }.padding(EdgeInsets(top: 50, leading: 0, bottom: 50, trailing: 0))
            }
            .cornerRadius(20)
            .padding(.top, -25)
        }
        .navigationBarTitle(bundleData.name, displayMode: .automatic)
    }
}

//struct BundlePagePage_Previews: PreviewProvider {
//    static var previews: some View {
//        BundlePage()
//    }
//}

struct BundlePageCell : View {
    
    let recipe : Recipe
    let image : String
    let title : String
    let description : String
    let ingridients : [Ingredient]
    
    var body: some View {
        VStack{
            ZStack{
                VStack(alignment: .leading, spacing: 2){
                    Text(title)
                        .font(.system(size: 14))
                        .fontWeight(.bold)
                        .lineSpacing(0)
                        .padding(EdgeInsets(top: 70, leading: 4, bottom: 0, trailing: 4))
                    Text(description)
                        .font(.caption)
                        .padding(EdgeInsets(top: 2, leading: 4, bottom: 0, trailing: 4))
                    
                    Text("Porsi \(String(format: "%.0f",recipe.yield ?? 0))  |  \(ingridients.count) Bahan")
                        .foregroundColor(Color.gray)
                        .font(.caption)
                        .padding(EdgeInsets(top: 10, leading: 4, bottom: 0, trailing: 4))
                    
                    Spacer()
                    HStack{
                        Spacer()
                        Text("Masak")
                            .font(.caption)
                            .frame(width: 130, height: 24, alignment: .center)
                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 4, trailing: 0))
                            .foregroundColor(Color.white)
                            .background(Color("primaryColor"))
                            .cornerRadius(15)
                        Spacer()
                    }
                    
                    
                }.frame(width: 150, height: 200)
                .background(Color("thirdColor"))
                .cornerRadius(15)
                
                URLImage(url: image)
                    .frame(width: 120, height: 120)
                    .cornerRadius(20)
                    .offset( y: -100)
            }
            
            
        }.frame(width: 150, height: 250)
        .padding(EdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0))
    }
}
