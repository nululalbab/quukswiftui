//
//  ExampleCalendarView.swift
//  TutorialElegantCalendar
//
//  Created by Lois Pangestu on 13/10/20.
//

import SwiftUI

struct ExampleCalendarView: View {

    @ObservedObject private var calendarManager: ElegantCalendarManager
    var fetchMealPlanVM : FetchMealPlanVM

    let visitsByDay: [Date: [MealPlan]]

    @State private var calendarTheme: CalendarTheme = .default

    init(ascVisits: [MealPlan], initialMonth: Date?, fetchMealPlanVM: FetchMealPlanVM) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let firstDate = Date.daysFromToday(-30*3)
        let lastDate = Date.daysFromToday(30*3)
        
        let configuration = CalendarConfiguration(
            calendar: currentCalendar,
            startDate: firstDate,
            endDate: lastDate)

        calendarManager = ElegantCalendarManager(
            configuration: configuration,
            initialMonth: initialMonth)

        visitsByDay = Dictionary(
            grouping: ascVisits,
            by: { currentCalendar.startOfDay(for: dateFormatter.date(from: $0.datetime)!) })
        
        self.fetchMealPlanVM = fetchMealPlanVM
        calendarManager.datasource = self
        calendarManager.delegate = self
        
        
    }

    var body: some View {
//        ForEach(fetchMealPlanVM.mealPlan, id: \.id) { result in
//            MealPlanCell(mealPlan: result)
//        }
        VStack {
            ElegantCalendarView(calendarManager: calendarManager, fetchMealPlanVM: fetchMealPlanVM)
                .theme(calendarTheme)
                .allowsHaptics(false)
                .frame(height: 300)
                .padding(.top, -50)
                .padding(.bottom, 50)
            
            ScrollView(showsIndicators: false) {
                ForEach(0..<fetchMealPlanVM.mealPlan.count, id: \.self) { result in
//                    NavigationLink(destination: DetailRecipeView(recipe: fetchMealPlanVM.mealPlan[result], ingridient: recipe.ingredients!, totalnutrients: recipe.totalNutrients! )){
                    MealPlanCell(mealPlan: fetchMealPlanVM.mealPlan[result])
//                    }.buttonStyle(PlainButtonStyle())
                }
            }
            .padding(.top, 25)
            .padding(.horizontal, 18)
            .padding(.bottom, 50)
        }
        .onAppear(){
//            calendar(didSelectDay: Date())
        }
    }

    private var changeThemeButton: some View {
        ChangeThemeButton(calendarTheme: $calendarTheme)
    }
    
}

extension ExampleCalendarView: ElegantCalendarDataSource {

    func calendar(viewForSelectedDate date: Date, dimensions size: CGSize) -> AnyView {
        let startOfDay = currentCalendar.startOfDay(for: date)
        return VisitsListView(mealPlans: visitsByDay[startOfDay] ?? [], height: size.height).erased
    }
    
}

extension ExampleCalendarView: ElegantCalendarDelegate {

    func calendar(didSelectDay date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let selectedDate = dateFormatter.string(from: date)
        
        print("Selected date: \(selectedDate)")
        let completionhandler:(Bool)->Void = { (sucess) in
            if sucess {
//                print("Data : ",fetchMealPlanVM.mealPlan)
                
            } else {
                print("Error")
            }

        }
        fetchMealPlanVM.fetchDataWithHandler(selectedDate: selectedDate, sucess: true, completionHandler: completionhandler)
    }

    func calendar(willDisplayMonth date: Date) {
//        print("Month displayed: \(date)")
    }

    func calendar(didSelectMonth date: Date) {
        print("Selected month: \(date)")
    }

    func calendar(willDisplayYear date: Date) {
        print("Year displayed: \(date)")
    }

}

//struct ExampleCalendarView_Previews: PreviewProvider {
//    static var previews: some View {
//        ExampleCalendarView(ascVisits: Visit.mocks(start: .daysFromToday(-365*2), end: .daysFromToday(365*2)), initialMonth: nil)
//    }
//}
