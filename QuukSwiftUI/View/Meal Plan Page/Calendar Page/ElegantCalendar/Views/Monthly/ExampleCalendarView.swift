//
//  ExampleCalendarView.swift
//  TutorialElegantCalendar
//
//  Created by Lois Pangestu on 13/10/20.
//

import SwiftUI

struct ExampleCalendarView: View {

    @ObservedObject private var calendarManager: ElegantCalendarManager
    @ObservedObject var fetchMealPlanVM : FetchMealPlanVM

    let visitsByDay: [Date: [MealPlan]]
    @State var selectedDate = String()
    
    @State private var isLogin = false

    @State private var calendarTheme: CalendarTheme = .default

    init(ascVisits: [MealPlan], initialMonth: Date?, fetchMealPlanVM: FetchMealPlanVM) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let firstDate = Date.daysFromToday(-30*3)
        let lastDate = Date.daysFromToday(30*3)
        
        let configuration = CalendarConfiguration(
            calendar: currentCalendar,
            startDate: firstDate,
            endDate: lastDate)

        calendarManager = ElegantCalendarManager(
            configuration: configuration,
            initialMonth: initialMonth)

        visitsByDay = Dictionary(
            grouping: ascVisits,
            by: { currentCalendar.startOfDay(for: dateFormatter.date(from: $0.datetime)!) })
        
        self.fetchMealPlanVM = fetchMealPlanVM
        calendarManager.datasource = self
        calendarManager.delegate = self
        
        calendarManager.scrollBackToToday()
    }

    var body: some View {
        VStack {
            ElegantCalendarView(calendarManager: calendarManager, fetchMealPlanVM: fetchMealPlanVM)
                .theme(calendarTheme)
                .allowsHaptics(false)
                .frame(height: 300)
                .padding(.top, 50)
                .padding(.bottom, 50)
        }
    }

    private var changeThemeButton: some View {
        ChangeThemeButton(calendarTheme: $calendarTheme)
    }
    
}

extension ExampleCalendarView: ElegantCalendarDataSource {

    func calendar(viewForSelectedDate date: Date, dimensions size: CGSize) -> AnyView {
        let startOfDay = currentCalendar.startOfDay(for: date)
        return VisitsListView(mealPlans: visitsByDay[startOfDay] ?? [], selectedDay: startOfDay, fetchMealPlanVM: fetchMealPlanVM, height: size.height).erased
    }
}

extension ExampleCalendarView: ElegantCalendarDelegate {

    func calendar(didSelectDay date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateString = dateFormatter.string(from: date)
        
        selectedDate = dateString
    }

    func calendar(willDisplayMonth date: Date) {
        
    }

    func calendar(didSelectMonth date: Date) {
        print("Selected month: \(date)")
    }
}
