// Kevin Li - 10:53 PM - 6/6/20

import SwiftUI

struct MonthView: View, MonthlyCalendarManagerDirectAccess {

    @Environment(\.calendarTheme) var theme: CalendarTheme

    @ObservedObject var calendarManager: MonthlyCalendarManager
    
    var fetchMealPlanVM : FetchMealPlanVM

    let month: Date

    private var weeks: [Date] {
        guard let monthInterval = calendar.dateInterval(of: .month, for: month) else {
            return []
        }
        return calendar.generateDates(
            inside: monthInterval,
            matching: calendar.firstDayOfEveryWeek)
    }

    private var isWithinSameMonthAndYearAsToday: Bool {
        calendar.isDate(month, equalTo: Date(), toGranularities: [.month, .year])
    }

    var body: some View {
        
//        ScrollView{
            VStack(spacing: 20) {
                monthYearHeader
                    .padding(.leading, CalendarConstants.Monthly.outerHorizontalPadding)
//                    .onTapGesture { self.communicator?.showYearlyView() }
                weeksViewWithDaysOfWeekHeader
                if selectedDate != nil {
                    calenderAccessoryView
                        .frame(height:10)
                        .padding(.leading, CalendarConstants.Monthly.outerHorizontalPadding)
                        .id(selectedDate!)
                    
                    
                }
                // List view meal plan
                // filter data berdasarkan tanggal dipilih
                // Datanya masukin ke Cell
                
//                ForEach(fetchMealPlanVM.mealPlan, id: \.id) { result in
//                    MealPlanCell(mealPlan: result)
//                }
                
                
//                Text("Ini : \(fetchMealPlanVM.mealPlan.description)")
//                ForEach(fetchMealPlanVM.mealPlan, id: \.id) { result in
//                    ForEach(0..<1) { _ in
//                        MealPlanCell(mealPlan: result)
//                    }
//                }
                
//                List(self.fetchMealPlanVM.mealPlan, id: \.id) { plan in
    //                NavigationLink(destination: DetailRecipeView(recipe: Recipe, ingridient: [Ingredient], totalnutrients: [String : Total])){
    //                    NavigationLink(destination: DetailRecipeView(id:"http://www.edamam.com/recipe/\(plan.uri ?? "")/lumpia")){
//                            MealPlanCell(mealPlan: plan)
    //                    }
    //                    .buttonStyle(PlainButtonStyle())
    //                    .padding(.leading, 15)
    //                    .background(Color("thirdColor"))
    //                    .cornerRadius(20)
    //                    .padding(.trailing, 20)
//                    }
            }
            .padding(.top, CalendarConstants.Monthly.topPadding)
            .frame(width: CalendarConstants.Monthly.cellWidth, height: CalendarConstants.cellHeight)
            
//            ScrollView(.horizontal, showsIndicators: false) {
//                HStack(alignment: .top) {
//                    ForEach(fetchMealPlanVM.mealPlan, id: \.id) { result in
//                        ForEach(0..<1) { _ in
//                            MealPlanCell(mealPlan: result)
//                        }
//                    }
//                }
//                .padding(.leading, 20)
//                .padding(.vertical, 20)
//                
//                Spacer()
//            }
//        }
        
    }

}

private extension MonthView {

    var monthYearHeader: some View {
        HStack {
            monthText
            yearText
            Spacer()
        }
    }

    var monthText: some View {
        Text(month.fullMonth)
            .font(.system(size: 22))
            .bold()
            .foregroundColor(.primary)
    }

    var yearText: some View {
        Text(month.year)
            .font(.system(size: 22))
            .bold()
            .foregroundColor(.primary)
    }

}

private extension MonthView {

    var weeksViewWithDaysOfWeekHeader: some View {
        VStack(spacing: 15) {
            daysOfWeekHeader
            weeksViewStack
        }
    }

    var daysOfWeekHeader: some View {
        HStack(spacing: CalendarConstants.Monthly.gridSpacing) {
            ForEach(calendar.dayOfWeekInitials, id: \.self) { dayOfWeek in
                Text(dayOfWeek)
                    .font(.caption)
                    .frame(width: CalendarConstants.Monthly.dayWidth)
                    .foregroundColor(.gray)
            }
        }
    }

    var weeksViewStack: some View {
        VStack(spacing: CalendarConstants.Monthly.gridSpacing) {
            ForEach(weeks, id: \.self) { week in
                WeekView(calendarManager: self.calendarManager, week: week)
            }
        }
    }

}

private extension MonthView {

    var calenderAccessoryView: some View {
        CalendarAccessoryView(calendarManager: calendarManager)
    }

}

private struct CalendarAccessoryView: View, MonthlyCalendarManagerDirectAccess {

    let calendarManager: MonthlyCalendarManager

    @State private var isVisible = false

    private var numberOfDaysFromTodayToSelectedDate: Int {
        let startOfToday = calendar.startOfDay(for: Date())
        let startOfSelectedDate = calendar.startOfDay(for: selectedDate!)
        return calendar.dateComponents([.day], from: startOfToday, to: startOfSelectedDate).day!
    }

    private var isNotYesterdayTodayOrTomorrow: Bool {
        abs(numberOfDaysFromTodayToSelectedDate) > 1
    }

    var body: some View {
        VStack {
            selectedDayInformationView
//            GeometryReader { geometry in
//                self.datasource?.calendar(viewForSelectedDate: self.selectedDate!,
//                                          dimensions: geometry.size)
//            }
        }
        .onAppear(perform: makeVisible)
        .opacity(isVisible ? 1 : 0)
        .animation(.easeInOut(duration: 0.5))
    }

    private func makeVisible() {
        isVisible = true
    }

    private var selectedDayInformationView: some View {
        HStack {
            VStack(alignment: .leading) {
                dayOfWeekWithMonthAndDayText
                if isNotYesterdayTodayOrTomorrow {
                    daysFromTodayText
                }
            }
            .padding(.top, 20)
            
            Spacer()
        }
    }

    private var dayOfWeekWithMonthAndDayText: some View {
        let monthDayText: String
        if numberOfDaysFromTodayToSelectedDate == -1 {
            monthDayText = "Kemarin"
        } else if numberOfDaysFromTodayToSelectedDate == 0 {
            monthDayText = "Hari Ini"
        } else if numberOfDaysFromTodayToSelectedDate == 1 {
            monthDayText = "Besok"
        } else {
            monthDayText = selectedDate!.dayOfWeekWithMonthAndDay
        }

        return Text(monthDayText)
            .font(.subheadline)
            .bold()
    }

    private var daysFromTodayText: some View {
        let isBeforeToday = numberOfDaysFromTodayToSelectedDate < 0
        let daysDescription = isBeforeToday ? "hari yang lalu" : "hari mendatang"

        return Text("\(abs(numberOfDaysFromTodayToSelectedDate)) \(daysDescription)")
            .font(.system(size: 10))
            .foregroundColor(.gray)
    }

}

//struct MonthView_Previews: PreviewProvider {
//    static var previews: some View {
//        LightDarkThemePreview {
//            MonthView(calendarManager: .mock, month: Date())
//
//            MonthView(calendarManager: .mock, month: .daysFromToday(45))
//        }
//    }
//}
