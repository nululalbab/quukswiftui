// Kevin Li - 2:26 PM - 6/14/20

import ElegantPages
import SwiftUI

public struct MonthlyCalendarView: View, MonthlyCalendarManagerDirectAccess {

    var theme: CalendarTheme = .default
    public var axis: Axis = .vertical

    @ObservedObject public var calendarManager: MonthlyCalendarManager
    @ObservedObject var fetchMealPlanVM : FetchMealPlanVM

    private var isTodayWithinDateRange: Bool {
        Date() >= calendar.startOfDay(for: startDate) &&
            calendar.startOfDay(for: Date()) <= endDate
    }

    private var isCurrentMonthYearSameAsTodayMonthYear: Bool {
        calendar.isDate(currentMonth, equalTo: Date(), toGranularities: [.month, .year])
    }

    init(calendarManager: MonthlyCalendarManager, fetchMealPlanVM: FetchMealPlanVM) {
        self.calendarManager = calendarManager
        self.fetchMealPlanVM = fetchMealPlanVM
    }

    public var body: some View {
        GeometryReader { geometry in
            self.content(geometry: geometry)
        }
    }

    private func content(geometry: GeometryProxy) -> some View {
        CalendarConstants.Monthly.cellWidth = geometry.size.width

        return ZStack(alignment: .top) {
            monthsList

            // FEATURE : Return to Current Date
//            if isTodayWithinDateRange && !isCurrentMonthYearSameAsTodayMonthYear {
//                leftAlignedScrollBackToTodayButton
//                    .padding(.trailing, CalendarConstants.Monthly.outerHorizontalPadding)
//                    .offset(y: CalendarConstants.Monthly.topPadding + 3)
//            }
        }
        .frame(height: CalendarConstants.cellHeight)
    }

    private var monthsList: some View {
        ElegantHList(manager: listManager,
                     pageTurnType: .monthlyEarlyCutoff,
                     viewForPage: monthView)
            .onPageChanged(configureNewMonth)
            .frame(width: CalendarConstants.Monthly.cellWidth)
    }

    private func monthView(for page: Int) -> AnyView {
        MonthView(calendarManager: calendarManager, fetchMealPlanVM: fetchMealPlanVM, month: months[page])
            .environment(\.calendarTheme, theme)
            .erased
    }

    private var leftAlignedScrollBackToTodayButton: some View {
        HStack {
            Spacer()
            ScrollBackToTodayButton(scrollBackToToday: scrollBackToToday,
                                    color: theme.primary)
        }
    }
}

private extension PageTurnType {

    static var monthlyEarlyCutoff: PageTurnType = .earlyCutoff(config: .monthlyConfig)

}

public extension EarlyCutOffConfiguration {

    static let monthlyConfig = EarlyCutOffConfiguration(
        scrollResistanceCutOff: 40,
        pageTurnCutOff: 80,
        pageTurnAnimation: .spring(response: 0.3, dampingFraction: 0.95))

}
