// Kevin Li - 6:19 PM - 6/6/20

import ElegantPages
import SwiftUI

public struct ElegantCalendarView: View {

    @ObservedObject var fetchMealPlanVM : FetchMealPlanVM
    var theme: CalendarTheme = .default
    public var axis: Axis = .vertical

    public let calendarManager: ElegantCalendarManager

    init(calendarManager: ElegantCalendarManager, fetchMealPlanVM: FetchMealPlanVM) {
        self.calendarManager = calendarManager
        self.fetchMealPlanVM = fetchMealPlanVM
    }

    public var body: some View {
        VStack{
            content
        }
        .frame(width: screen.width, height: screen.height, alignment: .top)
    }
    
    private var content: some View {
        monthlyCalendarView
        .erased
    }

    private var yearlyCalendarView: some View {
        YearlyCalendarView(calendarManager: calendarManager.yearlyManager)
            .axis(axis.inverted)
            .theme(theme)
    }

    private var monthlyCalendarView: some View {
        MonthlyCalendarView(calendarManager: calendarManager.monthlyManager, fetchMealPlanVM: fetchMealPlanVM)
            .axis(axis.inverted)
            .theme(theme)
    }
}
