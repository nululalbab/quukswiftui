//
//  VisitsListView.swift
//  TutorialElegantCalendar
//
//  Created by Lois Pangestu on 13/10/20.
//

import SwiftUI

struct VisitsListView: View {
    @ObservedObject var fetchMealPlanVM  : FetchMealPlanVM = FetchMealPlanVM()
    let mealPlans : [MealPlan]
    @State var planIndex = 0
    
    //    let visits: [Visit]
    let numberOfCellsInBlock: Int
    
    init(mealPlans: [MealPlan], height: CGFloat) {
        self.mealPlans = mealPlans
        numberOfCellsInBlock = Int(height / (VisitPreviewConstants.cellHeight + VisitPreviewConstants.cellPadding*2))
    }
    
    private var range: Range<Int> {
        let exclusiveEndIndex = planIndex + numberOfCellsInBlock
        guard mealPlans.count > numberOfCellsInBlock &&
            exclusiveEndIndex <= mealPlans.count else {
            return planIndex..<mealPlans.count
        }
        return planIndex..<exclusiveEndIndex
    }

    var body: some View {
        visitsPreviewList
            .animation(.easeInOut)
            
    }

    private func shiftActivePreviewVisitIndex() {
        let startingVisitIndexOfNextSlide = planIndex + numberOfCellsInBlock
        let startingVisitIndexOfNextSlideIsValid = startingVisitIndexOfNextSlide < mealPlans.count
        planIndex = startingVisitIndexOfNextSlideIsValid ? startingVisitIndexOfNextSlide : 0
    }

    private var visitsPreviewList: some View {
        VStack(spacing: 0) {
//            ScrollView{
//                ForEach(visits[range]) { visit in
//                    VisitCell(visit: visit)
//                }
//                .padding(.leading, 15)
//                .background(Color("thirdColor"))
//                .cornerRadius(20)
//                .padding(.trailing, 20)
//
//            }
            
            List(self.fetchMealPlanVM.mealPlan, id: \.id) { plan in
//                NavigationLink(destination: DetailRecipeView(recipe: Recipe, ingridient: [Ingredient], totalnutrients: [String : Total])){
//                    NavigationLink(destination: DetailRecipeView(id:"http://www.edamam.com/recipe/\(plan.uri ?? "")/lumpia")){
                        MealPlanCell(mealPlan: plan)
//                    }
//                    .buttonStyle(PlainButtonStyle())
//                    .padding(.leading, 15)
//                    .background(Color("thirdColor"))
//                    .cornerRadius(20)
//                    .padding(.trailing, 20)
                }
        }
        
    }
}

//struct VisitsListView_Previews: PreviewProvider {
//    static var previews: some View {
//        DarkThemePreview {
//            VisitsListView(visits: Visit.mocks(start: Date(), end: .daysFromToday(2)), height: 300)
//        }
//    }
//}
