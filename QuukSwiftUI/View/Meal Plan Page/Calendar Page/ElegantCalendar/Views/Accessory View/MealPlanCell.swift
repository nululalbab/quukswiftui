//
//  MealPlanCell.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 05/11/20.
//

import SwiftUI

struct MealPlanCell: View {
    @State var mealPlan : MealPlan
    @State var dateString = String()

    var body: some View {
        HStack {
            tagView

            VStack(alignment: .leading) {
                planRecipe
                planDate
                planNote
            }
            Spacer()
        }
        .frame(height: VisitPreviewConstants.cellHeight)
        .padding(.horizontal, 18)
        .background(Color("thirdColor"))
        .cornerRadius(20)
    }
}

private extension MealPlanCell {

    var tagView: some View {
        RoundedRectangle(cornerRadius: 8)
            .fill(Color("primaryColor"))
            .frame(width: 5, height: 50)
    }
    
    var planRecipe: some View {
        Text(mealPlan.recipe.label)
            .font(.system(size: 14))
            .lineLimit(1)
    }

    var planDate: some View {
        Text(mealPlan.datetime)
            .font(.system(size: 14))
            .lineLimit(1)
    }
    
    var planNote: some View {
        Text(mealPlan.name)
            .font(.system(size: 11))
            .lineLimit(1)
            .foregroundColor(Color(.systemGray))
    }
}
