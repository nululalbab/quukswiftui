//
//  VisitsListView.swift
//  TutorialElegantCalendar
//
//  Created by Lois Pangestu on 13/10/20.
//

import SwiftUI

struct VisitsListView: View {
    @ObservedObject var fetchMealPlanVM : FetchMealPlanVM
    var mealPlans : [MealPlan]
    @State var sortedMealPlans = [MealPlan]()
    @State var filteredMealPlans = [MealPlan]()
    var selectedDay : Date
    @State var planIndex = 0
    
    let numberOfCellsInBlock: Int
    
    init(mealPlans: [MealPlan], selectedDay: Date ,fetchMealPlanVM: FetchMealPlanVM, height: CGFloat) {
        self.mealPlans = mealPlans
        self.selectedDay = selectedDay
        self.fetchMealPlanVM = fetchMealPlanVM
        numberOfCellsInBlock = Int(height / (VisitPreviewConstants.cellHeight + VisitPreviewConstants.cellPadding*2))
    }
    
    private var range: Range<Int> {
        let exclusiveEndIndex = planIndex + numberOfCellsInBlock
        guard mealPlans.count > numberOfCellsInBlock &&
            exclusiveEndIndex <= mealPlans.count else {
            return planIndex..<mealPlans.count
        }
        return planIndex..<exclusiveEndIndex
    }

    var body: some View {
        visitsPreviewList
            .animation(.easeInOut)
    }

    private func shiftActivePreviewVisitIndex() {
        let startingVisitIndexOfNextSlide = planIndex + numberOfCellsInBlock
        let startingVisitIndexOfNextSlideIsValid = startingVisitIndexOfNextSlide < mealPlans.count
        planIndex = startingVisitIndexOfNextSlideIsValid ? startingVisitIndexOfNextSlide : 0
    }

    private var visitsPreviewList: some View {
        VStack(spacing: 0) {
            ScrollView(showsIndicators: false) {
                if filteredMealPlans.count != 0 {
                    ForEach(0..<filteredMealPlans.count, id: \.self) { result in
                        let recipeLink = filteredMealPlans[result].recipe.uri
                        
                        NavigationLink(destination: DetailRecipeView(id: recipeLink)){
                            
                            MealPlanCell(mealPlan: filteredMealPlans[result])
                            
                        }.buttonStyle(PlainButtonStyle())
                    }
                }else{
                    EmptyMealPlanCell()
                }
                Spacer()
                    .padding(.bottom, 30)
            }
            Spacer()
            .onAppear() {
                
                // First, Sort data by date Ascendingly
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                dateFormatter.timeZone = TimeZone(secondsFromGMT: TimeZone.current.secondsFromGMT())
                
                let data = fetchMealPlanVM.mealPlan.sorted{dateFormatter.date(from: $0.datetime)! < dateFormatter.date(from: $1.datetime)!}
                sortedMealPlans.append(contentsOf: data)
                
                // After sorting,
                // Filter the data by selected date
                for var result in sortedMealPlans{
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    dateFormatter.timeZone = TimeZone(secondsFromGMT: TimeZone.current.secondsFromGMT())
                    let date = dateFormatter.date(from:result.datetime)

                    let startDate = currentCalendar.startOfDay(for: date!)

                    if startDate == selectedDay {
                        
                        // Change the date format to Time format Only
                        let dateTime = dateFormatter.date(from: result.datetime)
                        dateFormatter.dateFormat = "HH:mm"

                        let timeOnly = dateFormatter.string(from: dateTime!)

                        result.datetime = timeOnly
                        filteredMealPlans.append(result)
                    }
                }
            }
        }
        .padding(.trailing, 20)
        .padding(.bottom, 120)
    }
}

//struct VisitsListView_Previews: PreviewProvider {
//    static var previews: some View {
//        DarkThemePreview {
//            VisitsListView(visits: Visit.mocks(start: Date(), end: .daysFromToday(2)), height: 300)
//        }
//    }
//}
