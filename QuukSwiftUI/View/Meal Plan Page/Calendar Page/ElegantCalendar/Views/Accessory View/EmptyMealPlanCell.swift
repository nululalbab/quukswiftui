//
//  EmptyMealPlanCell.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 15/11/20.
//

import SwiftUI

struct EmptyMealPlanCell: View {

    var body: some View {
        HStack {
            RoundedRectangle(cornerRadius: 8)
                .fill(Color("primaryColor"))
                .frame(width: 5, height: 50)

            Spacer()
            
            Text("Tidak ada rencana")
                .font(.system(size: 14))
            
            Spacer()
        }
        .frame(height: VisitPreviewConstants.cellHeight)
        .padding(.horizontal, 18)
        .background(Color("thirdColor"))
        .cornerRadius(20)
    }
}
