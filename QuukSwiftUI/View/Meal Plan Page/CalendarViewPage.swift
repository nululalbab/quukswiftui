//
//  CalendarViewPage.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 13/10/20.
//

import SwiftUI
import ToastUI

struct CalendarViewPage: View {
    
    @ObservedObject var fetchMealPlanVM  : FetchMealPlanVM = FetchMealPlanVM()
    
    @State var isLogin = false
    @State var isGetData = false
    @Binding var tab: Tab
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State private var counter = 0
    
    var body: some View {
        
        VStack {
            if isGetData == true {
                ExampleCalendarView(
                    ascVisits: MealPlan.mocks(
                        start: .daysFromToday(-30*3),
                        end: .daysFromToday(30*3)),
                    initialMonth: Date(),
                    fetchMealPlanVM: fetchMealPlanVM
                )
            }else{
                ExampleCalendarView(
                    ascVisits: MealPlan.mocks(
                        start: .daysFromToday(-30*3),
                        end: .daysFromToday(30*3)),
                    initialMonth: Date(),
                    fetchMealPlanVM: fetchMealPlanVM
                )
            }
        }
        .padding(.top, 50)
        .navigationBarTitle("Jadwal Menu", displayMode: .large)
        .navigationBarItems(trailing:
            Button(action: {
                self.tab = .resep
            }) {
                Image(systemName: "plus")
                    .font(.title2)
            }
            .frame(width: 34, height: 34, alignment: .center)
            .foregroundColor(.white)
            .background(Color("primaryColor"))
            .clipShape(Circle())
        )
        .onAppear() {
            if  DefaultsKeys.defaults.get(.bearer) == nil {
                self.isLogin = true
            } else {
                
                self.isLogin = false
                fetchData()
            }
        }
        .onReceive(timer) { time in
            if fetchMealPlanVM.mealPlan.isEmpty && !isLogin {
                // Retry in 5s & 10s & 15s
                if counter == 5 {
                    fetchData()
                } else if counter > 5 {
                    counter = 0
                }
            }else{
                self.timer.upstream.connect().cancel()
            }
            self.counter += 1
        }
        .toast(isPresented: $fetchMealPlanVM.isLoading) {
            print("Toast dismissed")
        } content: {
            ToastView("Loading...")
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
    }
    
    func fetchData() {
        
        fetchMealPlanVM.LoadFetch()
        
//        let completionhandler:(Bool)->Void = { (sucess) in
//            if sucess {
//                isGetData = true
//            }else{
//
//            }
//        }
//        fetchMealPlanVM.fetchAllDataWithHandler(sucess: true, completionHandler: completionhandler)
        
        // By Selected Day
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
//            let dateString = dateFormatter.string(from: Date())
//            fetchMealPlanVM.fetchDataWithHandler(selectedDate: "2020-11-16 00:00", sucess: true, completionHandler: completionhandler)
    }
}
