//
//  AddMealPlan.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 15/10/20.
//

import SwiftUI

struct AddMealPlan: View {
    let recipe : Recipe
    
    @State var mealPlanVM = MealPlanViewModel()
    
    @State private var showCalendar = false
    
    @State private var showingAdvancedOptions = false
    
    @State private var enableLogging = false
    
    static let selectedDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "y-MM-dd hh:mm a"
        return formatter
    }()
    
    
    var body: some View {
        
        Form {
            TextField("Judul", text: self.$mealPlanVM.name)
                .disableAutocorrection(true)
                .onTapGesture {
                    if showCalendar {
                        showCalendar.toggle()
                    }
                }
            
            HStack {
                
                Text("Tanggal")
                
                Spacer()
                
                Text("\(mealPlanVM.datetimeDate, formatter: AddMealPlan.selectedDateFormat)")
                    .onTapGesture {
                        
                        self.endEditing(true)
                        
                        withAnimation{
                            showCalendar.toggle()
                        }
                    }
                    .alignmentGuide(.trailing) { d in d[.trailing] }
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .frame(width: UIScreen.main.bounds.width / 2)
                    .foregroundColor(showCalendar ? Color("secondaryColor") : Color.black)
            }
            
            
            
            if showCalendar {
                DatePicker("", selection: $mealPlanVM.datetimeDate, displayedComponents: [.date, .hourAndMinute])
                    .datePickerStyle(WheelDatePickerStyle())
            }
        }
        .navigationBarTitle("Tambah Meal Plan", displayMode: .automatic)
        .navigationBarItems(trailing:
                Button(action: {
//                    self.mealPlanVM.CreateMealPlan()
            }, label: {
                
                NavigationLink(destination: CalendarViewPage()) {
                    Text("Simpan")
                }
                .foregroundColor(Color("secondaryColor"))
                .simultaneousGesture(TapGesture().onEnded{
                    let recipeID = recipe.shareAs!.split(separator: "/")
                    
                    mealPlanVM.recipe_id = String(recipeID[3])
                    mealPlanVM.recipe_name = recipe.label!
                    
                    self.mealPlanVM.CreateMealPlan()
                })
            })
        )
    }
}

#if canImport(UIKit)
extension View {
    func endEditing(_ force: Bool) {
        UIApplication.shared.windows.forEach { $0.endEditing(force)}
    }
}
#endif
