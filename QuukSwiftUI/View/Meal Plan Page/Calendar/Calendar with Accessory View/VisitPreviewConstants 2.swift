//
//  VisitPreviewConstants.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 13/10/20.
//

import SwiftUI

struct VisitPreviewConstants {

    static let cellHeight: CGFloat = 30
    static let cellPadding: CGFloat = 10

    static let previewTime: TimeInterval = 3

}
