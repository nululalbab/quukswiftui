//
//  AddMealPlan.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 15/10/20.
//

import SwiftUI
import ToastUI

struct AddMealPlan: View {
    let recipe : DetailRecipeResult
    
    @ObservedObject var mealPlanVM : MealPlanViewModel = MealPlanViewModel()
    
    @State private var showCalendar = false
    
    @State private var showingAdvancedOptions = false
    
    @State private var enableLogging = false
    
    @State private var mealNote = ""
    
    @State private var mealDate = Date()
    
    @Environment(\.presentationMode) var presentationMode
        
    
    static let selectedDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "y-MM-dd hh:mm a"
        return formatter
    }()
    
    var closedRange: ClosedRange<Date> {
        let twoDaysAgo = Calendar.current.date(byAdding: .hour, value: 1, to: Date())!
        let fiveDaysAgo = Calendar.current.date(byAdding: .day, value: 30*3, to: Date())!
        
        return twoDaysAgo...fiveDaysAgo
    }
    
    
    var body: some View {
        
        Form {
            TextField("Judul", text: self.$mealNote)
                .disableAutocorrection(true)
                .onTapGesture {
                    if showCalendar {
                        showCalendar.toggle()
                    }
                }
            
            HStack {
                
                Text("Tanggal")
                
                Spacer()
                
                Text("\(mealDate, formatter: AddMealPlan.selectedDateFormat)")
                    .onTapGesture {
                        
                        self.endEditing(true)
                        
                        withAnimation{
                            showCalendar.toggle()
                        }
                    }
                    .alignmentGuide(.trailing) { d in d[.trailing] }
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .frame(width: UIScreen.main.bounds.width / 2)
                    .foregroundColor(showCalendar ? Color("secondaryColor") : Color.black)
            }
            
            
            
            if showCalendar {
                DatePicker("", selection: $mealDate, in: Date()..., displayedComponents: [.date, .hourAndMinute])
                    .datePickerStyle(WheelDatePickerStyle())
            }
        }
        .navigationBarTitle("Tambah Meal Plan", displayMode: .automatic)
        .navigationBarItems(trailing:
                Button(action: {
//                    self.mealPlanVM.CreateMealPlan()
                    
            }, label: {
                Text("Simpan")
                .foregroundColor(mealNote.isEmpty ? Color(.lightGray) : Color("primaryColor"))
                .simultaneousGesture(TapGesture().onEnded{
                    if recipe.uri != nil {
                        mealPlanVM.recipe_id = recipe.uri!
                        mealPlanVM.recipe_name = recipe.label!
                        mealPlanVM.name = mealNote
                        mealPlanVM.datetimeDate = mealDate
                        
                        self.mealPlanVM.CreateMealPlan(completion: { result in
                            if result {
                                
                                self.presentationMode.wrappedValue.dismiss()
                                
                            }
                        })
                    }
                })
                .disabled(mealNote.isEmpty)
            })
        )
        .toast(isPresented: self.$mealPlanVM.isLoading) {
            print("Toast dismissed")
        } content: {
            ToastView("Loading...")
                .toastViewStyle(IndefiniteProgressToastViewStyle())
        }
    }
}

#if canImport(UIKit)
extension View {
    func endEditing(_ force: Bool) {
        UIApplication.shared.windows.forEach { $0.endEditing(force)}
    }
}
#endif
