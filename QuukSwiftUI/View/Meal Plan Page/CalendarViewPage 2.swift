//
//  CalendarViewPage.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 13/10/20.
//

import SwiftUI

struct CalendarViewPage: View {
    
//    @StateObject var localNotification = LocalNotification()
    @ObservedObject var fetchMealPlanVM  : FetchMealPlanVM = FetchMealPlanVM()
    
    @State var isLogin = false

    
//    let startDate = Date().addingTimeInterval(TimeInterval(60 * 60 * 24 * (-30 * 36)))
//    let endDate = Date().addingTimeInterval(TimeInterval(60 * 60 * 24 * (30 * 36)))
    
//    @ObservedObject var calendarManager = ElegantCalendarManager(
//            configuration: CalendarConfiguration(startDate: -Date().addingTimeInterval(TimeInterval(60 * 60 * 24 * (-30 * 36))),
//                                                 endDate: Date().addingTimeInterval(TimeInterval(60 * 60 * 24 * (30 * 36)))))
 
    var body: some View {
//        Text("\(fetchMealPlanVM.mealPlan.count)")
        
//        Text("\(fetchMealPlanVM.mealPlan.)")
        
        ExampleCalendarView(
            ascVisits: MealPlan.mocks(
                start: .daysFromToday(-30*3),
                end: .daysFromToday(30*3)),
            initialMonth: Date(),
            fetchMealPlanVM: fetchMealPlanVM
        )
        .padding(.top, 50)
        .navigationBarTitle("Jadwal Menu", displayMode: .large)
        .navigationBarItems(trailing:
            HStack {
                NavigationLink(destination: BookmarkPage()) {
                    NavigationLink(destination: SearchResultPage()) {
                        Image(systemName: "plus")
                            .font(.title2)
                    }
                    .frame(width: 34, height: 34, alignment: .center)
                    .foregroundColor(.white)
                    .background(Color(#colorLiteral(red: 0.6156862745, green: 0.7647058824, blue: 0.4509803922, alpha: 1)))
                    .clipShape(Circle())
                }
            }
        )
//        .navigationBarItems(trailing:
//                Button(action: {
////                    self.mealPlanVM.CreateMealPlan()
//
//            }, label: {
//
//                NavigationLink(destination: punyakuTabView()) {
//                    Image(systemName: "plus")
//                        .font(.title2)
//                }
//                .frame(width: 34, height: 34, alignment: .center)
//                .foregroundColor(.white)
//                .background(Color(#colorLiteral(red: 0.6156862745, green: 0.7647058824, blue: 0.4509803922, alpha: 1)))
//                .clipShape(Circle())
//                .simultaneousGesture(TapGesture().onEnded{
//                    localNotification.setLocalNotification(title: "title",
//                                                           subtitle: "Subtitle",
//                                                           body: "this is nody",
//                                                           when: 2)
//                })
//            })
//        )

        
    }
    
}

//struct CalendarViewPage_Previews: PreviewProvider {
//    static var previews: some View {
//        CalendarViewPage()
//    }
//}
