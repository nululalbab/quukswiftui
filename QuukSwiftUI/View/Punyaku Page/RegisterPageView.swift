//
//  RegisterPageView.swift
//  QuukSwiftUI
//
//  Created by Najibullah Ulul Albab on 06/10/20.
//

import SwiftUI
import ToastUI

struct RegisterPageView: View {

    @ObservedObject var registerviewmodel : RegisterViewModel = RegisterViewModel()
    @Environment(\.presentationMode) var presentationMode
    @State var isEmptyEmail : Bool
    @State var isEmptyPassword : Bool
    @State var isEmptyComfirmPassword : Bool
    @State var isEmptyName : Bool
    @State var isEmptyUsername : Bool

    var body: some View {
        ZStack {
            ScrollView{

                VStack {
                    VStack(alignment: .leading, spacing: 20){
                        Text("Daftar untuk membuat rencana belanja")
                            .font(.title)
                            .bold()

                        PlainTextField(titlefield: "Name", placeholder: "Full Name", textfield: self.$registerviewmodel.name, isError: $isEmptyName, errorhandling: "fjnj")

                        PlainTextField(titlefield: "Username", placeholder: "momslovetocook", textfield: self.$registerviewmodel.username, isError: $isEmptyUsername, errorhandling: "fjnj")
                        PlainTextField(titlefield: "Email", placeholder: "momslovetocook@email.com", textfield: self.$registerviewmodel.email, isError: $isEmptyEmail, errorhandling: "fjnj")


                        SecureTextField(titlefield: "Kata Sandi", placeholder: "**********", textfield: $registerviewmodel.password, isError: $isEmptyPassword, errorhandling: "snffvsfb")

                        SecureTextField(titlefield: "Konfirmasi Kata Sandi", placeholder: "**********", textfield:  self.$registerviewmodel.password_confirmation, isError: $isEmptyComfirmPassword, errorhandling: "snffvsfb")


                        HStack {
                            Spacer()
                            Button(action: {

                                if self.registerviewmodel.name.isEmpty {
                                    isEmptyName = true
                                } else {
                                    isEmptyName = false

                                }

                                if self.registerviewmodel.username.isEmpty {
                                    isEmptyUsername = true
                                } else {
                                    isEmptyUsername = false

                                }

                                if self.registerviewmodel.email.isEmpty {
                                    isEmptyEmail = true
                                } else {
                                    isEmptyEmail = false

                                }

                                if self.registerviewmodel.password.isEmpty {
                                    isEmptyPassword = true
                                } else {
                                    isEmptyPassword = false

                                }

                                if self.registerviewmodel.password_confirmation != self.registerviewmodel.password {
                                    isEmptyComfirmPassword = true
                                } else {
                                    isEmptyComfirmPassword = false

                                }


                                if self.registerviewmodel.email.isEmpty == false &&
                                    self.registerviewmodel.name.isEmpty == false &&
                                    self.registerviewmodel.username.isEmpty == false && self.registerviewmodel.password.isEmpty == false &&
                                    self.registerviewmodel.password_confirmation.isEmpty == false &&
                                    self.registerviewmodel.username.isEmpty == false {
                                    self.registerviewmodel.Register(completion: { result in

                                        if result {

                                            self.presentationMode.wrappedValue.dismiss()

                                        }
                                    })

                                }
                            }, label: {
                                ZStack{
                                    RoundedRectangle(cornerRadius: 20)
                                        .fill(Color(#colorLiteral(red: 0.9803921580314636, green: 0.8078431487083435, blue: 0.47058823704719543, alpha: 1)))
                                        .frame(width: 280, height: 40)
                                    Text("Register")
                                        .font(.body)
                                        .bold()
                                }

                            })
                            .buttonStyle(PlainButtonStyle())
                            Spacer()
                        }


                        Spacer()
                        HStack{
                            Spacer()

                            Button {
                                self.presentationMode.wrappedValue.dismiss()
                            } label: {


                                VStack(alignment: .center
                                       , spacing: 8){
                                    Text("Already have an account?")
                                    Text("Sign In")
                                        .bold()
                                }
                            }
                            .buttonStyle(PlainButtonStyle())

                            Spacer()
                        }

                    }
                    .padding(25)
                    .background(Color("thirdColor"))
                    .cornerRadius(40)
                }
                .padding(.horizontal, 20)
            }
            .padding(.vertical, 20)
            .padding(.bottom, 40)
            .navigationBarTitle("Register")
            .toast(isPresented: self.$registerviewmodel.isLoading) {

            } content: {
                ToastView("Loading...")
                    .toastViewStyle(IndefiniteProgressToastViewStyle())
            }

            if self.registerviewmodel.isError == true {
                toasttest( text: self.registerviewmodel.errormassage ?? "")
                    .padding(.bottom, 50)

            }


        }
    }

}

struct RegisterPageView_Previews: PreviewProvider {
    @State  static var email : String = ""
    @State  static var password : String = ""
    @State static  var username : String = ""
    static var previews: some View {
        RegisterPageView(isEmptyEmail: false, isEmptyPassword: false, isEmptyComfirmPassword: false, isEmptyName: false, isEmptyUsername: false)
    }
}

struct PlainTextField  : View{

    var titlefield : String
    var placeholder : String
    @Binding var textfield : String
    @Binding var isError : Bool
    var errorhandling : String

    var body: some View {
        Group{
            Text(titlefield)
                .font(.body)

            TextField(placeholder, text: $textfield)
                .padding()
                .frame(width: 280, height: 40)
                .background(Color(.white))
                .mask(RoundedRectangle(cornerRadius: 90))
            if isError {
            Text(errorhandling)
                .font(.caption)
                .foregroundColor(.red)
                .padding(.top, -10)
            }
        }
    }
}

struct SecureTextField  : View{

    var titlefield : String
    var placeholder : String
    @Binding var textfield : String
    @Binding var isError : Bool
    var errorhandling : String

    var body: some View {
        Group{
            Text(titlefield)
                .font(.body)

            SecureField(placeholder, text: $textfield)
                .padding()
                .frame(width: 280, height: 40)
                .background(Color(.white))
                .mask(RoundedRectangle(cornerRadius: 90))

            if isError {
            Text(errorhandling)
                .font(.caption)
                .foregroundColor(.red)
                .padding(.top, -10)
            }

        }
    }
}
