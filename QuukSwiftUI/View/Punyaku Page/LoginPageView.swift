//
//  LoginPageView.swift
//  QuukSwiftUI
//
//  Created by Najibullah Ulul Albab on 06/10/20.
//

import SwiftUI
import ToastUI
struct LoginPageView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject var loginviewmodel : LoginViewModel = LoginViewModel()
    @State var isEmptyEmail : Bool
    @State var isEmptyPassword : Bool
    
    var body: some View {
        ZStack {
            ScrollView{
                
                VStack {
                    VStack(alignment: .leading, spacing: 20){
                        Text("Masuk untuk simpan resep favoritmu")
                            .font(.title)
                            .bold()
                        
                        PlainTextField(titlefield: "Email", placeholder: "momslovetocook@email.com", textfield: self.$loginviewmodel.username, isError: $isEmptyEmail, errorhandling: "fjnj")
                        
                        SecureTextField(titlefield: "Kata Sandi", placeholder: "**********", textfield: $loginviewmodel.password, isError: $isEmptyPassword, errorhandling: "snffvsfb")
                        
                        
                        ZStack{
                            RoundedRectangle(cornerRadius: 20)
                                .fill(Color(#colorLiteral(red: 0.9803921580314636, green: 0.8078431487083435, blue: 0.47058823704719543, alpha: 1)))
                                .frame(width: 280, height: 40)
                            Text("Login")
                                .font(.body)
                                .bold()
                        } .simultaneousGesture(TapGesture().onEnded{
                            
                            if self.loginviewmodel.username.isEmpty {
                                isEmptyEmail = true
                            } else {
                                isEmptyEmail = false
                                
                            }
                            
                            if self.loginviewmodel.password.isEmpty {
                                isEmptyPassword = true
                                
                            }else {
                                isEmptyPassword = false
                            }
                            if self.loginviewmodel.password.isEmpty == false && self.loginviewmodel.username.isEmpty == false{
                                self.loginviewmodel.Login(completion: { result in
                                    
                                    if result {
                                        
                                        self.presentationMode.wrappedValue.dismiss()
                                        
                                    }
                                })
                                
                            }
                            
                        })
                        
                        Spacer()
                        
                        HStack{
                            Spacer()
                            VStack(alignment: .center
                                   , spacing: 8){
                                Text("Belum Punya Akun?")
                                NavigationLink(destination: RegisterPageView(isEmptyEmail: false, isEmptyPassword: false, isEmptyComfirmPassword: false, isEmptyName: false, isEmptyUsername: false)){
                                    Text("Daftar Disini")
                                        .bold()
                                }
                                .buttonStyle(PlainButtonStyle())
                            }
                            Spacer()
                        }
                        
                        
                    }
                    .frame(minHeight: 520, maxHeight: 600)
                    .padding(25)
                    .background(Color("thirdColor"))
                    .cornerRadius(40)
                }
                
                .padding(.horizontal, 20)
                
               
            }
            .padding(.vertical, 20)
            .navigationBarTitle("Login")
            
            .toast(isPresented: self.$loginviewmodel.isLoading) {
                print("Toast dismissed")
            } content: {
                ToastView("Loading...")
                    .toastViewStyle(IndefiniteProgressToastViewStyle())
            }
            
            if self.loginviewmodel.isError == true {
                toasttest( text: self.loginviewmodel.errormassage ?? "")
                    .padding(.bottom, 50)
            }
            
        }
    }
}

struct toasttest: View {
    var text: String
    
    var body: some View {
        VStack {
            Spacer()
            Text(text)
                .bold()
                .foregroundColor(.white)
                .padding()
                .background(Color.green)
                .cornerRadius(8.0)
                .shadow(radius: 4)
                .padding()
        }
    }
}
