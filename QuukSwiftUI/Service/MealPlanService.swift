//
//  MealPlanService.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 19/10/20.
//

import Foundation
class MealPlanService {
    
    func createPlanService(vm : MealPlanViewModel , completion: @escaping ((Swift.Result<CreatePlanResult, LoadErrorHelper>) -> Void)) {
        
        let createPlan = CreatePlanPost(vm)
        
        guard let urlString  = "http://inibukan.com/api/meal-plan" as? String else {
            return
        }
        guard let data = try? JSONEncoder().encode(createPlan) else {
            fatalError("Error encoding order!")
        }
        
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Post"
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "createPlanService URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "createPlanService httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    let posts = try JSON().newJSONDecoder().decode(CreatePlanResult.self, from: data)
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "createPlanService OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
            }
            
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "createPlanService BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "createPlanService EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
        }.resume()
        
    }
    
    func fetchAllMealPlan(completion: @escaping ((Swift.Result<FetchMealPlanResult, LoadErrorHelper>) -> Void)) {
        
        guard let urlString  = "http://inibukan.com/api/meal-plan" as? String else {
            return
        }
        
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Get"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    
                    let posts = try JSON().newJSONDecoder().decode(FetchMealPlanResult.self, from: data)
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    print(apiErrors)
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
        }.resume()
        
    }
    
    func fetchMealPlanByDate(selectedDate: String, completion: @escaping ((Swift.Result<FetchMealPlanResult, LoadErrorHelper>) -> Void)) {
        
        guard let urlString  = "http://inibukan.com/api/meal-plan?date="+selectedDate as? String else {
            return
        }
        
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Get"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    
                    let posts = try JSON().newJSONDecoder().decode(FetchMealPlanResult.self, from: data)
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
        }.resume()
        
    }
    
}
