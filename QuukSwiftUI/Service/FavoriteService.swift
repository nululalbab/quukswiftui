//
//  FavouriteService.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 08/10/20.
//

import Foundation


class FavoriteService {
    
    func updateFavorite(vm : UpdateFavoriteViewModel , completion: @escaping ((Swift.Result<FavoriteResult, LoadErrorHelper>) -> Void)) {
        
        let favoritedata = FavoritePost(vm)
        
        guard let urlString  = "http://inibukan.com/api/recipe/favorite" as? String else {
            return
        }
        
        guard let data = try? JSONEncoder().encode(favoritedata) else {
            fatalError("Error encoding body!")
        }
        
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Post"
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    
                    let posts = try JSON().newJSONDecoder().decode(FavoriteResult.self, from: data)
                    
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
         
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
        }.resume()
        
    }
    
    
    func fetchFavorite( completion: @escaping ((Swift.Result<FetchFavoriteResult, LoadErrorHelper>) -> Void)) {
        
        guard let urlString  = "http://inibukan.com/api/profile/favorites" as? String else {
            return
        }
    
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Get"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    
                    let posts = try JSON().newJSONDecoder().decode(FetchFavoriteResult.self, from: data)
                    
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
        }.resume()
        
    }
    
}
