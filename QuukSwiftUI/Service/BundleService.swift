//
//  BundleService.swift
//  QuukSwiftUI
//
//  Created by Lois Pangestu on 20/10/20.
//

import Foundation

class BundleService {
    
    func recipeBundleService(completion: @escaping ((Swift.Result<[BundleData], Error>) -> Void)) {
        
        let urlString  = "http://inibukan.com/api/bundle"
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { (data, urlResponse, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else { return }
            
            
            do {
                let posts = try JSON().newJSONDecoder().decode(BundleModel.self, from: data)
                
                var bundles = [BundleData]()
                
                for data in posts.data!{
                    
                    bundles.append(data)
                }
                completion(.success(bundles))
            } catch(let error) {
                completion(.failure(error))
            }
        }.resume()
    }
    
    
}
