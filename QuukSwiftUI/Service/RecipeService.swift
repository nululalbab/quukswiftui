//
//  SearchRecipeService.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 25/09/20.
//

import Foundation

class RecipeService {
    
    func searchRecipeService(query : String,calorie : String, completion: @escaping ((Swift.Result<[Recipe], LoadErrorHelper>) -> Void)) {
        
        let newKey:String = query.replacingOccurrences(of: " ", with: "+")
        
        guard let urlString  = "http://inibukan.com/api/recipe?q=\(newKey)&calories=50-\(calorie)" as? String else {
            return
        }
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { (data, urlResponse, error) in
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                
                
                do {
                    let posts = try JSON().newJSONDecoder().decode(RecipeResult.self, from: data)
                    
                    var recipes = [Recipe]()
                    
                    for hit in posts.hits!{
                        
                        recipes.append(hit.recipe!)
                    }
                    completion(.success(recipes))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
            }
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
       
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
            
            
        }.resume()
    }
    
    func recipeFeedService(query:String,completion: @escaping ((Swift.Result<[RecipeWajibCoba], LoadErrorHelper>) -> Void)) {
        
        let newKey:String = query.replacingOccurrences(of: " ", with: "+")
        
        guard let urlString  = "http://inibukan.com/api/recipe?to=5&q="+newKey as? String else {
            return
        }
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                
                do {
                    let posts = try JSON().newJSONDecoder().decode(RecipeWajibCobaResult.self, from: data)
                    
                    var recipes = [RecipeWajibCoba]()
                    
                    for hit in posts.hits!{
                        
                        recipes.append(hit.recipe!)
                    }
                    completion(.success(recipes))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
            }
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
          
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
            
        }.resume()
    }
    
    
    
    func FetchDetailRecipe(key : String, completion: @escaping ((Swift.Result<DetailRecipeResult, LoadErrorHelper>) -> Void)) {
        
        guard let urlString  = "http://inibukan.com/api/recipe/\(key)" as? String else {
            return
        }
        
        var bearer : String
        
        if DefaultsKeys.defaults.get(.bearer) == nil {
            bearer = ""
        }else{
            bearer = DefaultsKeys.defaults.get(.bearer)!
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Get"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    
                    let posts = try JSON().newJSONDecoder().decode(DetailRecipeResult.self, from: data)
                    
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
    
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
        }.resume()
        
    }
}
