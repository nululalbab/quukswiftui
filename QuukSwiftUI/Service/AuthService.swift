//
//  AuthService.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 06/10/20.
//
import Foundation

class AuthService {
    
    
    //Mark:- loginService
    
    func loginService(vm : LoginViewModel , completion: @escaping ((Swift.Result<LoginResult, LoadErrorHelper>) -> Void)) {
        
        let logindata = LoginPost(vm)
        
        guard let urlString  = "http://inibukan.com/api/auth/login" as? String else {
            return
        }
        
        guard let data = try? JSONEncoder().encode(logindata) else {
            fatalError("Error encoding order!")
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Post"
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "loginService error", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "loginService httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    let posts = try JSON().newJSONDecoder().decode(LoginResult.self, from: data)
                    
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "error", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "loginService BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.UNAUTHORIZED_401 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
    
                    completion(.failure(.unauthorized(apiError: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "loginService UNAUTHORIZED_401", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    print(apiErrors)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "loginService EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            
            if httpResponse.statusCode == HTTPCode.SERVER_ERROR_500 {
                
                completion(.failure(.internalServerError))
                
                return
                
            }
            
            
        }.resume()
    }
    
    
    //Mark:- registerService
    
    
    func registerService(vm : RegisterViewModel , completion: @escaping ((Swift.Result<RegisterResult, LoadErrorHelper>) -> Void)) {
        
        let registerdata = RegisterPost(vm)
        
        guard let urlString  = "http://inibukan.com/api/auth/register" as? String else {
            return
        }
        
        guard let data = try? JSONEncoder().encode(registerdata) else {
            fatalError("Error encoding order!")
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Post"
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "registerService URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "registerService httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    
                    let posts = try JSON().newJSONDecoder().decode(RegisterResult.self, from: data)
                    
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "registerService OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "registerService BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "registerService EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
        }.resume()
        
    }
    
    
    
}

