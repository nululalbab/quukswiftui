//
//  SearchRecipeService.swift
//  QuukSwiftUI
//
//  Created by Moh Zinnur Atthufail Addausi on 25/09/20.
//

import Foundation

class RecipeService {
    
    func searchRecipeService(query : String, completion: @escaping ((Swift.Result<[Recipe], Error>) -> Void)) {
        
        let newKey:String = query.replacingOccurrences(of: " ", with: "+")
        
        guard let urlString  = "http://rizkyhidayat.my.id/api/recipe?q="+newKey as? String else {
            return
        }
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { (data, urlResponse, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else { return }
            
            
            do {
                let posts = try JSON().newJSONDecoder().decode(RecipeResult.self, from: data)
                
                var recipes = [Recipe]()
                
                for hit in posts.hits!{
                
                    recipes.append(hit.recipe!)
                }
                completion(.success(recipes))
            } catch(let error) {
                completion(.failure(error))
            }
        }.resume()
    }
    
    func recipeFeedService(completion: @escaping ((Swift.Result<[Recipe], Error>) -> Void)) {
        let query = "Kacang"
        let newKey:String = query.replacingOccurrences(of: " ", with: "+")
        
        guard let urlString  = "http://rizkyhidayat.my.id/api/recipe?q="+newKey as? String else {
            return
        }
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { (data, urlResponse, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let data = data else { return }
            
            
            do {
                let posts = try JSON().newJSONDecoder().decode(RecipeResult.self, from: data)
                
                var recipes = [Recipe]()
                
                for hit in posts.hits!{
                
                    recipes.append(hit.recipe!)
                }
                completion(.success(recipes))
            } catch(let error) {
                completion(.failure(error))
            }
        }.resume()
    }
}



