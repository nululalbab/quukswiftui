//
//  HistoryService.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 17/11/20.
//

import Foundation


class HistoryService {
    
    // MARK: POST DONE
    func historyDone(completion: @escaping ((Swift.Result<CreateHistoryResult, LoadErrorHelper>) -> Void)){
        guard let urlString  = "http://inibukan.com/api/shop-list/done" as? String else {
            return
        }
        
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Post"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "createPlanService URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    let posts = try JSON().newJSONDecoder().decode(CreateHistoryResult.self, from: data)
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "createPlanService OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
            }
            
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "createPlanService BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "createPlanService EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
        }.resume()
    }
    
    // MARK: POST API
    func postHistory(vm : HistoryViewModel , completion: @escaping ((Swift.Result<CreateHistoryResult, LoadErrorHelper>) -> Void)) {
        
        let historydata = historyPost(vm)
        
        guard let urlString  = "http://inibukan.com/api/shop-list" as? String else {
            return
        }
        
        guard let data = try? JSONEncoder().encode(historydata) else {
            fatalError("Error encoding body!")
        }
        
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Post"
        request.httpBody = data
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-type")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            if let error = error {
                completion(.failure(.unknown(apiError: ApiError(error: "createPlanService URLSession data", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                return
            }
            
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    let posts = try JSON().newJSONDecoder().decode(CreateHistoryResult.self, from: data)
                    completion(.success(posts))
                    
                    self.historyDone{ (result) in
                        switch result {
                        case .success(let result):
                            print("post asd : \(result.message)")
                        case .failure(let error):
                            print("Post History Done error")
                        }
                    }
                    
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "createPlanService OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
            }
            
            
            if httpResponse.statusCode == HTTPCode.BAD_REQUEST_400 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiError.self, from: data)
                    completion(.failure(.badRequest(apiErrors: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "createPlanService BAD_REQUEST_400", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
            
            if httpResponse.statusCode == HTTPCode.EVENT_ALREADY_CREATED_422 {
                do {
                    let apiErrors = try JSON().newJSONDecoder().decode(ApiErrorDataAlready.self, from: data)
                    completion(.failure(.already(already: apiErrors)))
                } catch (let error) {
                    let apiError = ApiError(error: "createPlanService EVENT_ALREADY_CREATED_422", errorDescription: "error", hint: "error", message: error.localizedDescription)
                    completion(.failure(.unknown(apiError: apiError)))
                }
                return
                
            }
            
        }.resume()
        
    }
    
    // MARK: GET API
    func fetchHistory( completion: @escaping ((Swift.Result<FetchHistory, LoadErrorHelper>) -> Void)) {
        
        guard let urlString  = "http://inibukan.com/api/shop-list" as? String else {
            return
        }
    
        guard let bearer = DefaultsKeys.defaults.get(.bearer) else {
            return
        }
        
        let url = URL(string: urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = "Get"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("Bearer "+bearer, forHTTPHeaderField:"Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            
            guard let data = data else { return }
            
            guard let httpResponse = urlResponse as? HTTPURLResponse else {
                let apiError = ApiError(error: "httpResponse", errorDescription: "error", hint: "error", message: error?.localizedDescription)
                completion(.failure(.unknown(apiError: apiError)))
                return
            }
            
            if httpResponse.statusCode == HTTPCode.OK_200 {
                do {
                    
                    let posts = try JSON().newJSONDecoder().decode(FetchHistory.self, from: data)
                    
                    completion(.success(posts))
                } catch(let error) {
                    completion(.failure(.unknown(apiError: ApiError(error: "OK_200", errorDescription: "error", hint: "error", message: error.localizedDescription))))
                }
                return
                
            }
            
        }.resume()
        
    }
    
}
