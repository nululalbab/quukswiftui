//
//  Cart+CoreDataProperties.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 18/11/20.
//
//

import Foundation
import CoreData


extension Cart {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cart> {
        return NSFetchRequest<Cart>(entityName: "Cart")
    }

    @NSManaged public var date: Date?
    @NSManaged public var id: UUID?
    @NSManaged public var portion: Float
    @NSManaged public var qty: Float
    @NSManaged public var title: String?
    @NSManaged public var recipeID: String?
    @NSManaged public var image: String?
    @NSManaged public var bahans: NSSet?
    
    public var wrappedTitle: String {
                title ?? "Unknown Wizard"
            }
            
            public var wrappedDate: Date {
                date ?? Date()
            }
            
            public var candyArray: [Bahan] {
                let set = bahans as? Set<Bahan> ?? []
                
                return set.sorted {
                    $0.wrappedBahanName < $1.wrappedBahanName
                }
            }

}

// MARK: Generated accessors for bahans
extension Cart {

    @objc(addBahansObject:)
    @NSManaged public func addToBahans(_ value: Bahan)

    @objc(removeBahansObject:)
    @NSManaged public func removeFromBahans(_ value: Bahan)

    @objc(addBahans:)
    @NSManaged public func addToBahans(_ values: NSSet)

    @objc(removeBahans:)
    @NSManaged public func removeFromBahans(_ values: NSSet)

}

extension Cart : Identifiable {

}
