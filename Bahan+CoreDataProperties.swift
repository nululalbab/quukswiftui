//
//  Bahan+CoreDataProperties.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 18/11/20.
//
//

import Foundation
import CoreData


extension Bahan {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Bahan> {
        return NSFetchRequest<Bahan>(entityName: "Bahan")
    }

    @NSManaged public var check: Bool
    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var qty: Float
    @NSManaged public var origin: Cart?
    
    public var wrappedBahanName: String{
                        name ?? "Unknown bahan"
                    }
            public var wrappedBahanQty: Float{
                qty
            }

}

extension Bahan : Identifiable {

}
