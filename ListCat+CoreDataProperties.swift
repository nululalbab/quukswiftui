//
//  ListCat+CoreDataProperties.swift
//  QuukSwiftUI
//
//  Created by wahyujus on 18/11/20.
//
//

import Foundation
import CoreData


extension ListCat {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ListCat> {
        return NSFetchRequest<ListCat>(entityName: "ListCat")
    }

    @NSManaged public var date: Date?
    @NSManaged public var id: UUID?
    @NSManaged public var title: String?
    @NSManaged public var recipeID: String?
    @NSManaged public var categories: NSSet?
    
    public var wrappedCatTitle: String {
                title ?? "Unknown Wizard"
            }
            
            public var wrappedCatDate: Date {
                date ?? Date()
            }
            
            public var listCatArray: [ListItem] {
                let set = categories as? Set<ListItem> ?? []
                
                return set.sorted {
                    $0.wrappedListName < $1.wrappedListName
                }
            }

}

// MARK: Generated accessors for categories
extension ListCat {

    @objc(addCategoriesObject:)
    @NSManaged public func addToCategories(_ value: ListItem)

    @objc(removeCategoriesObject:)
    @NSManaged public func removeFromCategories(_ value: ListItem)

    @objc(addCategories:)
    @NSManaged public func addToCategories(_ values: NSSet)

    @objc(removeCategories:)
    @NSManaged public func removeFromCategories(_ values: NSSet)

}

extension ListCat : Identifiable {

}
